Project report location: doc/Report.pdf (contains all diagrams of interest)

Testing:

To test services with cUrl, see: src/test/services/cURL.txt

To test javascript with qUnit, see: src/main/webapp/js/test/qUnit.txt

To test the database and the model with JUnit, just run
the tests in the testpackage. By default they use the
embedded persistence unit.

.js files in src/main/webapp/lib/ are external javascript libraries that
we have imported, not written. Please exclude this folder when inspecting 
the repository with gitinspector.
