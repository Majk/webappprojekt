/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.webapp.bookemall.persistence;

import edu.webapp.bookemall.booking.Address;
import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.Venue;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
public class TestPersistVenue {
    final static String PU = "bookemall_pu";
    final static String TEST_PU = "bookemall_test_pu";
    final static String EMBEDDED_TEST_PU = "bookemall_embedded_test_pu";
    static VenueDAO venueDAO;
    static UserDAO userDAO;
    static EntityTransaction t;
    static Venue venue1;
    static Venue venue2;
    static Venue venue3;
    static User user1;
    static User user2;
    static Address address1;
    static Address address2;
    static Address address3;
    
    
    @BeforeClass
    public static void beforeClass() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        venueDAO = new VenueDAO();
        userDAO = new UserDAO();
        
        final EntityManager em = Persistence.createEntityManagerFactory(EMBEDDED_TEST_PU).createEntityManager();
        final Field f1 = userDAO.getClass().getDeclaredField("em");
        final Field f2 = venueDAO.getClass().getDeclaredField("em");
        f1.setAccessible(true);
        f1.set(userDAO, em);
        f2.setAccessible(true);
        f2.set(venueDAO, em);
        
        t = em.getTransaction();
        
        User creator = new User("Tomten", "Hallo", "user@user.com");
        user1 = new User("Evert", "Hallo", "user@user.com");
        user2 = new User("Sara", "Hallo", "user@user.com");
        t.begin();
        userDAO.add(user1);
        t.commit();
        t.begin();
        userDAO.add(user2);
        t.commit();
        t.begin();
        userDAO.add(creator);
        t.commit();
        
        address1 = new Address("thisplace", 5, "Gothenburg", "Sweden");
        address2 = new Address("thisplace", 5, "Gothenburg", "Sweden");
        address3 = new Address("thatotherpalce", 5, "Gothenburg", "Sweden");
        
        venue1 = new Venue(creator, address1, "Alberts Hole");
        venue2 = new Venue(creator, address2, "Hole");
        venue3 = new Venue(creator, address3, "Alberts Hole");
        venue1.setSeats(5);
        venue2.setSeats(10);
        venue3.setSeats(12);
        
        
        venue1.addAdmins(Arrays.asList(
                (new User[] { user1, user2 })));
        venue2.addAdmin(user1);
        venue3.addAdmin(user2);
        
        t.begin();
        venueDAO.add(venue1);
        t.commit();
        t.begin();
        venueDAO.add(venue2);
        t.commit();
        t.begin();
        venueDAO.add(venue3);
        t.commit();
    }
    
    @AfterClass
    public static void tearDown() {
        venueDAO.getEntityManager().getEntityManagerFactory().close();
        venue1 = null;
        venue2 = null;
        venue3 = null;
        user1 = null;
        user2 = null;
        address1 = null;
        address2 = null;
        address3 = null;
        venueDAO = null;
        userDAO = null;
    }
    /**
     * Test of getByVenueName method, of class VenueDAO.
     */
    @Test
    public void testGetByExactVenueName() {
        t.begin();
        List<Venue> list = venueDAO.getByVenueName("Alberts Hole");
        t.commit();
        assertTrue(list.size() == 2);
        assertTrue(list.contains(venue1));
        assertTrue(list.contains(venue3));
        assertFalse(list.contains(venue2));
        t.begin();
        Venue venue = venueDAO.getByExactVenueName("Hole");
        t.commit();
        assertTrue(venue.equals(venue2));
    }
    
//        /**
//     * Test of getPartOfName method, of class VenueDAO.
//     */
//    @Test
//    public void testGetPartOfName() {
//        System.out.println("getPartOfName");
//        List<Venue> list = venueDAO.getByVenueName("Hole");
//        assertEquals(list.size(), 3);
//        assertTrue(list.contains(venue1));
//        assertTrue(list.contains(venue2));
//        assertTrue(list.contains(venue3));
//    }
    
    /**
     * Test of getByAdmin method, of class VenueDAO.
     */
    @Test
    public void testGetByAdmins() {
        t.begin();
        List<Venue> list = venueDAO.getByAdmin(user1);
        t.commit();
        t.begin();
        List<Venue> list2 = venueDAO.getByAdmin(user2);
        t.commit();
        System.out.println("testGetByAdmins " + list.size());
        assertTrue(list.size() == 2);
        assertTrue(list2.size() == 2);
        assertTrue(list.contains(venue1));
        assertTrue(list.contains(venue2));
        assertTrue(list2.contains(venue1));
        assertTrue(list2.contains(venue3));
        
    }
    
    /**
     * Test of getByNumberOfSeats method, of class VenueDAO.
     */
    @Test
    public void testGetByNumberOfSeats() {
        t.begin();
        List<Venue> list = venueDAO.getByNumberOfSeats(0, 9);
        t.commit();
        
        assertTrue(list.size() == 1);
        assertTrue(list.contains(venue1));
        
        t.begin();
        list = venueDAO.getByNumberOfSeats(6, 15);
        t.commit();
        
        assertTrue(list.size() == 2);
        assertTrue(list.contains(venue2));
        assertTrue(list.contains(venue3));
    }
    
    /**
     * Test of getByAddress method, of class VenueDAO.
     */
    @Test
    public void testGetByAddress() {
        t.begin();
        List<Venue> list = venueDAO.getByAddress(address1);
        t.commit();
        t.begin();
        List<Venue> list2 = venueDAO.getByAddress(address2);
        t.commit();
        
        assertEquals(list, list2);
        assertTrue(list.size() == 2);
        assertTrue(list.contains(venue1));
        assertTrue(list.contains(venue2));
        
        t.begin();
        list = venueDAO.getByAddress(address3);
        t.commit();
        
        assertTrue(list.size() == 1);
        assertTrue(list.contains(venue3));
        
    }
}