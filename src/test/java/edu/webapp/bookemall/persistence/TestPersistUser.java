package edu.webapp.bookemall.persistence;

import edu.webapp.bookemall.booking.User;
import java.lang.reflect.Field;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.LocalDate;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 * Testing a Users database mapping and table data.
 * Testing EJB;s by using reflection to set the entityManager.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
public class TestPersistUser {
    
    final static String PU = "bookemall_pu";
    final static String TEST_PU = "bookemall_test_pu";
    final static String EMBEDDED_TEST_PU = "bookemall_embedded_test_pu";
    private static UserDAO DAO;
    static EntityTransaction t;
    static User user1;
    static User user2;
    static User user3;
    static LocalDate birthDate;
    static LocalDate birthDate2;
    
    @BeforeClass
    public static void before() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        DAO = new UserDAO();
        
        final EntityManager em = Persistence.createEntityManagerFactory(EMBEDDED_TEST_PU).createEntityManager();
        final Field f = DAO.getClass().getDeclaredField("em");
        f.setAccessible(true);
        f.set(DAO, em);
        
        birthDate = new LocalDate(1970, 1, 20);
        birthDate2 = new LocalDate(1991, 1, 20);
        user1 = new User("AwesomeAlbinoApa", "JagGillarBanan" , "Apa@MonkeyInternational.apa", "SexyMonkeyNiggahAssYoloSwag420Blazing.png", "Apa", "Apasson");
        user2 = new User("AwesomeAlbinoApa1", "JagGillarBanan" , "Apa@MonkeyInternational.apa", "SexyMonkeyNiggahAssYoloSwag420Blazing.png", "Apa", "Apasson");
        user3 = new User("AwesomeAlbinoApa2", "JagGillarBanan" , "Apa@MonkeyInternational.apa", "SexyMonkeyNiggahAssYoloSwag420Blazing.png", "Apa", "Apasson");
        
        t = em.getTransaction();
        
        t.begin();
        DAO.add(user1);
        t.commit();
        
        t.begin();
        DAO.add(user2);
        t.commit();
        
        t.begin();
        DAO.add(user3);
        t.commit();
    }
    
    @AfterClass
    public static void tearDown() {
        DAO.getEntityManager().getEntityManagerFactory().close();
        user1 = null;
        user2 = null;
        user3 = null;
        birthDate = null;
        birthDate2 = null;
        DAO = null;
    }
    
    @Test
    public void testInit() {
        assertNotNull(DAO);
    }
    
    @Test
    public void testFind() {
        t.begin();
        assertTrue(DAO.getCount() == 3);
        t.commit();
    }
    
    @Test
    public void testGetRange() {
        t.begin();
        List<User> list = DAO.getRange(0, 2);
        t.commit();
        
        System.out.println(list.toString());
        
        assertTrue(list.size() == 2);
        
        System.out.println(user1.getUserName());
        
        assertTrue(list.get(0).equals(user1));
        
        System.out.println(user2.getUserName());
        
        assertTrue(list.get(1).equals(user2));
        
        t.begin();
        assertTrue(DAO.getRange(0, 5).size() == 3);
        t.commit();
        
        t.begin();
        assertTrue(DAO.getRange(5, 5).isEmpty());
        t.commit();
    }
    
    @Test
    public void testGetByName() {
        t.begin();
        List<User> list = DAO.getByName("Apa");
        t.commit();
        System.out.println(list.toString());
        assertTrue(list.size() == 3);
        
        t.begin();
        list = DAO.getByName("Apasson");
        t.commit();
        assertTrue(list.size() == 3);
        
        t.begin();
        list = DAO.getByName("Unkown");
        t.commit();
        assertTrue(list.isEmpty());
        
    }
    
    @Test
    public void testGetByUserName() {
        t.begin();
        User ret = DAO.getByUserName("AwesomeAlbinoApa");
        t.commit();
        assertTrue(ret.equals(user1));
        t.begin();
        assertTrue(DAO.getByUserName("Unkown") == null);
        t.commit();
    }
    
    @Test
    public void testGetAll() {
        t.begin();
        assertTrue(DAO.getAll().size() == DAO.getCount());
        t.commit();
    }
    
    @Test
    public void testGetById() {
        t.begin();
        assertTrue(DAO.find(user1.getUuid()).equals(user1));
        t.commit();
        t.begin();
        assertTrue(DAO.find("0") == null);
        t.commit();
    }
    
    @Test
    public void testGetAdmins() {
        t.begin();
        assertTrue(DAO.getAdmins().isEmpty());
        t.commit();
        t.begin();
        DAO.promoteUser(user1);
        t.commit();
        t.begin();
        assertTrue(DAO.getAdmins().size() == 1);
        t.commit();
    }
    
    @Test
    public void testAddUpdateAndRemoveUser() {
        User user4 = new User("AwesomeAlbinoApa3", "JagGillarBanan" , "Apa@MonkeyInternational.apa", "SexyMonkeyNiggahAssYoloSwag420Blazing.png", "Apa", "Apasson");
        
        t.begin();
        DAO.add(user4);
        t.commit();
        
        t.begin();
        user4 = DAO.getByUserName(user4.getUserName());
        t.commit();
        
        assertTrue(user4 != null);
        
        t.begin();
        assertTrue(DAO.getCount() == 4);
        t.commit();
        
        t.begin();
        user4 = DAO.find(user4.getUuid());
        t.commit();
        
        t.begin();
        user4 = DAO.find(user4.getUuid());
        t.commit();
        
        String userID = user4.getUuid();
        DAO.getEntityManager().refresh(user4);
        t.begin();
        DAO.remove(userID);
        t.commit();
        
        t.begin();
        assertTrue(DAO.getCount() == 3);
        t.commit();
        
        t.begin();
        assertTrue(DAO.find(user4.getUuid()) == null);
        t.commit();
    }
    
    @Test
    public void testFindLoginCredentials() {
        t.begin();
        assertTrue(DAO.findLoginCredentials(user1.getUserName(), DigestUtils.sha512Hex("JagGillarBanan")).equals(user1));
        t.commit();
        
        t.begin();
        assertNull(DAO.findLoginCredentials("invalid", "invalid"));
        t.commit();
    }
    
    @Test 
    public void testSetAndDeleteRememberCookie() {
        String uuid = UUID.randomUUID().toString();
        user1.setRememberCookie(uuid);
        t.begin();
        user1 = DAO.update(user1);
        t.commit();
        
        t.begin();
        User tmp = DAO.getRememberMe(uuid);
        t.commit();
        assertTrue(tmp.equals(user1));
        
        String userName = user1.getUserName();
        
        t.begin();
        DAO.deleteRememberMe(userName);
        t.commit();
        
        t.begin();
        assertTrue(DAO.getRememberMe(uuid) == null);
        t.commit();
    }
    
    @Test
    public void testSetAndGetMailToken() {
        String mailToken = UUID.randomUUID().toString();
        user1.setMailToken(DigestUtils.sha512Hex(mailToken));
        t.begin();
        DAO.update(user1);
        t.commit();
        
        t.begin();
        User ret = DAO.getUserFromMailToken(DigestUtils.sha512Hex(mailToken));
        t.commit();
        
        System.out.println(ret);
        assertTrue(user1.equals(ret));
        
        t.begin();
        assertNull(DAO.getUserFromMailToken("invalid"));
        t.commit();
    }
}