/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.webapp.bookemall.persistence;

import edu.webapp.bookemall.booking.Address;
import edu.webapp.bookemall.booking.Booking;
import edu.webapp.bookemall.booking.DateStamp;
import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.Venue;
import java.lang.reflect.Field;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.joda.time.LocalDate;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Here we have tests of the cascading of the model.
 * 
 * There are many VERY complex cases here that we need to 
 * think about. I've made comments above some tests
 * that need careful planning. We got a lot of relationships
 * between different entities so the cascading will be
 * quite a challenge.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
public class TestCascades {
    
    final static String PU = "bookemall_pu";
    final static String TEST_PU = "bookemall_test_pu";
    final static String EMBEDDED_TEST_PU = "bookemall_embedded_test_pu";
    @EJB
    static BookingDAO bDAO;
    @EJB
    static UserDAO uDAO;
    @EJB
    static VenueDAO vDAO;
    static EntityTransaction t;
    static Address address1;
    static Venue venue1;
    static Venue venue2;
    static LocalDate birthDate;
    static User user1;
    static User user2;
    static User creator;
    static Booking booking;
    static DateStamp timeStamp;
    

    
    @BeforeClass
    public static void setUpClass() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        bDAO = new BookingDAO();
        vDAO = new VenueDAO();
        uDAO = new UserDAO();
        
        final EntityManager em = Persistence.createEntityManagerFactory(EMBEDDED_TEST_PU).createEntityManager();
        final Field f1 = bDAO.getClass().getDeclaredField("em");
        final Field f2 = vDAO.getClass().getDeclaredField("em");
        f1.setAccessible(true);
        f1.set(bDAO, em);
        f2.setAccessible(true);
        f2.set(vDAO, em);
        
        final Field f3 = uDAO.getClass().getDeclaredField("em");
        f3.setAccessible(true);
        f3.set(uDAO, em);
        
        t = em.getTransaction();
        
        creator = new User("creator", "password", "email@email.com");
        
        t.begin();
        uDAO.add(creator);
        t.commit();
        
        address1 = new Address("thisplace", 5, "Gothenburg", "Sweden");
        venue1 = new Venue(creator, address1, "Alberts Hole");
        venue2 = new Venue(creator, address1, "Alberts 2 Hole");
        
        t.begin();
        vDAO.add(venue1);
        t.commit();
        
        birthDate = new LocalDate(1970, 1, 20);
        user1 = new User("AwesomeAlbinoApa", "JagGillarBanan" , "Apa@MonkeyInternational.apa", "SexyMonkeyNiggahAssYoloSwag420Blazing.png", "Apa", "Apasson");
        user2 = new User("AwesomeAlbinoApa1", "JagGillarBanan" , "Apa@MonkeyInternational.apa", "SexyMonkeyNiggahAssYoloSwag420Blazing.png", "Apa", "Apasson");
        
        t.begin();
        uDAO.add(user1);
        t.commit();
        
        t.begin();
        uDAO.add(user2);
        t.commit();
        
        timeStamp = new DateStamp(2013, 10, 5, 15, 41);
        booking = new Booking(timeStamp, user1);
        booking.setEventName("test");
        booking.setVenue(venue1);
        booking.addGuest(user2);
        
        t.begin();
        bDAO.add(booking);
        t.commit();
    }
    
    @AfterClass
    public static void tearDownClass() {
        bDAO.getEntityManager().getEntityManagerFactory().close();
        bDAO = null;
        uDAO = null;
        vDAO = null;
        user1 = null;
        booking = null;
        timeStamp = null;
    }
    
    /**
     * User adds and removes a venue.
     * 
     * Venue shall be created, and deleted.
     * 
     * I don't know how to simulate JTA here.
     * Transactions doesn't seem to be enough
     * since it works when running the project.
     * 
     */
    
    //@Test 
    public void userAddsAndRemovesVenue(){
        t.begin();
        vDAO.add(venue2);
        t.commit();
        
        t.begin();
        assertTrue(vDAO.getCount() == 2);
        t.commit();
        
        t.begin();
        uDAO.getEntityManager().refresh(creator);
        t.commit();
        
        String username = creator.getUserName();
        
        t.begin();
        creator = uDAO.getByUserName(username);
        t.commit();
        
        assertTrue(creator.getVenues().contains(venue2));
        
        t.begin();
        venue2 = vDAO.getByVenueName(venue2.getVenueName()).get(0);
        t.commit();
        
        String uuid = venue2.getUuid();
        
        t.begin();
        vDAO.remove(uuid);
        t.commit();
        
        t.begin();
        assertTrue(vDAO.getCount() == 1);
        //assertTrue(vDAO.getByVenueName(venue2.getVenueName()) == null);
        t.commit();
    }
    
    /**
     * User adds and removes a booking.
     * 
     * If there are guests in a booking. Shall the
     * creator of that booking be able to remove it.
     * 
     * Confirmed that Guests and Admins of the event
     * are removed along with the event.
     */
    
    @Test
    public void userCreatesAndRemovesBooking(){
        t.begin();
        assertTrue(bDAO.getByEventName(booking.getEventName()).get(0) != null);
        t.commit();
        
        assertTrue(booking.getAdmins().size() == 1);
        assertTrue(booking.getGuests().size() == 1);
        
        t.begin();
        bDAO.remove(booking.getUuid());
        t.commit();
        
        t.begin();
        assertTrue(bDAO.getByEventName(booking.getEventName()).isEmpty());
        t.commit();
    }
    
    /**
     * User attends and leaves an event.
     * 
     * Need to solve that if the last admin leaves an event.
     * 
     * Then it should be deleted along with the list of users 
     * for that event.
     */
    
    @Test
    public void userAttendsAndLeavesEvent(){
        t.begin();
        bDAO.add(booking);
        t.commit();
        
        t.begin();
        assertTrue(bDAO.getByEventName(booking.getEventName()).get(0) != null);
        t.commit();
        
        assertTrue(booking.getGuests().size() == 1);
        booking.removeGuest(user2);
        
        t.begin();
        booking = bDAO.update(booking);
        t.commit();
        
        t.begin();
        booking = bDAO.getByEventName(booking.getEventName()).get(0);
        t.commit();
        
        assertTrue(booking.getGuests().isEmpty());
        assertTrue(booking.getAdmins().size() == 1);
        
        booking.addAdmin(user2);
        
        t.begin();
        booking = bDAO.update(booking);
        t.commit();
        
        t.begin();
        booking = bDAO.getByEventName(booking.getEventName()).get(0);
        t.commit();
        
        assertTrue(booking.getAdmins().size() == 2);
        
        booking.demote(user2.getUuid());
        
        t.begin();
        booking = bDAO.update(booking);
        t.commit();
        
        t.begin();
        booking = bDAO.getByEventName(booking.getEventName()).get(0);
        t.commit();
        
        assertTrue(booking.getAdmins().size() == 1);
        assertTrue(booking.getGuests().size() == 1);
    }
    
    /**
     * Deleting user with events.
     * 
     * Should it be possible to remove a user with active events?
     * 
     * NO! give him/her a popup in the
     * frontend saying that he/she needs to remove
     * all of his events.
     */
    
    //@Test
    public void removeUserWithEvents(){
        String uuid = creator.getUuid();
        
        t.begin();
        assertTrue(uDAO.getByUserName(creator.getUserName()).equals(creator));
        t.commit();
        
        t.begin();
        uDAO.remove(uuid);
        t.commit();
        
        t.begin();
        int count = uDAO.getCount();
        t.commit();
        
        System.out.println(count);
        
        assertTrue(count == 1);
        
    }
    
    /**
     * Deleting user with Venues. 
     * 
     * Should it be possible to remove a user with
     * active Venues? NO! give him/her a popup in the
     * frontend saying that he/she needs to remove
     * all of his venues.
     */
    
    //@Test
    public void removeUserWithVenues(){
        String uuid = creator.getUuid();
        
        t.begin();
        uDAO.remove(uuid);
        t.commit();
        
        t.begin();
        int count = uDAO.getCount();
        t.commit();
        
        System.out.println(count);
        
        assertTrue(count == 1);
    }
}