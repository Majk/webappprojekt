/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.webapp.bookemall.persistence;


import edu.webapp.bookemall.booking.Address;
import edu.webapp.bookemall.booking.Booking;
import edu.webapp.bookemall.booking.DateStamp;
import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.Venue;
import static edu.webapp.bookemall.persistence.TestPersistUser.birthDate;
import java.lang.reflect.Field;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.joda.time.LocalDate;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
public class TestPersistBooking {
    
    final static String PU = "bookemall_pu";
    final static String TEST_PU = "bookemall_test_pu";
    final static String EMBEDDED_TEST_PU = "bookemall_embedded_test_pu";
    static BookingDAO bDAO;
    static IUserDAO uDAO;
    static IVenueDAO vDAO;
    static EntityTransaction t;
    static Address address1;
    static Venue venue1;
    static User creator;
    static User guest;
    static User admin;
    static Booking booking;
    static DateStamp timeStamp;
    
    @BeforeClass
    public static void setUpClass() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        bDAO = new BookingDAO();
        vDAO = new VenueDAO();
        uDAO = new UserDAO();
        
        final EntityManager em = Persistence.createEntityManagerFactory(EMBEDDED_TEST_PU).createEntityManager();
        final Field f1 = bDAO.getClass().getDeclaredField("em");
        final Field f2 = vDAO.getClass().getDeclaredField("em");
        f1.setAccessible(true);
        f1.set(bDAO, em);
        f2.setAccessible(true);
        f2.set(vDAO, em);
        
        final Field f3 = uDAO.getClass().getDeclaredField("em");
        f3.setAccessible(true);
        f3.set(uDAO, em);
        
        t = em.getTransaction();

        creator = new User("creator", "password", "email@email.com");
        
        t.begin();
        uDAO.add(creator);
        t.commit();
        
        address1 = new Address("thisplace", 5, "Gothenburg", "Sweden");
        venue1 = new Venue(creator, address1, "Alberts Hole");
        
        t.begin();
        vDAO.add(venue1);
        t.commit();
        
        birthDate = new LocalDate(1970, 1, 20);
        
        
        guest = new User("AwesomeAlbinoApa3", "JagGillarBanan" , "Apa@MonkeyInternational.apa", "SexyMonkeyNiggahAssYoloSwag420Blazing.png", "Apa", "Apasson");
        admin = new User("AwesomeAlbinoApa4", "JagGillarBanan" , "Apa@MonkeyInternational.apa", "SexyMonkeyNiggahAssYoloSwag420Blazing.png", "Apa", "Apasson");

        t.begin();
        uDAO.add(guest);
        t.commit();
        
        t.begin();
        uDAO.add(admin);
        t.commit();
        
        timeStamp = new DateStamp(2013, 10, 5, 15, 41);
        booking = new Booking(timeStamp, creator);
        booking.setEventName("test");
        booking.setVenue(venue1);
        booking.addGuest(guest);
        booking.addAdmin(admin);
        
        t.begin();
        bDAO.add(booking);
        t.commit();
    }
    
    @AfterClass
    public static void tearDownClass() {
        bDAO.getEntityManager().getEntityManagerFactory().close();
        bDAO = null;
        vDAO = null;
        uDAO = null;
        creator = null;
        booking = null;
        timeStamp = null;
    }
    
    @Test
    public void testGetByCreator() {
        
        t.begin();
        List<Booking> result = bDAO.getByCreator(creator.getUuid());
        t.commit();
        
        assert(result.contains(booking));
    }
    
    @Test
    public void testGetByEventName() {
        
        t.begin();
        List<Booking> result = bDAO.getByEventName("test");
        t.commit();
        
        assertTrue(result.get(0).equals(booking));
    }
    
    @Test
    public void testGetByVenue() {
        
        t.begin();
        List<Booking> result = bDAO.getByVenue(venue1.getUuid());
        t.commit();
        
        assertTrue(result.get(0).equals(booking));
    }
    
    @Test
    public void testGetByAttender() {
        
        t.begin();
        List<Booking> result = bDAO.getByAttender(guest.getUuid());
        t.commit();
        
        assertTrue(result.get(0).equals(booking));
        
        t.begin();
        result = bDAO.getByAttender(admin.getUuid());
        t.commit();
        
        assertTrue(result.get(0).equals(booking));
        
        t.begin();
        result = bDAO.getByAttender(creator.getUuid());
        t.commit();
        
        assertTrue(result.get(0).equals(booking));
    }
    
    @Test
    public void testGetByAdmin() {
        t.begin();
        List<Booking> result = bDAO.getByAdmin(admin.getUuid());
        t.commit();
        assertTrue(result.get(0).equals(booking));
    }
    
    @Test
    public void testGetByGuest() {
        t.begin();
        List<Booking> result = bDAO.getByGuest(guest.getUuid());
        t.commit();
        assertTrue(result.get(0).equals(booking));
    }
    
    @Test
    public void testGetByDateStamp() {
        t.begin();
        List<Booking> result = bDAO.getByDateStamp(timeStamp.getUuid());
        t.commit();
        assertTrue(result.get(0).equals(booking));
    }
 }