package edu.webapp.bookemall.booking;

import java.util.UUID;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
public class UserTest {
    private User instance;
    private LocalDate birthDate;
    private DateTime now;
    
    @Before
    public void setUp() {
        birthDate = new LocalDate(1970, 1, 20);
        instance = new User("Mikael", "password" , "chalmers@hotmail.com", "this@this.com", "Mikael", "Stolpe");
        now = new DateTime();
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testRegisteredOn(){
        assertTrue(instance.getRegisteredOn() != null);
        instance.setRegisteredOn(now);
        String on = now.toString("yyyy-MM-dd'T'HH:mm");
        assertTrue(instance.getRegisteredOn().equals(now));
        
        assertTrue(instance.getRegistrationDate().equals(on));
    }
    
    
    
    /**
     * Testing using Null as userName argument to constructor causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testCopyConstructorNameNull() {
    	new User(null, "password", "email");
    }
    
    /**
     * Testing using Null as password argument to constructor causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testCopyConstructorPasswordNull() {
    	new User("user", null, "email");
    }
    
    /**
     * Testing using Null as email argument to constructor causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testCopyConstructorEmailNull() {
    	new User("user", "password", null);
    }
     
    /**
     * Testing setting UserName to Null causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetUserNameToNull() {
    	instance.setUserName(null);
    }
    
    /**
     * Testing setting Password to Null causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetPasswordToNull() {
    	instance.setPassword(null);
    }
    
    /**
     * Testing setting EncryptPassword to Null causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetEncryptPasswordToNull() {
    	instance.setAndEncryptPassword(null);
    }
    
    /**
     * Testing setting Email to Null causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetEmailToNull() {
    	instance.setEmail(null);
    }
    
    /**
     * Testing setting UUID to Null causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetUUIDToNull() {
    	instance.setUuid(null);
    }
    
    /**
     * Testing setting UserName to empty string causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetUserNameToEmptyString() {
    	instance.setUserName("");
    }
    
    /**
     * Testing setting Password to empty string causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetPasswordToEmptyString() {
    	instance.setPassword("");
    }
    
    /**
     * Testing setting EncryptPassword to empty string causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetEncryptPasswordToEmptyString() {
    	instance.setAndEncryptPassword("");
    }
    
    /**
     * Testing setting Email to empty string causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetEmailToEmptyString() {
    	instance.setEmail("");
    }
    
    /**
     * Testing setting UUID to empty string causes IllegalArgumentException
     */
    @Test (expected = IllegalArgumentException.class)
    public void testSetUUIDToEmptyString() {
    	instance.setUuid("");
    }
    
    /**
     * Test of getUUId method, of class User.
     */
    @Test
    public void testGetUUId() {
        String expResult = "111";
        instance.setUuid("111");
        String result = instance.getUuid();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setUUId method, of class User.
     */
    @Test
    public void testSetUUId() {
        String uuid = UUID.randomUUID().toString();
        instance.setUuid(uuid);
        assertEquals(uuid, instance.getUuid());
    }
    
    /**
     * Test of getName method, of class User.
     */
    @Test
    public void testGetName() {
        String expResult = "Mikael";
        String result = instance.getUserName();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setName method, of class User.
     */
    @Test
    public void testSetName() {
        String name = "Roffe";
        instance.setUserName(name);
        assertEquals(name, instance.getUserName());
    }
    
    /**
     * Test of getEmail method, of class User.
     */
    @Test
    public void testGetEmail() {
        String expResult = "chalmers@hotmail.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setEmail method, of class User.
     */
    @Test
    public void testSetEmail() {
        String email = "email@email.com";
        instance.setEmail(email);
        assertEquals(email, instance.getEmail());
    }
    
    /**
     * Test of getPicture method, of class User.
     */
    @Test
    public void testGetPicture() {
        String expResult = "this@this.com";
        String result = instance.getPicture();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setPicture method, of class User.
     */
    @Test
    public void testSetPicture() {
        String picture = "that@that.com";
        instance.setPicture(picture);
        assertEquals(picture, instance.getPicture());
    }
    
    /**
     * Test of getPassword method, of class User.
     */
    @Test
    public void testGetPassword() {
        String result = instance.getPassword();
        assertTrue(DigestUtils.sha512Hex("password").equals(result));
    }
    
    /**
     * Test of setPassword method, of class User.
     */
    @Test
    public void testSetPassword() {
        String pass = "hardpassword";
        instance.setPassword("hardpassword");
        assertEquals(pass, instance.getPassword());
    }
    
    /**
     * Test of setAndEncryptPassword method, of class User.
     */
    @Test
    public void testSetAndEncryptPassword() {
        instance.setAndEncryptPassword("hardpassword");
        assertTrue(DigestUtils.sha512Hex("hardpassword").equals(instance.getPassword()));
    }
    
    /**
     * Test of equals method, of class User.
     */
    
    @Test
    public void testEquals() {
        assertEquals(false, instance.equals(null));
        
        User user1  = new User("name", "pass", "email@email.com");
        assertEquals(user1.equals(instance), false);
        assertTrue (user1 != instance);
        
        User user2 = new User(instance.getUserName(), instance.getPassword(), instance.getEmail());
        user2.setUuid(instance.getUuid());
        assertEquals(user2.equals(instance), true);
        assertTrue (user2 != instance);
        
    }
    
    /**
     * Test null and empty strings entry of constructor
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNull() {
        new User(null, "pass", "email@email.com");
        new User("user", null, "email@email.com");
        new User("user", "pass", null);
        new User("    ", "pass", "email@email.com");
        new User(null, "      ", "email@email.com");
        new User(null, "pass", "       ");
    }
    
    /**
     * Test of setters and getters for firstName, lastName and admin
     */
    @Test
    public void testSimple() {
        assertEquals(instance.getFirstName(), "Mikael");
        instance.setFirstName("Majk");
        assertEquals(instance.getFirstName(), "Majk");
        assertEquals(instance.getLastName(), "Stolpe");
        instance.setFirstName("Stolpina");
        assertEquals(instance.getFirstName(), "Stolpina");
        assertFalse(instance.isAdmin());
        instance.setAdmin();
        assertTrue(instance.isAdmin());       
    }
    
}