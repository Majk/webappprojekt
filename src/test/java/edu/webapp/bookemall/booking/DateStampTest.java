/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.webapp.bookemall.booking;

import org.junit.Test;

/**
 * Tests for Date class, mainly tests for exception throwing
 * for invalid field sets.
 * @author Johan W-Schützer <Johan.DSS@Gmail.com>
 */
public class DateStampTest {

    /**
     * Test of setYear method, of class Date.
     * Tests if day is changed from 29 to 28 when
     * year is switched from leap year to non leap year
     * and month is 2.
     */
    @Test
    public void testSetYear() {
        System.out.println("Testing setYear");
        DateStamp date = new DateStamp(2012, 2, 29, 15, 42);
        date.setYear(2013);
        assert(date.getDay() == 28);
    }
    
    /**
     * Test of setMonth method, of class Date.
     * Tests if day gets changed from 31 to 30
     * when the month is set from a month with
     * 31 days to a month with 30 days
     */
    @Test
    public void testSetMonth() {
        System.out.println("Testing setMonth");
        DateStamp date = new DateStamp(2012, 1, 31, 15, 42);
        date.setMonth(4);
        assert(date.getDay() == 30);
    }
    
    /**
     * Test of setMonth method, of class Date.
     * Tests if day gets changed to 29 when 
     * month is set to 2 and year is a leap year
     */
    @Test
    public void testSetMonth2() {
        System.out.println("Testing setMonth");
        DateStamp date = new DateStamp(2012, 1, 31, 15, 42);
        date.setMonth(2);
        assert(date.getDay() == 29);
    }
    
    /**
     * Test of setMonth method, of class Date.
     * Tests if day gets changed to 28 when 
     * month is set to 2 and year is a no leap year
     */
    @Test
    public void testSetMonth3() {
        System.out.println("Testing setMonth");
        DateStamp date = new DateStamp(2013, 1, 31, 15, 42);
        date.setMonth(2);
        assert(date.getDay() == 28);
    }
    
    /**
     * Test of setMonth method, of class Date.
     * Tests that a day doesn't change when 
     * it shouldn't
     */
    @Test
    public void testSetMonth4() {
        System.out.println("Testing setMonth");
        DateStamp date = new DateStamp(2013, 1, 4, 15, 42);
        date.setMonth(2);
        assert(date.getDay() == 4);
    }

    /**
     * Test of setMonth method, of class Date.
     * Test exception thrown for faulty month
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetMonth() {
        System.out.println("Testing setMonth");
        DateStamp date = new DateStamp(2013, 9, 21, 15, 42);
        date.setMonth(13);
    }
    
    /**
     * Test of setMonth method, of class Date.
     * Test exception thrown for faulty month
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetMonth2() {
        System.out.println("Testing setMonth");
        DateStamp date = new DateStamp(2013, 9, 21, 15, 42);
        date.setMonth(0);
    }

    /**
     * Test of setDay method, of class Date.
     * Test 29/2 for leap year
     */
    @Test
    public void testSetDay() {
        System.out.println("Testing setDay");
        DateStamp date = new DateStamp(2012, 2, 13, 15, 42);
        date.setDay(29);
        assert(date.getDay() == 29);
    }
    
    /**
     * Test of setDay method, of class Date.
     * Test exception thrown for faulty day
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetDay() {
        System.out.println("Testing setDay");
        DateStamp date = new DateStamp(2013, 2, 13, 15, 42);
        date.setDay(29);
    }
    
    /**
     * Test of setDay method, of class Date.
     * Test exception thrown for faulty day
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetDay2() {
        System.out.println("Testing setDay");
        DateStamp date = new DateStamp(2013, 4, 13, 15, 42);
        date.setDay(31);
    }
    
    /**
     * Test of setDay method, of class Date.
     * Test exception thrown for faulty day
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetDay3() {
        System.out.println("Testing setDay");
        DateStamp date = new DateStamp(2013, 1, 13, 15, 42);
        date.setDay(32);
    }

    /**
     * Test of setDay method, of class Date.
     * Test exception thrown for faulty day
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetDay4() {
        System.out.println("Testing setDay");
        DateStamp date = new DateStamp(2013, 1, 13, 15, 42);
        date.setDay(0);
    }

    /**
     * Test of setHour method, of class Date.
     * Test exception thrown for faulty hour
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetHour() {
        System.out.println("Testing setHour");
        DateStamp date = new DateStamp(2013, 1, 13, 15, 42);
        date.setHour(24);
    }
    
    /**
     * Test of setHour method, of class Date.
     * Test exception thrown for faulty hour
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetHour2() {
        System.out.println("Testing setHour");
        DateStamp date = new DateStamp(2013, 1, 13, 15, 42);
        date.setHour(-1);
    }

    /**
     * Test of setHour method, of class Date.
     * Test exception thrown for faulty minute
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetMinute() {
        System.out.println("Testing setHour");
        DateStamp date = new DateStamp(2013, 1, 13, 15, 42);
        date.setMinute(60);
    }
    
    /**
     * Test of setHour method, of class Date.
     * Test exception thrown for faulty minute
     */
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalSetMinute2() {
        System.out.println("Testing setHour");
        DateStamp date = new DateStamp(2013, 1, 13, 15, 42);
        date.setMinute(-1);
    }
}