package edu.webapp.bookemall.booking;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 * @author Johan W-Schützer <Johan.DSS@Gmail.com>
 */
public class BookingTest {

    private DateStamp dateStamp;
    private Booking instance;
    private User user1;
    private String uuid1;
    private User user2;
    private User creator;
    private String uuid2;
    private Venue venue;
    
    @Before
    public void setUp() {
        dateStamp = new DateStamp (2013,10,01,15,57);
        user1 = new User("testUser", "pass", "email");
        creator = new User("testUser", "pass", "email");
        instance =  new Booking(dateStamp, creator);
        uuid1 = "testUUId1";
        user1.setUuid(uuid1);
        user2 = new User("testUser", "pass", "email");
        uuid2 = "testUUId2";
        user2.setUuid(uuid2);
        venue = new Venue(new User(),new Address());
    }
    
    /**
     * Test of addNonAdmin method, of class Booking.
     * Tests if added properly
     */
    @Test
    public void testAddGuest() {
        System.out.println("Testing addNonAdmin");
        assert(instance.getGuests().isEmpty());
        instance.addGuest(user1);
        assert(instance.getGuests().size() == 1);
        instance.addGuest(null);
        assert(instance.getGuests().size() == 1);
        assert(instance.getGuests().contains(user1));
        int admins = instance.getAdmins().size();
        instance.addAdmin(user1);
        assert(instance.getAdmins().size() == admins);
    }

    /**
     * Test of addAdmin method, of class Booking.
     * Tests if added properly
     */
    @Test
    public void testAddAdmin() {
        System.out.println("Testing addAdmin");
        instance.addAdmin(user1);
        assert(instance.getAdmins().size() == 2);
        instance.addAdmin(null);
        assert(instance.getAdmins().size() == 2);
        assert(instance.getAdmins().contains(user1));
        int guests = instance.getGuests().size();
        instance.addGuest(user1);
        assert(instance.getGuests().size() == guests);
    }

    /**
     * Test of demote method, of class Booking.
     * Tests if removed from adminlist and added to nonadmin list
     */
    @Test
    public void testDemote() {
        System.out.println("Testing demote");
        instance.addAdmin(user1);
        instance.demote(user1.getUuid());
        assert(instance.getAdmins().size() == 1);
        assert(instance.getGuests().size() == 1);
        instance.demote(null);
        assert(instance.getAdmins().size() == 1);
        assert(instance.getGuests().size() == 1);
        assert(instance.getGuests().contains(user1));
    }

    /**
     * Test of promote method, of class Booking.
     * Tests if removed from nonadmin list and added to admin list
     */
    @Test
    public void testPromote() {
        System.out.println("Testing promote");
        instance.addGuest(user1);
        instance.promote(user1.getUuid());
        assert(instance.getGuests().isEmpty());
        assert(instance.getAdmins().size() == 2);
        instance.promote(null);
        assert(instance.getGuests().isEmpty());
        assert(instance.getAdmins().size() == 2);
        assert(instance.getAdmins().contains(user1));
    }

    /**
     * Test of removeUser method, of class Booking.
     * Tests if an added admin or nonadmin is removed properly
     */
    @Test
    public void testRemoveUser() {
        System.out.println("removeUser");
        instance.addAdmin(user1);
        instance.removeUser(user1);
        instance.removeUser(null);
        assert(instance.getAdmins().size() == 1);
        assert(instance.getAttenders().size() == 1);
        instance.addGuest(user1);
        instance.removeUser(user1);
        instance.removeUser(null);
        assert(instance.getGuests()).isEmpty();
        assert(instance.getAttenders()).size() == 1;
    }

    /**
     * Test of setVenue method, of class Booking.
     * Tests if venue is set properly
     */
    @Test
    public void testSetVenue() {
        System.out.println("Testing setVenue");
        instance.setVenue(venue);
        assertEquals(instance.getVenue(), venue);
    }

    /**
     * Test of getVenue method, of class Booking.
     * Tests if getVenue returns properly
     */
    @Test
    public void testGetVenue() {
        System.out.println("Testing getVenue");
        instance.setVenue(venue);
        Venue result = instance.getVenue();
        assertEquals(venue, result);
    }

    /**
     * Test of getAttenders method, of class Booking.
     * Tests if all attenders are retrieved properly
     */
    @Test
    public void testGetAttenders() {
        System.out.println("Testing getAttenders");
        instance.addAdmin(user1);
        instance.addAdmin(user2);
        assert(instance.getAttenders().size() == 3);
        assert(instance.getAttenders().contains(user1));
        assert(instance.getAttenders().contains(user2));
        
        instance.removeUser(user1);
        instance.removeUser(user2);
        instance.addAdmin(user1);
        instance.addGuest(user2);
        assert(instance.getAttenders().size() == 3);
        assert(instance.getAttenders().contains(user1));
        assert(instance.getAttenders().contains(user2));
        
        instance.removeUser(user1);
        instance.removeUser(user2);
        instance.addGuest(user1);
        instance.addGuest(user2);
        assert(instance.getAttenders().size() == 3);
        assert(instance.getAttenders().contains(user1));
        assert(instance.getAttenders().contains(user2));
    }

    /**
     * Test of getAdmins method, of class Booking.
     * Tests if admins are retrieved properly
     * Also tests if the creator is added to admins
     */
    @Test
    public void testGetAdmins() {
        System.out.println("Testing getAdmins");
        instance.addAdmin(user1);
        assert(instance.getAdmins().size() == 2);
        assert(instance.getAdmins().contains(user1));
    }

    /**
     * Test of getNonAdmins method, of class Booking.
     * Tests if nonAdmins are retrieved properly
     */
    @Test
    public void testGetGuests() {
        System.out.println("Testing getGuests");
        instance.addGuest(user1);
        assert(instance.getGuests().size() == 1);
        assert(instance.getGuests().contains(user1));
    }
}