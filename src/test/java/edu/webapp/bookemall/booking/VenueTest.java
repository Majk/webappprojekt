/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.webapp.bookemall.booking;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
public class VenueTest {
    private Venue instance;
    private User creator;
    private User admin1;
    private User admin2;
    private User admin3;
    
    @Before
    public void setUp() {
        creator = new User("username", "password", "email@email.com");
        admin1 = new User("admin1", "password", "email@email.com");
        admin2= new User("admin2", "password", "email@email.com");
        admin3 = new User("admin3", "password", "email@email.com");
        instance = new Venue(creator ,new Address("test",1,"TestTown","TestCity"));
                
        List<User> admin = new ArrayList<>();
        admin.add(admin1);
        admin.add(admin2);
        admin.add(admin3);
        instance.addAdmins(admin);
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of getAddress method, of class Venue.
     */
    @Test
    public void testGetAddress() {
        Address expResult = new Address("test",1,"TestTown","TestCity");
        Address result = instance.getAddress();
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of setAddress method, of class Venue.
     */
    @Test
    public void testSetAddress() {
        Address address = new Address("test",1,"TestTown","TestCity");
        instance.setAddress(address);
        assertEquals(address, instance.getAddress());
    }
    
    /**
     * Test of getId method, of class Venue.
     */
    @Test
    public void testGetUuid() {
        String expResult = "111";
        instance.setUuid(expResult);
        String result = instance.getUuid();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setId method, of class Venue.
     */
    @Test
    public void testSetId() {
        String UUId = UUID.randomUUID().toString();
        instance.setUuid(UUId);
        assertEquals(UUId, instance.getUuid());
    }
    
    /**
     * Test of getAdmins method, of class Venue.
     */
    @Test
    public void testGetAdmins() {
        List<User> expResult = new ArrayList<>();
        expResult.add(admin1);
        expResult.add(admin2);
        expResult.add(admin3);
        List<User> result = instance.getAdmins();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addAdmin method, of class Venue.
     */
    @Test
    public void testAddAdmin() {
        User newAdmin = new User("newAdmin", "password", "email@email.com");
        instance.addAdmin(newAdmin);
        assertTrue(instance.isAdmin(newAdmin));
    }
    
    /**
     * Test of isAdmin method, of class Venue.
     */
    @Test
    public void testIsAdmin() {
        User notAdmin = new User("notAdmin", "password", "email@email.com");
        boolean expResult = false;
        boolean result = instance.isAdmin(notAdmin);
        assertEquals(expResult, result);
        expResult = true;
        result = instance.isAdmin(admin1);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addAdminExists method, of class Venue.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddExistingAdmin() {
        instance.addAdmin(admin1);
    }
    
    /**
     * Test of addAdmins, of class Venue
     */
    @Test
    public void testAddAdmins() {
        List<User> newAdmins = new ArrayList<>();
        User newAdmin = new User("newAdmin", "password", "email@email.com");
        newAdmins.add(admin1);
        newAdmins.add(newAdmin);
        int expResult = 1;
        assertEquals(expResult, instance.addAdmins(newAdmins));
        assertEquals(instance.getAdmins().size(), 4);
    }
    
    /**
     * Test null check of addAdmins, of class Venue
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddNullAdmins() {
        instance.addAdmins(null);
    }
    /**
     * Test empty check of addAdmins, of class Venue
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddEmptyAdmins() {
        instance.addAdmins(new ArrayList<User>());
    }
    
    /**
     * Test null check of addAdmins, of class Venue
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddNullAdmin() {
        instance.addAdmin(null);
    }
    
    /**
     * Test equals, of class Venue
     */
    @Test
    public void testNotEquals() {
        Venue otherInstance = new Venue(admin1, new Address("test",1,"TestTown","TestCity"));
        assertFalse(otherInstance.equals(instance));
    }
    
    /**
     * Test equals, of class Venue
     */
    @Test

    public void testEquals() {
        Venue otherInstance = new Venue(instance.getCreator(), instance.getAddress());
        otherInstance.setUuid(instance.getUuid());
        assertTrue(otherInstance.equals(instance));
    }
    
    
}