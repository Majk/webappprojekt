cURL tests for Web Service in package services. Methods which require @Context is NOT tested with cURL.

Each test assumes the tester knows what is in the database such that they can verify the results.
First curl-command on each method is syntax the second is an example. The text within {} describe which paramter you need to input for each command to test the service method.

Tests for VenueService. 


getById
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/byId/{UUID of Venue}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/byId/d69a0522-7217-43c9-b79e-87fbe744f660

getByAdmin
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/byAdmin/{UUID of User}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/byAdmin/0cbc285d-b374-4925-9618-684aa97c2d74

getAdmins
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/getAdmins/{UUID of Venue}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/getAdmins/d69a0522-7217-43c9-b79e-87fbe744f660

getByVenueName
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/byName/{name to search for}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/byName/chalm

getByNumberOfSeats
curl -k -H "Accept: application/json" -v "https://localhost:8181/webappprojekt/rs/venue/bySeats?from{>= value}&to={<= value}"
curl -k -H "Accept: application/json" -v "https://localhost:8181/webappprojekt/rs/venue/bySeats?from0&to=15"

getByAddress
curl -k -H "Accept: application/json" -v "https://localhost:8181/webappprojekt/rs/venue/byAddress?street={}&number={}&city={}&country={}"
curl -k -H "Accept: application/json" -v "https://localhost:8181/webappprojekt/rs/venue/byAddress?street=Chalmers&number=5666&city=G�tet&country=Sv�rge"

getAll
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/venue/


Tests for UserService

getUserById
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/user/{UUID of User}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/user/0cbc285d-b374-4925-9618-684aa97c2d74

Tests for BookingService


getAll
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/

getBookingById
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/byId/{UUID of Booking}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/byId/eb9c89de-0386-4a2c-bc7b-7e2299f1ce31

getBookingByAttender
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/byAttenderId/{UUID of attender(user)}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/byAttenderId/0cbc285d-b374-4925-9618-684aa97c2d74

getBookingByVenue
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/byVenueId/{UUID of Venue}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/byVenueId/d69a0522-7217-43c9-b79e-87fbe744f660

getBookingByEventName
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/byName/{Name of Event}
curl -k -H "Accept: application/json" -v https://localhost:8181/webappprojekt/rs/booking/byName/FestU