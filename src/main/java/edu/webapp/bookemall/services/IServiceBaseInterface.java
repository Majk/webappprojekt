package edu.webapp.bookemall.services;

public interface IServiceBaseInterface {
    String PU = "bookemall_pu";
    String TEST_PU = "bookemall_test_pu";
    String EMBEDDED_TEST_PU = "bookemall_embedded_test_pu";
    int FORBIDDEN = 400;
    int NO_CONTENT = 204;
}
