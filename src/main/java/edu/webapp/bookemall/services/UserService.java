package edu.webapp.bookemall.services;

import edu.webapp.bookemall.booking.Booking;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.Venue;
import edu.webapp.bookemall.persistence.IBookingDAO;
import edu.webapp.bookemall.persistence.IUserDAO;
import edu.webapp.bookemall.persistence.IVenueDAO;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.GenericEntity;

/**
 *
 * I changed the URI-pattern in web.xml to: /#!/rs/* for this service.
 * Everything after /#!/ will run through the filter now, i.e you need
 * to be logged on to watch/use this. /Gustaf
 *
 * @author Viktor Sjölind
 */
@LocalBean
@Stateless
@Path("user")
public class UserService extends AbstractService implements IUserService{
    
    private static final int FORBIDDEN = 400;
    @EJB
    private IUserDAO DAO;
    @EJB
    private IVenueDAO venueDAO;
    @EJB
    private IBookingDAO bookingDAO;
    
    
    @GET
    @Path("/getAll/")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getAll() {
        logger.log(Level.INFO, "Entering getAll");

        List<User> result = DAO.getAll();
        GenericEntity<List<User>> ge = new GenericEntity<List<User>>(result) {
        };
        return Response.ok(ge).build();
    }
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getUser() {
        logger.log(Level.INFO, "Entering getUser()");
        String username = getUserName();
        User user = DAO.getByUserName(username);
        
        if (!checkValidUser(user)) {
            return Response.noContent().build();
        }
        
        logger.log(Level.INFO, "user: "+user);
        logger.log(Level.INFO, "Exiting getUser()");
        return Response.ok(user).build();
    }
    
    @GET
    @Path("{uuid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getUserById(@PathParam("uuid") String uuid) {
        logger.log(Level.INFO, "Entering getUserById()");
        if (uuid == null || uuid.isEmpty()) {
            return Response.status(FORBIDDEN).build();
        }
        
        User user = null;
        
        for (User u : getDAO().getAll()) {
            if (u.getUuid().equals(uuid)) {
                user = u;
            }
        }
        
        if (!checkValidUser(user)) {
            return Response.noContent().build();
        }

        logger.log(Level.INFO, "user: "+user);
        logger.log(Level.INFO, "Exiting getUserById()");
        return Response.ok(user).build();
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response updateUser(JAXBElement<User> user) {
        logger.log(Level.INFO,"Entering updateUser()");
        
        String username = getUserName();
        User me = getDAO().getByUserName(username);
 
        if (!checkValidUser(user.getValue())) {
            return Response.status(FORBIDDEN).entity("Invalid user!").build();
        }
        
        String uuid = user.getValue().getUuid();
        if (uuid == null || uuid.isEmpty() || !uuid.equals(me.getUuid())) {
            return Response.status(FORBIDDEN).entity("Invalid uuid or not trying to update self!").build();
        }
        User oldUser = getDAO().find(uuid);
        User updatedUser = user.getValue();
        // have to access the children list before to load it into the cache, if this isnt done it will delete the children
        updatedUser.setBookings(oldUser.getBookings());
        updatedUser.setVenues(oldUser.getVenues());
        getDAO().update(user.getValue());
        
        logger.log(Level.INFO,"Exiting updateUser()");
        return Response.ok().build();
        
    }
    
    @DELETE
    @Path("{uuid}")
    public Response deleteUser(@PathParam("uuid") String uuid) {
        logger.log(Level.INFO,"Entering deleteUser()");
        
        String username = getUserName();
        User me = getDAO().getByUserName(username);
        
        if (uuid == null || uuid.isEmpty() || !uuid.equals(me.getUuid())) {
            return Response.status(FORBIDDEN).entity("Not trying to delete self.").build();
        }
        
        Object obj = getUserById(uuid).getEntity();
        if (obj == null) {
            return Response.noContent().build();
        }
        User user = (User) obj;
        
        if (!checkValidUser(user)) {
            return Response.status(FORBIDDEN).build();
        }
        
        // need to remove user from admin and attender in venue and booking before deletion
        for(Venue v: venueDAO.getByAdmin(me)) {
            v.removeAdmin(me);
        }
        for(Booking b: bookingDAO.getByAttender(me.getUuid())) {
            b.removeUser(me);
        }
        getDAO().remove(user.getUuid());
        
        logger.log(Level.INFO,"Exiting deleteUser()");
        return Response.ok().build();
    }
    
    public IUserDAO getDAO() {
        return DAO;
    }
    
    private boolean checkValidUser(User user) {
        return (user != null
                && user.getUuid() != null && !user.getUuid().isEmpty()
                && user.getUserName() != null && !user.getUserName().isEmpty()
                && user.getPassword() != null && !user.getPassword().isEmpty());
    }
}
