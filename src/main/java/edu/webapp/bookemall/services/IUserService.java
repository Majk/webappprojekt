package edu.webapp.bookemall.services;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import edu.webapp.bookemall.booking.User;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

public interface IUserService extends IServiceBaseInterface{
    
    public Response getUserById(@PathParam("uuid") String uuid);
    
    public Response getAll();
    
    public Response updateUser(JAXBElement<User> user);
    
    public Response deleteUser(@PathParam("uuid") String uuid);
    
    
}
