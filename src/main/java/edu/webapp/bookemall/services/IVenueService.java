package edu.webapp.bookemall.services;

import edu.webapp.bookemall.booking.Venue;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

public interface IVenueService extends IServiceBaseInterface {
    
    public String NAMEERROR = "There already exists one Venue with this name, please choose another";
    public String SEATSERROR = "Seats need to be a positive integer";
    
    /**
     * Retrieves a venue from with a specific uuid
     * @param uuid of the venue to return
     * @return either the found venue or a forbidden status code
     */
    public Response getById(@PathParam("uuid") String uuid);
    /**
     * Retrieves a list of all users that are not admins or creator of a specific venue
     * @param venueUuid of the venue to check
     * @param req The Context
     * @return List of all Users that are not admins or creator
     */
    public Response getInvokeList(@PathParam("venueUuid") String venueUuid);
    /**
     * Gets all the venues a users is admin for
     * @param uuid user uuid
     * @return List of all venues a user is admin for
     */
    public Response getByAdmin(@PathParam("uuid") String uuid);
    /**
     * Returns all admins of a venue
     * @param uuid venue to check
     * @return all admins of a venue
     */
    public Response getAdmins(@PathParam("uuid") String uuid);
    /**
     * Gets all the venues the logged in user is not admin or creator of
     * @param req The Context
     * @return List of all venues that matches
     */
    public Response getRegular();
    /**
     * Returns all venues which match a name
     * @param venueName name string to look for
     * @return All matching venues
     */
    public Response getByVenueName(@PathParam("venueName") String venueName);
    /**
     * Returns all venues which seat nr match a speciif range
     * @param from from nr of seats
     * @param to to nr of seats
     * @return All matching venues
     */
    public Response getByNumberOfSeats(@QueryParam("from") int from, @QueryParam("to") int to);
    /**
     * Returns all venues that match a certain address
     * @param street street of address
     * @param number house number of addres
     * @param city city of address
     * @param country country of address
     * @return All matching venues
     */
    public Response getByAddress(@QueryParam("street") String street, @QueryParam("number") int number, @QueryParam("city") String city, @QueryParam("country") String country);
    /**
     * Gets all the venues
     * @return All venues
     */
    public Response getAll();
    /**
     * Gets all venues the logged in user is creator of
     * @param req The Context
     * @return All matching venues
     */
    public Response getByCreator();
    /**
     * Adds a venue
     * @param venue venue to add
     * @param req The Context
     * @return Ok status if successful
     */
    public Response addVenue(Venue venue);
    /**
     * Remove a venue
     * @param uuid Uuid of venue to remove
     * @param req The Context
     * @return Ok status if successful
     */
    public Response deleteVenue(@PathParam("uuid") String uuid);
    /**
     * Updates a venue
     * @param venue New values of a venue
     * @param req The context
     * @return Ok status if successful
     */
    public Response updateVenue(Venue venue);
    /**
     * Removes the admin status a user has of a venue
     * @param venueUuid Uuid of venue
     * @param adminUuid Uuid of admin
     * @param req The Context
     * @return Ok status if successful
     */
    public Response revokeAdminStatus(@PathParam("venueUuid") String venueUuid, @PathParam("adminUuid") String adminUuid);
    /**
     * Grants admin status of a venue to a user
     * @param venueUuid Uuid of venue
     * @param adminUuid Uuid of admin
     * @param req The Context
     * @return Ok status if successful
     */
    public Response invokeAdminStatus(@PathParam("venueUuid") String venueUuid, @PathParam("adminUuid") String adminUuid);
    
    
    
}
