package edu.webapp.bookemall.services;

import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;

/**
 *
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
class AbstractService {
    
    protected Logger logger = Logger.getAnonymousLogger();
    @Context
    protected HttpServletRequest req;
    
    
    /**
     * Checks if a string is valid
     * @param check String to check
     * @return true if invalid
     */
    protected boolean checkInvalidString(String check) {
        return check == null || check.trim().length() == 0;
    }
    
    /**
     * Checks a range of Strings for their validity
     * @param args Strings to check
     * @return  true if any of them fail
     */
    protected boolean checkInvalidStrings(String... args) {
        boolean result = false;
        for(String s : args) {
            result |= checkInvalidString(s);
        }
        return result;
    }
    /**
     * Returns the userName contained in the @param
     * @return Username
     */
    protected String getUserName() {
        HttpSession session = req.getSession(true);
        Object attr = session.getAttribute("USER");
    	return attr.toString();
    }
}
