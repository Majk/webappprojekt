package edu.webapp.bookemall.services;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import edu.webapp.bookemall.booking.Booking;
import edu.webapp.bookemall.booking.User;

/**
 * Interface for BookingService
 * 
 * @author Johan W-Schützer <Johan.DSS@Gmail.com>
 */
public interface IBookingService extends IServiceBaseInterface {
    
    /**
     * Gets all bookings
     * Returns a Response with an embedded list of bookings
     */
    public Response getAll();
    /**
     * Gets a booking by id
     * Returns a Response with an embedded list of bookings
     * @param uuid the uuid of the booking to be fetched
     */
    public Response getBookingById(@PathParam("uuid") String uuid);
    /**
     * Gets all bookings attended by the user with matching uuid
     * Returns a Response with an embedded booking
     * @param uuid the uuid of the attender to be matched
     */
    public Response getBookingByAttender(@PathParam("uuid") String uuid);
    /**
     * Gets all bookings created by the logged in user
     * Returns a Response with an embedded list of bookings
     */
    public Response getBookingByCreator();
    /**
     * Gets all bookings by the venue with matching uuid
     * Returns a Response with an embedded list of bookings
     * @param uuid the uuid of the venue to be matched
     */
    public Response getBookingByVenue(@PathParam ("uuid") String uuid);
    /**
     * Gets all bookings with the matching eventName
     * Returns a Response with an embedded list of bookings
     * @param eventName the eventname to be matched
     */
    public Response getBookingByEventName(@PathParam("eventName") String eventName);
    /**
     * Adds the given admin the the booking with matching uuid
     * Returns a Response with an embedded list of bookings
     * @param uuid the uuid of the booking
     * @param admin the admin to be added
     */
    public Response addAdmin(@PathParam("uuid") String uuid, JAXBElement<User> admin);
    /**
     * Adds the given guest the the booking with matching uuid
     * Returns ok if successful
     * @param uuid the uuid of the booking
     * @param guest the guest to be added
     */
    public Response addGuest(@PathParam("uuid") String uuid, JAXBElement<User> guest);
    /**
     * Adds the logged in user to the booking with matching uuid
     * Returns ok if successful
     * @param bookingID the uuid of the booking to be attended
     */
    public Response attendBooking(@PathParam("uuid") String bookingID);
    /**
     * Removes the logged in user from the booking with matching uuid
     * Returns ok if successful
     * @param bookingID the uuid of the booking to be unattended
     */
    public Response unAttendBooking(@PathParam("uuid") String bookingID);
    /**
     * Demotes an existing admin to guest in the booking with matching uuid
     * Returns ok if successful
     * @param bUuid the uuid of the booking
     * @param aUuid the uuid of the admin to be demoted
     */
    public Response demoteAdmin(@PathParam("bookingUuid") String bUuid, @PathParam("adminUuid") String aUuid);
    /**
     * Promotes an existing guest to admin in the booking with matching uuid
     * Returns ok if successful
     * @param bUuid the uuid of the booking
     * @param gUuid the uuid of the guest to be promoted
     */
    public Response promoteGuest(@PathParam("bookingUuid") String bUuid, @PathParam("guestUuid") String gUuid);
    /**
     * Removes an attender with matching uuid from the booking with matching uuid
     * Returns ok if successful
     * @param bookingUuid the uuid of the booking
     * @param attenderUuid the uuid of the attender to be removed
     */
    public Response removeAttender(@PathParam("bookingUuid") String bookingUuid, @PathParam("attenderUuid") String attenderUuid);
    /**
     * Adds the given booking to the database
     * Returns ok if successful
     * @param booking the booking to be added
     */
    public Response addBooking(Booking booking);
    /**
     * Updates an existing booking in the database
     * Returns ok if successful
     * @param booking the booking with the updated values
     */
    public Response updateBooking(Booking booking);
    /**
     * Deletes an existing booking from the database
     * Returns ok if successful
     * @param uuid the uuid of the booking to be deleted
     */
    public Response deleteBooking(@PathParam("uuid") String uuid);
}
