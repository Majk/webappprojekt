package edu.webapp.bookemall.services;

import edu.webapp.bookemall.booking.Address;
import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.Venue;
import edu.webapp.bookemall.persistence.IUserDAO;
import edu.webapp.bookemall.persistence.IVenueDAO;
import static edu.webapp.bookemall.services.IServiceBaseInterface.FORBIDDEN;
import static edu.webapp.bookemall.services.IVenueService.NAMEERROR;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * The WebService for venues, handles calls from frontend to backend
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
@Stateless
@LocalBean
@Path("venue")
public class VenueService extends AbstractService implements IVenueService{
    
    /**
     * Inject DAOS
     */
    @EJB
    private IVenueDAO venueDAO;
    @EJB
    private IUserDAO userDAO;
    

    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getAll() {
        logger.log(Level.INFO, "Entering getAll");
        List<Venue> result = venueDAO.getAll();
        GenericEntity<List<Venue>> ge = new GenericEntity<List<Venue>>(result) {
        };
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("getAdmins/{uuid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getAdmins(@PathParam("uuid") String uuid) {
        logger.log(Level.INFO, "Entering getAdmins");
        if(checkInvalidString(uuid)) {
            return Response.status(FORBIDDEN).build();
        }
        Venue v = venueDAO.find(uuid);
        if(v == null) {
            return Response.status(FORBIDDEN).build();
        }
        List<User> admins = v.getAdmins();
        GenericEntity<List<User>> ge = new GenericEntity<List<User>>(admins) {
        };
        logger.log(Level.INFO, "Exiting getAdmins");
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("/regular/")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getRegular() {
        logger.log(Level.INFO, "Entering getRegular");
        String userName = getUserName();
        logger.log(Level.INFO, "Logged in user : " + userName);
        if(checkInvalidString(userName)) {
            return Response.status(FORBIDDEN).build();
        }
        User current = userDAO.getByUserName(userName);
        List<Venue> regular = venueDAO.getAll();
        regular.removeAll(venueDAO.getByCreator(current));
        regular.removeAll(venueDAO.getByAdmin(current));
        GenericEntity<List<Venue>> ge = new GenericEntity<List<Venue>>(regular) {
        };
        logger.log(Level.INFO, "Exiting getRegular");
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("/getInvokeList/{venueUuid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getInvokeList(@PathParam("venueUuid") String venueUuid) {
        logger.log(Level.INFO, "Entering getInvokeList");
        String userName = getUserName();
        logger.log(Level.INFO, "Logged in user : " + userName);
        if(checkInvalidString(userName)) {
            return Response.status(FORBIDDEN).build();
        }
        Venue venue = venueDAO.find(venueUuid);
        User current = userDAO.getByUserName(userName);
        List<User> invokeList = userDAO.getAll();
        //remove calling user
        invokeList.remove(current);
        //remove creator
        invokeList.remove(venue.getCreator());
        //remove admins
        invokeList.removeAll(venue.getAdmins());
        GenericEntity<List<User>> ge = new GenericEntity<List<User>>(invokeList) {
        };
        logger.log(Level.INFO, "Exiting getInvokeList");
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("byId/{uuid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getById(@PathParam("uuid") String uuid) {
        logger.log(Level.INFO, "Entering getById");
        if(checkInvalidString(uuid)) {
            return Response.status(FORBIDDEN).build();
        }
        Venue venue = venueDAO.find(uuid);
        if(venue == null) {
            return Response.status(FORBIDDEN).build();
        }
        
        return Response.ok(venue).build();
        
    }
    
    @GET
    @Path("byAdmin/{uuid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getByAdmin(@PathParam("uuid") String uuid) {
        if (checkInvalidString(uuid)){
            return Response.status(FORBIDDEN).build();
        }
        
        List<Venue> result = venueDAO.getByAdmin(userDAO.find(uuid));
        
        GenericEntity<List<Venue>> ge = new GenericEntity<List<Venue>>(result) {
        };
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("byName/{venueName}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getByVenueName(@PathParam("venueName") String venueName) {
        if(checkInvalidString(venueName)) {
            return Response.status(FORBIDDEN).build();
        }
        List<Venue> result = venueDAO.getByVenueName(venueName);
        
        GenericEntity<List<Venue>> ge = new GenericEntity<List<Venue>>(result) {
        };
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("bySeats")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getByNumberOfSeats(@QueryParam("from") int from, @QueryParam("to") int to) {
        if(from < 0 || to < 0 || to < from) {
            return Response.status(FORBIDDEN).build();
        }
        List<Venue> result = venueDAO.getByNumberOfSeats(from, to);
        GenericEntity<List<Venue>> ge = new GenericEntity<List<Venue>>(result) {
        };
        return Response.ok(ge).build();
    }
    
    
    @GET
    @Path("byAddress")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getByAddress(@QueryParam("street") String street, @QueryParam("number") int number, @QueryParam("city") String city, @QueryParam("country") String country) {
        if(checkInvalidString(street) && checkInvalidString(city) && checkInvalidString(country) && number < 0) {
            return Response.status(FORBIDDEN).build();
        }
        
        List<Venue> result = venueDAO.getByAddress(new Address(street, number, city, country));
        if(result.isEmpty()) {
            return Response.status(FORBIDDEN).build();
        }
        GenericEntity<List<Venue>> ge = new GenericEntity<List<Venue>>(result) {
        };
        return Response.ok(ge).build();
    }
    
    
    @GET
    @Path("byCreator")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getByCreator() {
        String userName = getUserName();
        logger.log(Level.INFO, "Entering getByCreator");
        logger.log(Level.INFO, "Logged in user : " + userName);
        logger.log(Level.INFO, "Trying to get venues by creator : " + userName);
        if(checkInvalidString(userName)) {
            return Response.status(FORBIDDEN).build();
        }
        User creator = userDAO.getByUserName(userName);
        List<Venue> result = venueDAO.getByCreator(creator);
        GenericEntity<List<Venue>> ge = new GenericEntity<List<Venue>>(result) {
        };
        logger.log(Level.INFO, "Exiting getByCreator");
        return Response.ok(ge).build();
    }
    
    @POST
    @Path("add")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
    @Override
    public Response addVenue(Venue venue) {
        String userName = getUserName();
        logger.log(Level.INFO, "Logged in user : " + userName);
        logger.log(Level.INFO, "Trying to add venue with name : " + venue.getVenueName());
        logger.log(Level.INFO, "venue = " + venue);
        if(checkInvalidStrings(userName, venue.getVenueName())) {
            return Response.status(FORBIDDEN).build();
        }
        logger.log(Level.INFO, "venueDAO = " + venueDAO);
        // check if name exists already
        Venue v = venueDAO.getByExactVenueName(venue.getVenueName());
        logger.log(Level.INFO, "venues = " + venue);
        if(v != null) {
            return Response.status(FORBIDDEN).entity(NAMEERROR).build();
        }
        
        if(venue.getSeats() < 0) {
            return Response.status(FORBIDDEN).build();
        }
        
        User creator = userDAO.getByUserName(userName);
        
        if(creator == null) {
            Logger.getAnonymousLogger().log(Level.INFO, "Creator is null");
            return Response.status(FORBIDDEN).build();
        }
        
        logger.log(Level.INFO, "Found user with username : " + creator.getUserName());
        venue.setCreator(creator);
        venueDAO.add(venue);
        return Response.ok().build();
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
    @Path("update")
    @Override
    public Response updateVenue (Venue venue) {
        logger.log(Level.INFO, "Entering update venue");
        String userName = getUserName();
        if(checkInvalidString(userName) || venue == null) {
            return Response.status(FORBIDDEN).build();
        }
        
        logger.log(Level.INFO, "Username is: " + userName);
        // check if venue exists
        Venue v = venueDAO.find(venue.getUuid());
        if(v == null) {
            return Response.status(FORBIDDEN).build();
        }
        // check if new name is different, in that case check if it exists already
        logger.log(Level.INFO, "Venue found: " + v.getVenueName());
        if(!v.getVenueName().equals(venue.getVenueName())) {
            Venue checkName = venueDAO.getByExactVenueName(venue.getVenueName());
            if(checkName != null) {
                return Response.status(FORBIDDEN).entity(NAMEERROR).build();
            }
        }
        if(venue.getSeats() < 0) {
            return Response.status(FORBIDDEN).entity(SEATSERROR).build();
        }
        
        logger.log(Level.INFO, "Venue found: " + v.getVenueName());
        User current = userDAO.getByUserName(userName);
        logger.log(Level.INFO, "Creator?  " + v.isCreator(current));
        logger.log(Level.INFO, "Admin? " + v.isAdmin(current));
        if(!v.isCreator(current) && !v.isAdmin(current)) {
            return Response.status(FORBIDDEN).build();
        }
        // have to access the children list before to load it into the cache, if this isnt done it will delete the children
        venue.setBookings(v.getBookings());
        logger.log(Level.INFO, "Everything ok updating venue ");
        venueDAO.update(venue);
        logger.log(Level.INFO, "Exiting update venue");
        return Response.ok().build();
        
    }
    
    @DELETE
    @Path("delete/{uuid}")
    @Override
    public Response deleteVenue(@PathParam("uuid") String uuid) {
        logger.log(Level.INFO, "Entering deleteVenue");
        String userName = getUserName();
        if(checkInvalidString(uuid) && checkInvalidString(userName)) {
            return Response.status(FORBIDDEN).build();
        }
        Venue delete = venueDAO.find(uuid);
        logger.log(Level.INFO, "Found venue with name: " + delete.getVenueName());
        User currentUser = userDAO.getByUserName(userName);
        logger.log(Level.INFO, "Found user with name" + currentUser.getUserName());
        if(delete == null || !delete.isCreator(currentUser)) {
            return Response.status(FORBIDDEN).build();
        }
        
        logger.log(Level.INFO, "Everything ok, remoing venue");
        venueDAO.remove(uuid);
        
        
        logger.log(Level.INFO, "Exiting deleteVenue");
        return Response.ok().build();
    }
    
    @PUT
    @Path("revokeAdmin/{venueUuid}/{adminUuid}")
    @Override
    public Response revokeAdminStatus(@PathParam("venueUuid") String venueUuid, @PathParam("adminUuid") String adminUuid) {
        logger.log(Level.INFO, "Entering revokeAdminStatus");
        String userName = getUserName();
        if(checkInvalidString(venueUuid) && checkInvalidString(userName) && checkInvalidString(adminUuid)) {
            return Response.status(FORBIDDEN).build();
        }
        Venue venue = venueDAO.find(venueUuid);
        User uester = userDAO.getByUserName(userName);
        User admin = userDAO.find(adminUuid);
        
        
        if(venue == null || uester == null || admin == null) {
            return Response.status(FORBIDDEN).build();
        }
        logger.log(Level.INFO, "All objects received from database correct");
        if(!venue.isAdmin(uester) && !venue.isCreator(uester)) {
            return Response.status(FORBIDDEN).build();
        }
        logger.log(Level.INFO, "Requester has propper status");
        
        if(!venue.isAdmin(admin)) {
            return Response.status(FORBIDDEN).build();
        }
        logger.log(Level.INFO, "User is admin, revoking status");
        venue.removeAdmin(admin);
        venueDAO.update(venue);
        logger.log(Level.INFO, "Exiting revokeAdminStatus");
        return Response.ok().build();
    }
    
    @PUT
    @Path("invokeAdmin/{venueUuid}/{adminUuid}")
    @Override
    public Response invokeAdminStatus(@PathParam("venueUuid") String venueUuid, @PathParam("adminUuid") String adminUuid) {
        logger.log(Level.INFO, "Entering invokeAdminStatus");
        String userName = getUserName();
        if(checkInvalidString(venueUuid) && checkInvalidString(userName) && checkInvalidString(adminUuid)) {
            return Response.status(FORBIDDEN).build();
        }
        Venue venue = venueDAO.find(venueUuid);
        User uester = userDAO.getByUserName(userName);
        User admin = userDAO.find(adminUuid);
        
        if(venue == null || uester == null || admin == null) {
            return Response.status(FORBIDDEN).build();
        }
        logger.log(Level.INFO, "All objects received from database correct");
        if(!venue.isAdmin(uester) && !venue.isCreator(uester)) {
            return Response.status(FORBIDDEN).build();
        }
        logger.log(Level.INFO, "Requester has propper status");
        
        if(venue.isAdmin(admin)) {
            return Response.status(FORBIDDEN).build();
        }
        logger.log(Level.INFO, "User is not admin already, invoking status");
        venue.addAdmin(admin);
        venueDAO.update(venue);
        logger.log(Level.INFO, "Exiting invokeAdminStatus");
        return Response.ok().build();
    }
    
}