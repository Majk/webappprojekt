package edu.webapp.bookemall.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.Booking;
import edu.webapp.bookemall.persistence.IBookingDAO;
import edu.webapp.bookemall.persistence.IUserDAO;
import static edu.webapp.bookemall.services.IServiceBaseInterface.FORBIDDEN;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.GenericEntity;

/**
 *
 * Service for Booking for handling incoming http requests
 *
 * @author Johan W-Schützer <Johan.DSS@Gmail.com>
 */
@Stateless
@LocalBean
@Path("booking")
public class BookingService extends AbstractService implements IBookingService {
    
    @EJB
    private IBookingDAO DAO;
    @EJB
    private IUserDAO userDAO;
    private static Logger logger = Logger.getAnonymousLogger();
    
    @GET
    @Path("byId/{uuid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getBookingById(@PathParam("uuid") String uuid) {
        
        if (uuid == null || uuid.isEmpty()) {
            return Response.status(FORBIDDEN).build();
        }
        
        Booking booking = DAO.find(uuid);
        
        if (!checkValidBooking(booking)) {
            return Response.noContent().build();
        }
        
        return Response.ok(booking).build();
    }
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getAll() {
        
        List<Booking> bookings = DAO.getAll();
        
        GenericEntity<List<Booking>> ge = new GenericEntity<List<Booking>>(bookings){};
        
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("byAttenderId/{uuid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getBookingByAttender(@PathParam ("uuid") String uuid) {
        Logger.getAnonymousLogger().log(Level.INFO, "entering getBookingByAttender");
        if (uuid == null || uuid.isEmpty()) {
            return Response.status(FORBIDDEN).build();
        }
        
        Logger.getAnonymousLogger().log(Level.INFO, "getting bookings by attender");
        List<Booking> bookings = DAO.getByAttender(uuid);
        Logger.getAnonymousLogger().log(Level.INFO, "Got bookings: " + bookings);
        List<Booking> inValid = new ArrayList<>();
        for (Booking booking : bookings) {
            if (!checkValidBooking(booking)) {
                inValid.add(booking);
            }
        }
        bookings.removeAll(inValid);
        
        if (bookings.isEmpty()) {
            return Response.noContent().build();
        }
        
        Logger.getAnonymousLogger().log(Level.INFO, "Remaining bookings after clean: " + bookings);
        
        GenericEntity<List<Booking>> ge = new GenericEntity<List<Booking>>(bookings){};
        
        Logger.getAnonymousLogger().log(Level.INFO, "exiting getBookingByAttender");
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("byCreator")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getBookingByCreator() {
        String userName = getUserName();
        logger.log(Level.INFO, "Entering getByCreator");
        logger.log(Level.INFO, "Logged in user : " + userName);
        if(checkInvalidString(userName)) {
            return Response.status(FORBIDDEN).build();
        }
        
        logger.log(Level.INFO, "Trying to get creator : " + userName);
        User creator = userDAO.getByUserName(userName);
        logger.log(Level.INFO, "Got creator : " + userName);
        
        if (creator == null || creator.getUuid() == null || creator.getUuid().isEmpty()) {
            return Response.status(FORBIDDEN).build();
        }
        
        logger.log(Level.INFO, "Trying to get bookings by creator : " + userName);
        List<Booking> bookings = DAO.getByCreator(creator.getUuid());
        logger.log(Level.INFO, "Got bookings :" + bookings);
        List<Booking> inValid = new ArrayList<>();
        for (Booking booking : bookings) {
            if (!checkValidBooking(booking)) {
                inValid.add(booking);
            }
        }
        bookings.removeAll(inValid);
        
        if (bookings.isEmpty()) {
            return Response.noContent().build();
        }
        
        GenericEntity<List<Booking>> ge = new GenericEntity<List<Booking>>(bookings){};
        logger.log(Level.INFO, "Exiting getByCreator");
        return Response.ok(ge).build();
    }
    
    @GET
    @Path("byVenueId/{uuid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getBookingByVenue(@PathParam ("uuid") String uuid) {
        
        if (uuid == null || uuid.isEmpty()) {
            return Response.status(FORBIDDEN).build();
        }
        
        List<Booking> bookings = DAO.getByVenue(uuid);
        List<Booking> inValid = new ArrayList<>();
        for (Booking booking : bookings) {
            if (!checkValidBooking(booking)) {
                inValid.add(booking);
            }
        }
        bookings.removeAll(inValid);
        
        if (bookings.isEmpty()) {
            return Response.noContent().build();
        }
        
        GenericEntity<List<Booking>> ge = new GenericEntity<List<Booking>>(bookings){};
        
        return Response.ok(ge).build();
    }
    
    @POST
    @Path("addAdmin/{uuid}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response addAdmin(@PathParam("uuid") String uuid, JAXBElement<User> admin) {
        Booking b = (Booking)getBookingById(uuid).getEntity();
        User current = userDAO.getByUserName(getUserName());
        if (!b.isAdmin(current.getUuid())) {
            return Response.status(FORBIDDEN).build();
        }
        b.addAdmin(admin.getValue());
        DAO.update(b);
        return Response.ok().build();
    }
    
    @POST
    @Path("addGuest/{uuid}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response addGuest(@PathParam("uuid") String uuid, JAXBElement<User> guest) {
        Booking b = (Booking)getBookingById(uuid).getEntity();
        b.addAdmin(guest.getValue());
        DAO.update(b);
        return Response.ok().build();
    }
    
    @PUT
    @Path("demoteAdmin/{bookingUuid}/{adminUuid}")
    @Override
    public Response demoteAdmin(@PathParam("bookingUuid") String bookingUuid, @PathParam("adminUuid") String adminUuid) {
        Booking booking = DAO.find(bookingUuid);
        //Invalid booking or not admin
        if (booking == null || !booking.isAdmin(adminUuid)) {
            return Response.status(FORBIDDEN).entity("Invalid booking or not admin").build();
        }
        User current = userDAO.getByUserName(getUserName());
        
        //An admin can only be demoted by himself or the creator
        if (!adminUuid.equals(current.getUuid()) && !booking.isCreator(current.getUuid())) {
            return Response.status(FORBIDDEN).entity("Admin tried to demote other admin").build();
        }
        //The creator cant demote itself
        if (booking.isCreator(adminUuid) && booking.isCreator(current.getUuid())) {
            return Response.status(FORBIDDEN).entity("Creator cant be demoted, stupid").build();
        }
        booking.demote(adminUuid);
        DAO.update(booking);
        return Response.ok().build();
    }
    
    @PUT
    @Path("promoteGuest/{bookingUuid}/{guestUuid}")
    @Override
    public Response promoteGuest(@PathParam("bookingUuid") String bookingUuid, @PathParam("guestUuid") String guestUuid) {
        Booking booking = DAO.find(bookingUuid);
        if (booking == null || !booking.isGuest(guestUuid)) {
            return Response.status(FORBIDDEN).entity("Invalid booking or given guest not guest in booking").build();
        }
        User current = userDAO.getByUserName(getUserName());
        if (!booking.isAdmin(current.getUuid()) && !booking.isCreator(current.getUuid())) {
            return Response.status(FORBIDDEN).entity("Cant promote guest to admin: Logged in user is not admin of this booking").build();
        }
        booking.promote(guestUuid);
        DAO.update(booking);
        return Response.ok().build();
    }
    
    @PUT
    @Path("removeAttender/{bookingUuid}/{attenderUuid}")
    @Override
    public Response removeAttender(@PathParam("bookingUuid") String bookingUuid, @PathParam("attenderUuid") String attenderUuid) {
        Logger.getAnonymousLogger().log(Level.INFO, "Entering removeAttender()");
        Booking booking = DAO.find(bookingUuid);
        if (booking == null || !booking.isAttending(attenderUuid)) {
            return Response.status(FORBIDDEN).entity("No such booking or not attending user").build();
        }
        User loggedIn = userDAO.getByUserName(getUserName());
        if (booking.isAdmin(attenderUuid) && !attenderUuid.equals(loggedIn.getUuid())) {
            return Response.status(FORBIDDEN).entity("Admin can only remove itself and guests").build();
        }
        if (!booking.isCreator(loggedIn.getUuid()) && !booking.isAdmin(loggedIn.getUuid())) {
            return Response.status(FORBIDDEN).entity("Logged in user is not creator or admin").build();
        }
        User user = userDAO.find(attenderUuid);
        booking.removeUser(user);
        DAO.update(booking);
        return Response.ok().build();
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response addBooking(Booking booking) {
        logger.log(Level.INFO, "Entering addBooking");
        String creatorName = getUserName();
        if (creatorName == null || creatorName.isEmpty()) {
            return Response.status(FORBIDDEN).entity("Bad user").build();
        }
        
        User creator = userDAO.getByUserName(creatorName);
        booking.setCreator(creator);
        
        if (!checkValidBooking(booking)) {
            return Response.status(FORBIDDEN).entity("Invalid booking").build();
        }
        
        DAO.add(booking);
        
        return Response.ok().build();
    }
    
    @POST
    @Path("attendBooking/{uuid}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response attendBooking(@PathParam("uuid") String bookingID) {
        logger.log(Level.INFO, "Entering attendBooking");
        String loggedInName = getUserName();
        if (loggedInName == null || loggedInName.isEmpty()) {
            return Response.status(FORBIDDEN).entity("Bad user").build();
        }
        if (checkInvalidString(bookingID)) {
            return Response.status(FORBIDDEN).entity("Bad booking").build();
        }
        
        User loggedIn = userDAO.getByUserName(loggedInName);
        Booking booking = DAO.find(bookingID);
        
        if (booking == null) {
            return Response.status(FORBIDDEN).entity("No such booking").build();
        }
        
        User creator = booking.getCreator();
        
        if (loggedIn.getUuid().equals(creator.getUuid())) {
            booking.addAdmin(loggedIn);
        } else {
            booking.addGuest(loggedIn);
        }
        
        if (!checkValidBooking(booking)) {
            return Response.status(FORBIDDEN).entity("Invalid booking").build();
        }
        
        DAO.update(booking);
        
        return Response.ok().build();
    }
    
    @POST
    @Path("unAttendBooking/{uuid}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response unAttendBooking(@PathParam("uuid") String bookingID) {
        logger.log(Level.INFO, "Entering unAttendBooking");
        String creatorName = getUserName();
        if (creatorName == null || creatorName.isEmpty()) {
            return Response.status(FORBIDDEN).entity("Bad user").build();
        }
        if (checkInvalidString(bookingID)) {
            return Response.status(FORBIDDEN).entity("Bad booking").build();
        }
        
        User guest = userDAO.getByUserName(creatorName);
        Booking booking = DAO.find(bookingID);
        if (booking == null) {
            return Response.status(FORBIDDEN).entity("No such booking").build();
        }
        
        booking.removeUser(guest);
        
        if (!checkValidBooking(booking)) {
            return Response.status(FORBIDDEN).entity("Invalid booking").build();
        }
        
        DAO.update(booking);
        
        return Response.ok().build();
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
    @Override
    public Response updateBooking(Booking booking) {
        logger.log(Level.INFO, "Entering updateBooking");
        String userName = getUserName();
        if (!checkValidBooking(booking) || checkInvalidString(userName)) {
            return Response.status(NO_CONTENT).build();
        }
        
        Booking b = DAO.find(booking.getUuid());
        if (b == null) {
            return Response.status(NO_CONTENT).build();
        }
        logger.log(Level.INFO, "booking found");
        boolean cookieIsAdmin = false;
        User current = userDAO.getByUserName(userName);
        if(b.getCreator().getUuid().equals(current.getUuid())) {
            cookieIsAdmin = true;
        }
        if(!cookieIsAdmin) {
            for (User u : b.getAdmins()) {
                if (u.getUserName().equals(getUserName())) {
                    cookieIsAdmin = true;
                }
            }
        }
        
        
        if (!cookieIsAdmin) {
            return Response.status(FORBIDDEN).build();
        }
        Logger.getAnonymousLogger().log(Level.INFO, b.getVenue().getVenueName());
        booking.setVenue(b.getVenue());
        booking.setCreator(b.getCreator());
        logger.log(Level.INFO, "user is admin");
        DAO.update(booking);
        logger.log(Level.INFO, "Exiting updateBooking");
        return Response.ok().build();
    }
    
    @DELETE
    @Path("{uuid}")
    @Override
    public Response deleteBooking(@PathParam("uuid") String uuid) {
        
        Logger.getAnonymousLogger().log(Level.INFO, "Entering delete booking");
        
        Booking b = (Booking) getBookingById(uuid).getEntity();
        boolean cookieIsCreator = b.getCreator().getUserName().equals(getUserName());
        
        if (uuid == null || uuid.isEmpty() || !cookieIsCreator) {
            return Response.status(FORBIDDEN).entity("No permission: Logged in isnt creator").build();
        }
        
        Object obj = getBookingById(uuid).getEntity();
        if (obj == null) {
            return Response.noContent().build();
        }
        Booking booking = (Booking) obj;
        
        if (!checkValidBooking(booking)) {
            return Response.status(FORBIDDEN).entity("Not a valid booking").build();
        }
        
        DAO.remove(uuid);
        
        Logger.getAnonymousLogger().log(Level.INFO, "Exiting delete booking");
        return Response.ok().build();
    }
    
    private boolean checkValidBooking(Booking booking) {
        return (booking != null && booking.getUuid() != null
                && !booking.getUuid().trim().isEmpty() && booking.getCreator() != null
                && booking.getVenue() != null && booking.getEventName() != null
                && !booking.getEventName().trim().isEmpty());
    }
    
    @GET
    @Path("byName/{eventName}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Override
    public Response getBookingByEventName(@PathParam("eventName") String eventName) {
        if(checkInvalidString(eventName)) {
            return Response.status(FORBIDDEN).build();
        }
        List<Booking> result = DAO.getByEventName(eventName);
        
        GenericEntity<List<Booking>> ge = new GenericEntity<List<Booking>>(result) {
        };
        return Response.ok(ge).build();
    }
}