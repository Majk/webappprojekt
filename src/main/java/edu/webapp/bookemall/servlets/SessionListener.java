package edu.webapp.bookemall.servlets;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Web application lifecycle listener.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
@WebListener()
public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        Logger.getAnonymousLogger().log(Level.INFO,"Session with id: (" + se.getSession().getId() + ") created" );
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        Logger.getAnonymousLogger().log(Level.INFO,"Session with id: (" + se.getSession().getId() + ") destroyed" );
    }
}

