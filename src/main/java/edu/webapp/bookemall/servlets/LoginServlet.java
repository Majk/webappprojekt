package edu.webapp.bookemall.servlets;

import edu.webapp.bookemall.util.Keys;
import edu.webapp.bookemall.util.Navigation;
import edu.webapp.bookemall.util.CookieHelper;
import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.persistence.IUserDAO;
import edu.webapp.bookemall.util.Mailer;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Login Servlet. Should remain stateless.
 *
 * Login has the following: captcha, full form validation,
 * username check, password strength bar, confirmation mail on new users,
 * remember Me cookie and mail password reset link.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    
    @EJB
    private IUserDAO DAO;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String action = request.getParameter("action");
        String confirm = request.getParameter("confirm");
        String ignore = request.getParameter("ignore");
        String view = request.getParameter("view");
        String username;
        String password;
        String email;
        PrintWriter out = response.getWriter();
        
        if (action != null){
            switch( action ){
                case "login" :
                    Logger.getAnonymousLogger().log(Level.INFO, "Attempting to login with name: "
                            + request.getParameter("username") 
                            + " password: " + request.getParameter("password"));
                    
                    username = request.getParameter("username");
                    password = DigestUtils.sha512Hex(request.getParameter("password"));
                    
                    boolean remember = "true".equals(request.getParameter("remember"));
                    
                    if (validInput(username) && validInput(password)){
                        try{
                            User user = DAO.findLoginCredentials(username, password);
                            
                            if (user.getConfirmation()){
                                request.login(user.getUuid(), password);
                                request.getSession().setAttribute("USER", username);
                                setRememberMe(user, remember, request, response);
                                
                                if (user.isAdmin()){
                                    response.sendRedirect(Navigation.ADMIN.toString());
                                } else {
                                    response.sendRedirect(Navigation.USER.toString());
                                }
                                
                            } else {
                                response.sendRedirect(Navigation.NOTCONFIRMED.toString());
                            }
                            
                        } catch (NullPointerException e) { // No such user found
                            response.sendRedirect(Navigation.INVALIDLOGIN.toString());
                            
                        } catch (Exception e){ // Something more severe happened
                            Logger.getAnonymousLogger().log(Level.SEVERE, "Login Exception: ", e);
                            response.sendRedirect(Navigation.INVALIDLOGIN.toString());
                        }
                    }else { // Invalid input
                        response.sendRedirect(Navigation.INVALIDLOGIN.toString());
                    }
                    break;
                    
                    /*
                     * Adding a new User to the database.
                     */
                case "newUser" :
                    Logger.getAnonymousLogger().log(Level.INFO, "Adding user with username: " 
                            + request.getParameter("username") + " email: " 
                            + request.getParameter("email") + " password: " 
                            + request.getParameter("password"));
                    
                    username = request.getParameter("username");
                    email = request.getParameter("email");
                    password = request.getParameter("password");
                    
                    if (validInput(username) && validInput(email) && validInput(password)) {
                        if(DAO.getByUserName(username) == null){
                            String mailToken = UUID.randomUUID().toString();
                            User newUser = new User(username,password,email);
                            // Hash token
                            newUser.setMailToken(DigestUtils.sha512Hex(mailToken));
                            DAO.add(newUser);
                            
                            Mailer mailer = new Mailer();
                            String servermail = "bookemall.server@gmail.com";
                            String htmlText = "<H1>Bookemall Confirmation!</H1>";
                            String content = "Hello there, "
                                    + "\n\n"
                                    + "Please click on the link below to "
                                    + "activate / confirm your account."
                                    + "\n\n"
                                    + "Confirmation link: https://localhost:8181/webappprojekt/login?confirm=" + mailToken
                                    + "\n\n"
                                    + "Thank you and welcome to bookemall!";
                            String subject = "Bookemall Confirmation!";
                            
                            try{
                                mailer.sendMessage(servermail, email, subject, content, htmlText);
                                out.print("added");
                            }catch (MessagingException | NamingException e) {
                                out.print("invalid");
                            }
                        }
                    }else {
                        out.print("invalid");
                    }
                    
                    break;
                case "checkUserName" :
                    Logger.getAnonymousLogger().log(Level.INFO, "Checking username: " 
                            + request.getParameter("username"));
                    
                    username = request.getParameter("username");
                    
                    if (!validInput(username)){
                        out.print("invalid");
                    } else {
                        if (DAO.getByUserName(username) == null){
                            out.print("true");
                        } else {
                            out.print("false");
                        }
                    }
                    break;
                case "verify" :
                    Logger.getAnonymousLogger().log(Level.INFO, "Verify captcha for " 
                            + request.getRemoteAddr() + " parameters: " 
                            + "challange: " + request.getParameter("challenge") 
                            + " response" + request.getParameter("response"));
                    
                    String remoteip = request.getRemoteAddr();
                    String challenge = request.getParameter("challenge");
                    String res = request.getParameter("response");
                    ReCaptchaResponse reCaptchaResponse = ((ReCaptchaImpl) request.getServletContext()
                                                            .getAttribute(Keys.RECAPTCHA.toString()))
                                                            .checkAnswer(remoteip, challenge, res);
                    if (reCaptchaResponse.isValid()) {
                        out.print("true"); // Write true to validation in script
                    } else {
                        out.print("false");
                    }
                    break;
                case "reset" :
                    Logger.getAnonymousLogger().log(Level.INFO, "Sending password reset code to email: " 
                            + request.getParameter("email")
                            + " request ip: " + request.getRemoteAddr());
                    
                    email = request.getParameter("email");
                    username = request.getParameter("username");
                    String IP = request.getRemoteAddr();
                    
                    if (validInput(username) && validInput(email)) {
                        User user = DAO.getByUserName(username);
                        if(user == null){
                            out.print("invalid user");
                        } else {
                            if (!user.getEmail().equals(email)){
                                out.print("invalid user and email combination");
                            } else {
                                String mailToken = UUID.randomUUID().toString();
                                // Hash token to make sure if database is somehow stolen
                                // it shouldn't be possible to use the reset link out of the box.
                                user.setMailToken(DigestUtils.sha512Hex(mailToken));
                                DAO.update(user);
                                Mailer mailer = new Mailer();
                                String mail = "bookemall.server@gmail.com";
                                String htmlText = "<H1>Password change request!</H1>";
                                String content = "Hello there, "
                                        + "\n\n"
                                        + "Here is the code to reset your "
                                        + "password / that was requested from IP : " + IP
                                        + "\n\n"
                                        + "Copy this code and enter in the change password form: " + mailToken
                                        + "\n\n"
                                        + "If you did not request a password change press the link below:"
                                        + "\n\n"
                                        + "https://localhost:8181/login?ignore="+ mailToken;
                                String subject = "Password change request!";
                                try{
                                    mailer.sendMessage(mail, email, subject, content, htmlText);
                                    out.print("new code sent");
                                }catch (MessagingException | NamingException e) {
                                    out.print("error while sending code");
                                }
                            }
                        }
                    }
                    break;
                case "newPass" :
                    Logger.getAnonymousLogger().log(Level.INFO, "Setting new password: " 
                            + request.getParameter("password")
                            + " to user with code: " + request.getParameter("code"));
                    
                    password = DigestUtils.sha512Hex(request.getParameter("password"));
                    String code = DigestUtils.sha512Hex(request.getParameter("code"));
                    
                    User tmp = DAO.getUserFromMailToken(code);
                    if (tmp != null){
                        tmp.setPassword(password);
                        tmp.setMailToken(null);
                        DAO.update(tmp);
                        out.print("success");
                    } else {
                        out.print("The information entered was not valid and was rejected."
                                + "The code is now invalid. Please try again or contact a"
                                + "site adminstrator if the problem persists.");
                    }
                    break;
                case "logout" :
                    String name = (String) request.getServletContext().getAttribute(Keys.COOKIE_NAME.toString());
                    CookieHelper.removeCookie(response, name);
                    DAO.deleteRememberMe((String)request.getSession().getAttribute("USER"));
                    request.getSession().invalidate();
                    response.sendRedirect(Navigation.LOGOUT.toString());
                    break;
                default:
                    break;
            }
        }
        
        /**
         * Confirms the user from the link that was sent to her.
         */
        if (confirm != null){
            confirm = DigestUtils.sha512Hex(confirm);
            User tmp = DAO.getUserFromMailToken(confirm);
            if (tmp != null){
                tmp.setMailToken(null);
                tmp.setConfirmation();
                DAO.update(tmp);
                response.sendRedirect(Navigation.CONFIRMED.toString());
            } else {
                response.sendRedirect(Navigation.LOGIN.toString());
            }
        }
        
        /**
         * User presses the link in the reset password mail and
         * erases the token in the database.
         */
        if (ignore != null){
            ignore = DigestUtils.sha512Hex(ignore);
            User tmp = DAO.getUserFromMailToken(ignore);
            if (tmp != null){
                tmp.setMailToken(null);
                DAO.update(tmp);
                response.sendRedirect(Navigation.LOGIN.toString());
            } else {
                response.sendRedirect(Navigation.LOGIN.toString());
            }
        }
        
        if (view != null){
            switch ( view ){
                case "login" :
                    request.getRequestDispatcher(Navigation.LOGIN.toString()).forward(request, response);
                    break;
                default :
                    break;
            }
        }
        
    }
    
    /**
     * Checks user input for empty strings and null
     * 
     * @param s string to test
     * @return true if input is valid, false otherwise.
     */
    private boolean validInput(String s){
        if (s != null && !s.trim().isEmpty()){
            return true;
        }
        return false;
    }
    
    /**
     * Sets the Remember Me cookie if the user enabled this option.
     * 
     * @param user to remember
     * @param remember the user if true, remove otherwise
     * @param request from the client
     * @param response to the client
     */
    private void setRememberMe(User user, boolean remember, HttpServletRequest request, HttpServletResponse response){
        if(remember) {
            String uuid = UUID.randomUUID().toString();
            String name = (String) request.getServletContext()
                    .getAttribute(Keys.COOKIE_NAME.toString());
            
            int age = (int) request.getServletContext()
                    .getAttribute(Keys.COOKIE_AGE.toString());
            
            user.setRememberCookie(uuid);
            DAO.update(user);
            CookieHelper.addCookie(response, name , uuid, age);
        } else {
            user.setRememberCookie(null);
            DAO.update(user);
            String name = (String) request.getServletContext()
                    .getAttribute(Keys.COOKIE_NAME.toString());
            
            CookieHelper.removeCookie(response, name);
        }
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet handeling authentication of users";
    }// </editor-fold>
}
