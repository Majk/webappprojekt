package edu.webapp.bookemall.servlets;

import edu.webapp.bookemall.util.Keys;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import net.tanesha.recaptcha.ReCaptchaImpl;

/**
 * Web application lifecycle listener.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */

@WebListener()
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Logger.getAnonymousLogger().log(Level.INFO,"Putting data in application scope");
        ReCaptchaImpl recap = new ReCaptchaImpl();    
        recap.setPrivateKey("6Le_necSAAAAAIMqPV5vuZfZPt53QDPIp4RfOB9O");
        sce.getServletContext().setAttribute(Keys.RECAPTCHA.toString(), recap);
        sce.getServletContext().setAttribute(Keys.COOKIE_AGE.toString(), 2592000);
        sce.getServletContext().setAttribute(Keys.COOKIE_NAME.toString(), "REMEMBER");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Logger.getAnonymousLogger().log(Level.INFO,"Removing data from application scope");
        sce.getServletContext().removeAttribute(Keys.RECAPTCHA.toString());
        sce.getServletContext().removeAttribute(Keys.COOKIE_AGE.toString());
        sce.getServletContext().removeAttribute(Keys.COOKIE_NAME.toString());
    }
}
