package edu.webapp.bookemall.booking;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for handling dates, contains year, month, day, hour and minute.
 * 
 * @author Johan W-Schützer <Johan.DSS@Gmail.com>
 */
@Entity
@XmlRootElement
public class DateStamp extends AbstractEntity implements Serializable{

    @CollectionTable(
        uniqueConstraints= @UniqueConstraint(columnNames={"nYEAR","nMONTH","nDAY","nHOUR","nMINUTE"})
    )
    
    @OneToMany(mappedBy="dateStamp")
    private List<Booking> bookings;
    @Column(name = "nYEAR")
    private int year;
    @Column(name = "nMONTH")
    private int month;
    @Column(name = "nDAY")
    private int day;
    @Column(name = "nHOUR")
    private int hour;
    @Column(name = "nMINUTE")
    private int minute;
    
    public static final int YEAR = 0;
    public static final int MONTH = 1;
    public static final int DAY = 2;
    public static final int HOUR = 3;
    public static final int MINUTE = 4;

    /**
     * Default empty constructor, used internally
     */
    public DateStamp() {
        super();
    }

    /**
     * Creates a date
     *
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param minute
     * @throws IllegalArgumentException if the combination of parameters doesn't
     * result in a valid date
     */
    public DateStamp(int year, int month, int day, int hour, int minute) throws IllegalArgumentException {
        super();
        try {
            setYear(year);
            setMonth(month);
            setDay(day);
            setHour(hour);
            setMinute(minute);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid date construction", e);
        }
    }

    /**
     * Clones a date
     *
     * @param date
     * @throws IllegalArgumentException if the given date was invalid
     */
    public DateStamp(DateStamp date) throws IllegalArgumentException {
        try {
            setUuid(date.getUuid());
            setYear(date.year);
            setMonth(date.month);
            setDay(date.day);
            setHour(date.hour);
            setMinute(date.minute);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid date cloning", e);
        }
    }
    
    /**
     * Creates a date from a formatted string
     *
     * @param date
     * @throws IllegalArgumentException if the given date was invalid
     */
    public DateStamp(String formattedDate) throws IllegalArgumentException {
        String[] values = formattedDate.split("/");
        try {
            setYear(Integer.parseInt(values[YEAR]));
            setMonth(Integer.parseInt(values[MONTH]));
            setDay(Integer.parseInt(values[DAY]));
            setHour(Integer.parseInt(values[HOUR]));
            setMinute(Integer.parseInt(values[MINUTE]));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Badly formatted string (value was not an int): " + formattedDate, e);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid date cloning", e);
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Badly formatted string: " + formattedDate, e);
        }
    }

    /**
     * Returns the year of the date
     *
     * @return the year of the date
     */
    public int getYear() {
        return year;
    }

    /**
     * Sets the year of the date. If the year is set to a non leap year and the
     * month is 2 and the day is greater than 28, the day is set to 28
     *
     * @param year the given year
     */
    public void setYear(int year) {
        this.year = year;
        if (!isLeapYear(year) && month == 2 && day > 28) {
            day = 28;
        }
    }

    /**
     * returns the month of the date
     *
     * @return the month of the date
     */
    public int getMonth() {
        return month;
    }

    /**
     * Sets the month of the date Resets the day to the highest valid day of the
     * month if the day becomes invalid
     *
     * @param month the given month
     * @throws IllegalArgumentException if the given month is above 12 or below
     * 1
     */
    public void setMonth(int month) throws IllegalArgumentException {
        if (month > 12 || month < 1) {
            throw new IllegalArgumentException("Month cant be under 1 or over 12");
        }
        this.month = month;
        switch (this.month) {
            case 2:
                if (day > 29 && isLeapYear(year)) {
                    day = 29;
                } else if (day > 28 && !isLeapYear(year)) {
                    day = 28;
                }
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                if (day > 31) {
                    day = 31;
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                if (day > 30) {
                    day = 30;
                }
                break;
        }
    }

    /**
     * Returns the day of the date
     *
     * @return the day of the date
     */
    public int getDay() {
        return day;
    }

    /**
     * Sets the day of the date
     *
     * @param day the given
     * @throws IllegalArgumentException if a faulty day was given, according to
     * the month and year of the date
     */
    public void setDay(int day) throws IllegalArgumentException {
        switch (this.month) {
            case 2:
                if (day > 29 || day < 1 || (day > 28 && !isLeapYear(this.year))) {
                    throw new IllegalArgumentException("Day cant be under 1 or over 29, or over 28 if year is not a leap year, for month 2");
                }
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                if (day > 31 || day < 1) {
                    throw new IllegalArgumentException("Day cant be under 1 or over 31 for months 1, 3, 5, 7, 8, 10 and 12");
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                if (day > 30 || day < 1) {
                    throw new IllegalArgumentException("Day cant be under 1 or over 30 for months 4, 6, 9, 11");
                }
                break;
        }
        this.day = day;
    }

    /**
     * Returns the hour of the date
     *
     * @return the hour of the date
     */
    public int getHour() {
        return hour;
    }

    /**
     * Sets the hour of the date
     *
     * @param hour the given hour
     * @throws IllegalArgumentException if the given hour was over 23 or under 0
     */
    public void setHour(int hour) throws IllegalArgumentException {
        if (hour > 23 || hour < 0) {
            throw new IllegalArgumentException("Hour cant be uner 0 or over 23");
        }
        this.hour = hour;
    }

    /**
     * Returns the minute of the date
     *
     * @return the minute of the date
     */
    public int getMinute() {
        return minute;
    }

    /**
     * Sets the minute of the date
     *
     * @param minute the given minute
     * @throws IllegalArgumentException if the given minute is over 59 or under
     * 0
     */
    public void setMinute(int minute) throws IllegalArgumentException {
        if (minute > 59 || minute < 0) {
            throw new IllegalArgumentException("Minutes cant be under 0 or over 59");
        }
        this.minute = minute;
    }

    /**
     * Checks if the given year is a leap year
     */
    private boolean isLeapYear(int year) {
        if ((year % 4 == 0) && year % 100 != 0) {
            return true;
        } else if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) {
            return true;
        } else {
            return false;
        }
    }

    public List<Booking> getBookings() {
        return bookings;
    }
}