package edu.webapp.bookemall.booking;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Address embedded in Venue.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 * @edited Mikael Stolpe
 */
@XmlRootElement
@Embeddable
public class Address implements Serializable {
    
    private String street;
    private String city;
    private String country;
    private int nbr;
    
    public Address() {
    }
    
    public Address(String street, int nbr, String city, String country) {
        this.street = street;
        this.nbr = nbr;
        this.city = city;
        this.country = country;
    }
    
    public String getStreet() {
        return this.street;
    }
    
    public void setStreet(String street) {
        this.street = street;
    }
    
    public int getNbr() {
        return nbr;
    }
    
    public int setNbr(int nbr) {
        return this.nbr = nbr;
    }
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        if(!(obj instanceof Address)) {
            return false;
        }
        Address other = (Address) obj;
        return this.city.equals(other.city) && this.country.equals(other.country) 
                && this.street.equals(other.street) && this.nbr == other.nbr;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.street);
        hash = 59 * hash + Objects.hashCode(this.city);
        hash = 59 * hash + Objects.hashCode(this.country);
        hash = 59 * hash + this.nbr;
        return hash;
    }
}
