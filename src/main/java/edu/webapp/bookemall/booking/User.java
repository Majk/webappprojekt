package edu.webapp.bookemall.booking;

import java.io.Serializable;

import java.util.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.codec.digest.DigestUtils;

import org.joda.time.*;

/**
 * Holds a user and its information.
 *
 * @author Mikael Stolpe
 * @DatabaseMappings and updates : Gustaf Ringius <gustaf@linux.com>
 */
@XmlRootElement(name= "user")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="USERS")
public class User extends AbstractEntity implements Serializable {
    
    public static enum Group {
        ADMIN, USER;
    }
    
    @Column(unique=true, nullable = false, length = 20)
    private String userName;
    private String password;
    private String email;
    private String rememberCookie;
    @Basic(fetch=FetchType.LAZY)
    private DateTime registeredOn;
    private String picture;
    private String firstName;
    private String lastName;
    private boolean confirmed;
    private String mailToken;
    @OneToMany(mappedBy = "creator", targetEntity=Booking.class,
                cascade = CascadeType.MERGE, orphanRemoval=true)
    @XmlTransient
    private List<Booking> bookings;
    @OneToMany(mappedBy = "creator", targetEntity=Venue.class,
                cascade = CascadeType.MERGE, orphanRemoval=true)
    @XmlTransient
    private List<Venue> venues;
    @ElementCollection(targetClass = Group.class)
    @CollectionTable(name = "USERS_GROUPS",
            joinColumns       = @JoinColumn(name = "UUID", nullable=false),
            uniqueConstraints = { @UniqueConstraint(columnNames={"UUID","GROUPNAME"}) } )
    @Enumerated(EnumType.STRING)
    @Column(name="GROUPNAME", length=64, nullable=false)
    private List<Group> groups;
    
    public User() {
        super();
        confirmed = false;
        registeredOn = new DateTime();
        groups = new ArrayList<>();
        groups.add(Group.USER);
    }
    
    @PrePersist
    @PreUpdate
    private void validate() {
        if (registeredOn == null){
            throw new IllegalArgumentException("Registration Date failed");
        }
        if (!groups.contains(Group.USER)) {
            throw new IllegalArgumentException("Group Registration failed");
        }
        if (userName == null || "".equals(userName)){
            throw new IllegalArgumentException("Invalid username");
        }
        if (password == null || "".equals(password)){
            throw new IllegalArgumentException("Password set failed");
        }
        if (email == null || "".equals(email)){
            throw new IllegalArgumentException("Invlid email");
        }
    }
    
    public User(String userName, String password, String email) throws IllegalArgumentException {
        this();
        try {
            setUserName(userName);
            setEmail(email);
            setAndEncryptPassword(password);
            setPicture("/webappprojekt/resources/img/NoFace.jpg");
        } catch (IllegalArgumentException e ) {
            throw new IllegalArgumentException("Occured in the constructor", e);
        }
    }
    
    public User(String userName, String password, String email, String picture) {
        this(userName, password, email);
        this.picture = picture;
    }
    
    public User(String userName, String password, String email, String picture, String firstName) {
        this(userName, password, email, picture);
        this.firstName = firstName;
    }
    
    public User(String userName, String password, String email, String picture, String firstName, String lastName) {
        this(userName, password, email, picture, firstName);
        this.lastName = lastName;
    }
    
    public String getMailToken() {
        return mailToken;
    }
    
    public void setMailToken(String mailToken) {
        this.mailToken = mailToken;
    }
    
    public void setConfirmation () {
        confirmed = true;
    }
    
    public boolean getConfirmation () {
        return confirmed;
    }
    
    public List<Group> getGroups() {
        List<Group> ret = new ArrayList<>();
        for (Group g : groups){
            ret.add(g);
        }
        return ret;
    }
    
    public DateTime getRegisteredOn() {
        return new DateTime(registeredOn);
    }
    
    public String getRegistrationDate() {
        return registeredOn.toString("yyyy-MM-dd'T'HH:mm");
    }
    
    public void setRegisteredOn(DateTime registeredOn) {
        this.registeredOn = registeredOn;
    }
    
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
    
    public void setRememberCookie(String cookieNr) {
        this.rememberCookie = cookieNr;
    }
    
    public String getRememberCookie() {
        return rememberCookie;
    }
    
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        if (userName == null || userName.trim().isEmpty()) {
            throw new IllegalArgumentException("UserName cannot be set to null or empty");
        }
        this.userName = userName;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        if (email == null || email.trim().isEmpty()) {
            throw new IllegalArgumentException("Email cannot be set to null or empty");
        }
        this.email = email;
    }
    
    public String getPicture() {
        return picture;
    }
    
    public void setPicture(String picture) {
        this.picture = picture;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public boolean isAdmin() {
        return groups.contains(Group.ADMIN);
    }
    
    public void setAdmin() {
        if (!groups.contains(Group.ADMIN)){
            groups.add(Group.ADMIN);
        }
    }
    
    public String getPassword() {
        return this.password;
    }
    
    /** Sets a new password for the user
     *
     * @param pass new password for the user
     * @return
     */
    public void setPassword(String password) {
        
        if (password == null || password.trim().isEmpty()) {
            throw new IllegalArgumentException("Password cannot be set to null or empty");
        }
        this.password = password;
    }
    
    /**
     * Sets and encrypts the password
     * @param password
     */
    public void setAndEncryptPassword(String password) {
        if (password == null || password.trim().isEmpty()) {
            throw new IllegalArgumentException("Password cannot be set to null or empty");
        }
        this.password = DigestUtils.sha512Hex(password);
    }
    
    public List<Venue> getVenues() {
        return venues;
    }
    
    public void setVenues(List<Venue> venues){
        this.venues = venues;
    }
    
    public List<Booking> getBookings() {
        return bookings;
    }
    
    public void setBookings(List<Booking> bookings){
        this.bookings = bookings;
    }
}
