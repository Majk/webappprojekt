package edu.webapp.bookemall.booking;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * 
 * @author Gustaf Ringius <gustaf@linux.com>
 */
@XmlRootElement
@XmlSeeAlso({Booking.class, User.class, Venue.class})
@MappedSuperclass
public abstract class AbstractEntity implements Serializable{
    
    @Id
    @Basic(optional = false)
    @Column(nullable = false)
    private String Uuid;
    
    protected AbstractEntity(){
        this.Uuid = UUID.randomUUID().toString();
    }
    protected AbstractEntity(String id){
        this.Uuid = id;
    }

    public String getUuid(){
        return Uuid;
    }

    public void setUuid(String Uuid){
    	if (Uuid == null || Uuid.trim().isEmpty()) {
    		throw new IllegalArgumentException("Uuid cannot be set to null or empty");
    	}
        this.Uuid = Uuid;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.Uuid);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractEntity other = (AbstractEntity) obj;
        if (!Objects.equals(this.Uuid, other.Uuid)) {
            return false;
        }
        return true;
    }
}
