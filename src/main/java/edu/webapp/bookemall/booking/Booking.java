package edu.webapp.bookemall.booking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class that handles Bookings.
 *
 * @author Johan W-Schützer <Johan.DSS@Gmail.com>
 * 
 * @DatabaseMappings and updates : Gustaf Ringius <gustaf@linux.com>, Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Booking extends AbstractEntity implements Serializable {
    
    @XmlElementWrapper(name = "admins")
    @XmlElement(name = "user")
    @JoinTable(name="EVENT_ADMIN")
    @OneToMany(fetch=FetchType.EAGER)
    private List<User> admins;
    @XmlElementWrapper(name = "guests")
    @XmlElement(name = "user")
    @JoinTable(name="EVENT_GUEST")
    @OneToMany(fetch=FetchType.EAGER)
    private List<User> guests;
    @ManyToOne(optional=false, cascade={CascadeType.MERGE})
    @JoinColumn(name="CREATOR_ID", referencedColumnName="UUID")
    private User creator;
    @ManyToOne(optional=false, cascade={CascadeType.MERGE})
    @JoinColumn(name="VENUE_ID", referencedColumnName="UUID")
    private Venue venue;
    private String eventName;
    @Column(length = 2000)
    private String description;
    @JoinTable(name="EVENT_DATE")
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private DateStamp dateStamp;
    
    /**
     * Default empty constructor, used internally
     */
    public Booking() {
        super();
    }
    
    /**
     * Constructor to be used externally
     * @param dateStamp The given datestamp
     * @param creator The given creator
     */
    public Booking(DateStamp dateStamp, User creator) {
        this();
        this.dateStamp = dateStamp;
        admins = new ArrayList<>();
        guests = new ArrayList<>();
        this.creator = creator;
        admins.add(creator);
    }
    
    /**
     * Used internally for validation checks
     */
    @PrePersist
    @PreUpdate
    private void validate(){
        if (creator == null){
            throw new IllegalArgumentException("Invalid Creator");
        }
        if (dateStamp == null){
            throw new IllegalArgumentException("Invalid DateStamp");
        }
    }
    
    /**
     * Checks for null
     *
     * @param o : object to check
     * @return true if null, false otherwise.
     */
    public boolean checkNull(Object o){
        return o == null;
    }
    
    /**
     * Adds a guest to the guest list
     * @param guest the given guest
     */
    public void addGuest(User guest) {
        if(!checkNull(guest)){
            if (!isAttending(guest.getUuid()))
                guests.add(guest);
        }
    }
    
    /**
     * Adds an admin to the admin list
     * @param admin the given admin
     */
    public void addAdmin(User admin) {
        if(!checkNull(admin)){
            if (!isAttending(admin.getUuid()))
                admins.add(admin);
        }
    }
    
    /**
     * Demotes an existing admin to guest
     * @param adminUuid the uuid of the admin to be demoted
     */
    public void demote(String adminUuid) {
        if(!checkNull(adminUuid)){
            User toRemove = null;
            for (User a : admins){
                if (a.getUuid().equals(adminUuid)){
                    guests.add(a);
                    toRemove = a;
                }
            }
            admins.remove(toRemove);
        }
    }
    
    /**
     * Promotes an existing guest to admin
     * @param guestUuid the uuid of the guest to be promoted
     */
    public void promote(String guestUuid) {
        if(!checkNull(guestUuid)){
            User toRemove = null;
            for (User g : guests){
                if (g.getUuid().equals(guestUuid)){
                    admins.add(g);
                    toRemove = g;
                }
            }
            guests.remove(toRemove);
        }
    }
    
    /**
     * Removes a guest from the booking
     * @param guest the guest to be removed
     */
    public void removeGuest(User guest) {
        if(!checkNull(guest)){
            guests.remove(guest);
        }
    }
    
    /**
     * Removes a user (guest or admin) from the booking
     * @param user the user to be removed
     */
    public void removeUser(User user) {
        if(!checkNull(user)){
            guests.remove(user);
            admins.remove(user);
        }
    }
    
    /**
     * Sets the venue of the booking
     * @param venue the given booking
     */
    public void setVenue(Venue venue) {
        if(!checkNull(venue)){
            this.venue = venue;
        }
    }
    
    /**
     * Returns the venue of the booking
     * @return venue
     */
    public Venue getVenue() {
        return venue;
    }
    
    /**
     * Returns all attenders (guest and admins) of the booking
     * @return list of user
     */
    public List<User> getAttenders() {
        List<User> toReturn = getAdmins();
        toReturn.addAll(getGuests());
        return toReturn;
    }
    
    /**
     * Returns all admins of the booking
     * @return list of user
     */
    public List<User> getAdmins() {
        List<User> ret = new ArrayList<>();
        for (User a : admins){
            ret.add(a);
        }
        return ret;
    }
    
    /**
     * Returns all guests of the booking
     * @return list of user
     */
    public List<User> getGuests() {
        List<User> ret = new ArrayList<>();
        for(User g : guests){
            ret.add(g);
        }
        return ret;
    }
    
    /**
     * Checks if a user with the given uuid is attending the booking
     * @param uuid the given uuid
     * @return true of the user with given uuid is attending the booking
     */
    public boolean isAttending(String uuid) {
        boolean attending = false;
        if(!checkNull(uuid)){
            for (User g : getAttenders()){
                if (g.getUuid().equals(uuid)){
                    attending = true;
                }
            }
        }
        return attending;
    }
    
    /**
     * Checks if a user with the given uuid is creator of the booking
     * @param uuid the given uuid
     * @return true of the user with given uuid is creator of the booking
     */
    public boolean isCreator(String uuid) {
        if(!checkNull(uuid)){
            return creator.getUuid().equals(uuid);
        }
        return false;
    }
    
    /**
     * Checks if a user with the given uuid is admin of the booking
     * @param uuid the given uuid
     * @return true of the user with given uuid is admin of the booking
     */
    public boolean isAdmin(String uuid) {
        if(!checkNull(uuid)){
            for (User a : admins){
                if (a.getUuid().equals(uuid)){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Checks if a user with the given uuid is guest of the booking
     * @param uuid the given uuid
     * @return true of the user with given uuid is guest of the booking
     */
    public boolean isGuest(String uuid) {
        if(!checkNull(uuid)){
            for(User g : guests){
                if (g.getUuid().equals(uuid)){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Sets the event name of the booking
     * @param eventName the given event name
     */
    public void setEventName(String eventName){
        this.eventName = eventName;
    }
    
    /**
     * Returns the event name of the booking
     * @return string the eventname
     */
    public String getEventName(){
        return eventName;
    }
    
    /**
     * Sets the event description of the booking
     * @param description the given description
     */
    public void setEventDescription(String description){
        this.description = description;
    }
    
    /**
     * Gets the event description of the booking
     * @return string the event description of the booking
     */
    public String getEventDescription(){
        return description;
    }
    
    /**
     * Gets the date stamp of the booking
     * @return DateStamp the date stamp of the booking
     */
    public DateStamp getDateStamp() {
        return dateStamp;
    }

    /**
     * Sets the date stamp of the booking
     * @param dateStamp DateStamp the given date stamp
     */
    public void setDateStamp(DateStamp dateStamp) {
        this.dateStamp = dateStamp;
    }

    /**
     * Gets the creator of the booking
     * @return User the creator of the booking
     */
    public User getCreator() {
        return creator;
    }

    /**
     * Sets the creator of the booking
     * @param creator the given user
     */
    public void setCreator(User creator) {
        this.creator = creator;
    }
}