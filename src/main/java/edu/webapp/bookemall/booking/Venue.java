package edu.webapp.bookemall.booking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Represents a location where Events takes place
 *
 * @author Mikael Stolpe
 * @DatabaseMappings and updates : Gustaf Ringius <gustaf@linux.com>, Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
@XmlRootElement(name = "venue")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Venue extends AbstractEntity implements Serializable {
   
    private String venueName;
    @ManyToOne(optional=false)
    @JoinColumn(name="CREATOR_ID", referencedColumnName="uuid", nullable=false)
    private User creator;
    @XmlElementWrapper(name = "admins")
    @XmlElement(name = "user")
    @JoinTable
    @OneToMany(fetch=FetchType.EAGER)
    private List<User> admins;
    @OneToMany(mappedBy = "venue", targetEntity=Booking.class,
            fetch = FetchType.EAGER,
            cascade = CascadeType.MERGE, orphanRemoval=true)
    @XmlTransient
    private List<Booking> bookings;
    @Embedded
    private Address address;
    private int seats;
    
    public Venue() {
        super();
    }
    
    @PrePersist
    @PreUpdate
    private void validate(){
        if (creator == null){
            throw new IllegalArgumentException("Invalid Creator");
        }
    }
    
    public Venue(User creator) {
        this();
        this.creator = creator;
    }
    
    public Venue(User creator, Address address) {
        this(creator);
        this.address = address;
    }
    
    public Venue(User creator, Address address, String venueName) {
        this(creator, address);
        this.venueName = venueName;
    }
    
    public Venue(User creator, Address address, String venueName, int seats) {
        this();
        this.creator = creator;
        this.venueName = venueName;
        this.address = address;
        this.seats = seats;
        
    }
    
    
    public int getSeats(){
        return this.seats;
    }
    
    public void setSeats(int seats){
        this.seats = seats;
    }
    
    public String getVenueName() {
        return venueName;
    }
    
    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }
    
    public Address getAddress() {
        return address;
    }
    
    public void setAddress(Address address) {
        this.address = address;
    }
    
    public List<Booking> getBookings() {
        return bookings;
    }
    
    public void setBookings(List<Booking> bookings){
        this.bookings = bookings;
    }
    
    /**
     * Returns all the admins associated to this Venue
     * @return A list with all the admins
     */
    public List<User> getAdmins() {
        return new ArrayList<>(admins);
    }
    
    
    public void setAdmins(List<User> newAdmins){
        if(newAdmins == null){
            throw new IllegalArgumentException("newAdmins can't be null");
        }
        admins = newAdmins;
    }
    
    
    /**
     * Adds a user as admin of venue
     * @param newAdmin users uuid
     * @throws IllegalArgumentException if already admin or @param is null
     */
    
    public void addAdmin(User newAdmin) throws IllegalArgumentException{
        if(admins == null) {
            admins = new ArrayList<>();
        }
        if(newAdmin == null){
            throw new IllegalArgumentException("newAdmin can't be null");
        }
        if(isAdmin(newAdmin)) {
            throw new IllegalArgumentException("User with" + newAdmin.getUuid() + "as UUId is already an Admin of this venue");
        }
        admins.add(newAdmin);
    }
    
    /**
     * Returns admin status of user with uuid
     * @param user users uuid
     * @return true if admin false ow
     */
    public boolean isAdmin(User user) {
        if(admins == null) {
            admins = new ArrayList<>();
        }
        if(user == null){
            throw new IllegalArgumentException("Parameter can't be null");
        }
        return admins.contains(user);
    }
    
    
    /**
     * Adds a list of new admins to current admins
     * @param newAdmins The new admins to add
     * @return how many admins were successfully added
     * @throws IllegalArgumentException if null
     */
    public int addAdmins(List<User> newAdmins) throws IllegalArgumentException{
        if(admins == null) {
            admins = new ArrayList<>();
        }
        int count = 0;
        if(newAdmins == null || newAdmins.isEmpty()) {
            throw new IllegalArgumentException("Cant add null or empty list");
        }
        for(User newAdmin: newAdmins) {
            if(!isAdmin(newAdmin)) {
                try{
                    addAdmin(newAdmin);
                    count++;
                }catch(IllegalArgumentException e){
                }
            }
        }
        return count;
    }
    
    /**
     * Removes a user as a admin from this venue
     * @param admin Admin to remove
     * @return true if successfully removed
     * @throws IllegalArgumentException if user is null or user isn't an admin
     */
    public boolean removeAdmin(User user) throws IllegalArgumentException {
        if(user == null) {
            throw new IllegalArgumentException("Can't have null as parameter");
        }else if(!isAdmin(user)) {
            throw new IllegalArgumentException("This user is not an admin");
        }
        return admins.remove(user);
    }
    
    public User getCreator() {
        return creator;
    }
    
    public void setCreator(User creator) {
        this.creator = creator;
    }
    
    public boolean isCreator(User checkCreator) {
        return creator.equals(checkCreator);
    }
}
