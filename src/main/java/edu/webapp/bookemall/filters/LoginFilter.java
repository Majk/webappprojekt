package edu.webapp.bookemall.filters;

import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.persistence.IUserDAO;
import edu.webapp.bookemall.util.CookieHelper;
import edu.webapp.bookemall.util.Keys;
import edu.webapp.bookemall.util.Navigation;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Loginfilter handling the remember me cookie
 *  and automatically logs in the linked user.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
@WebFilter(filterName = "LoginFilter")
public class LoginFilter implements Filter {
    
    @EJB
    private IUserDAO DAO;
    
    /** Initial method just printing information to the Log to show status.
     *
     * @param config is a link to the web.xml file
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig config) throws ServletException {
        Logger.getAnonymousLogger().log(Level.INFO,"Login Filter initialized");
    }
    
    /** The main filter method handling the requests.
     *
     * @param req is the current request that triggered the filter.
     * @param res is the response we will allow
     * @param chain is just a chain that makes it possible to combine multiple filters.
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession();
        String userAtt = (String) session.getAttribute("USER");
        boolean invalid = true;
        
        if (userAtt == null) {
            String cookieName = (String) request.getServletContext().getAttribute(Keys.COOKIE_NAME.toString());
            String uuid = CookieHelper.getCookieValue(request, cookieName);
            if(uuid != null){
                User user = DAO.getRememberMe(uuid);
                if(user != null){
                    request.login(user.getUuid(), user.getPassword());
                    request.getSession().setAttribute("USER", user.getUserName());
                    String name = (String) session.getServletContext().getAttribute(Keys.COOKIE_NAME.toString());
                    int age = (int) session.getServletContext().getAttribute(Keys.COOKIE_AGE.toString());
                    CookieHelper.addCookie(response, name, uuid, age);
                    if (user.isAdmin()){
                        response.sendRedirect(Navigation.ADMIN.toString());
                    } else {
                        response.sendRedirect(Navigation.USER.toString());
                    }
                    invalid = false;
                } else {
                    CookieHelper.removeCookie(response, Keys.COOKIE_NAME.toString());
                }
            }
        }
        if(invalid)
        chain.doFilter(request, response);
    }
    
    @Override
    public void destroy() {
        Logger.getAnonymousLogger().log(Level.INFO,"Login Filter destoryed");
    }
}
