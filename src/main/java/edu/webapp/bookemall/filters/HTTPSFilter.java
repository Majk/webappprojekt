package edu.webapp.bookemall.filters;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A filter checking all connections to the website.
 * 
 * NO HTTP connections are allowed. All must be
 * of HTTPS, i.e secure! so if an incoming request
 * is of HTTP then this filter will automatically
 * redirect to HTTPS so the user shouldn't
 * expose their information by mistake.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 * @author Nabi Zamani
 */
@WebFilter(filterName = "HTTPSFilter", urlPatterns = {"/*"})
public class HTTPSFilter implements Filter {
    
    @Override
    public void init(FilterConfig config) throws ServletException {
        Logger.getAnonymousLogger().log(Level.INFO,"HTTPSFilter initialized");
    }  
    
    /** HTTPS filter.
     * 
     * Redirect all HTTP requests going to our login/registration page 
     * to their HTTPS equivalents.
     *
     * @param req is the current request that triggered the filter.
     * @param res is the response we will allow
     * @param chain is just a chain that makes it possible to combine multiple filters.
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        
        if (!request.isSecure()) {
            String url = request.getRequestURL().toString().replaceFirst("http", "https");
            url = url.replaceFirst(":8080/", ":8181/");
            
            if (request.getQueryString() != null){
                url += "?" + request.getQueryString();
            }
            response.sendRedirect(url);
        } else {
            chain.doFilter(req, res);
        }
    }
    
    @Override
    public void destroy() {
        Logger.getAnonymousLogger().log(Level.INFO,"HTTPSFilter destoryed");
    }
}
