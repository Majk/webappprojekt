package edu.webapp.bookemall.persistence;

import edu.webapp.bookemall.booking.Booking;
import edu.webapp.bookemall.booking.Booking_;
import edu.webapp.bookemall.booking.DateStamp_;
import edu.webapp.bookemall.booking.User_;
import edu.webapp.bookemall.booking.Venue_;
import java.util.LinkedList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;


/**
 * Stateless class for interacting with the jta database
 * 
 * @author Johan W-Schützer <Johan.DSS@Gmail.com>
 */
@Stateless
public class BookingDAO extends AbstractDAO<Booking, String> implements IBookingDAO{
    
    
    private static final String JTAPU = "bookemall_jta_pu";
    
    @PersistenceContext(unitName = JTAPU)
    private EntityManager em;
    
    /**
     * Default empty constructor
     */
    public BookingDAO() {
        super(Booking.class);
    }
    
    /**
     * Gets the entitymanager used by the DAO
     * @return EntityManager
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    /**
     * Get the bookings created by a User by its uuid
     *
     * @param uuid of the creator of the booking
     * @return the events created by the user if any, otherwise null.
     */
    @Override
    public List<Booking> getByCreator(String uuid) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Booking> query = cb.createQuery(Booking.class);
        Root<Booking> root = query.from(Booking.class);
        From join = root.join(Booking_.creator);
        Path<Booking> p = join.get(User_.Uuid);
        Predicate predicate = cb.equal(p, uuid);
        query.where(predicate);
        query.select(root);
        try{
            return em.createQuery(query).getResultList();
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No such userName found");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for creator with uuid: " + uuid, e);
        }
        return null;
    }
    
    /**
     * Gets all the bookings that contains a certain part of a name
     * @param eventName the name to query by
     * @return a list with all the matching booking
     */
    @Override
    public List<Booking> getByEventName(String eventName) {
        List<Booking> bookingsByName = new LinkedList<>();
        eventName = eventName.trim().toLowerCase();
        for(Booking booking : getAll()) {
            String check = booking.getEventName().trim().toLowerCase();
            if(check.contains(eventName)) {
                bookingsByName.add(booking);
            }
        }
        return bookingsByName;
    }
    
    /**
     * Gets all bookings with the venue with matching uuid
     * @param uuid of the venue
     * @return List of bookings
     */
    @Override
    public List<Booking> getByVenue(String uuid) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Booking> query = cb.createQuery(Booking.class);
        Root<Booking> root = query.from(Booking.class);
        From join = root.join(Booking_.venue);
        Path<Booking> p = join.get(Venue_.Uuid);
        Predicate predicate = cb.equal(p, uuid);
        query.where(predicate);
        query.select(root);
        try{
            return em.createQuery(query).getResultList();
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No such venue found");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for venue with ID: " + uuid, e);
        }
        return null;
    }
    
    /**
     * Gets all bookings with the date stamp with matching uuid
     * @param uuid of the date stamp
     * @return List of bookings
     */
    @Override
    public List<Booking> getByDateStamp(String uuid) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Booking> query = cb.createQuery(Booking.class);
        Root<Booking> root = query.from(Booking.class);
        From join = root.join(Booking_.dateStamp);
        Path<Booking> p = join.get(DateStamp_.Uuid);
        Predicate predicate = cb.equal(p, uuid);
        query.where(predicate);
        query.select(root);
        try{
            return em.createQuery(query).getResultList();
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No such dateStamp found");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for dateStamp with uuid: " + uuid, e);
        }
        return null;
    }
    
    /**
     * Get the bookings for a user with given uuid who is admin or guest
     *
     * @param uuid of the given attender
     * @return all bookings a user is attending or has attended, if none null
     */
    @Override
    public List<Booking> getByAttender(String uuid) {
        List<Booking> result = new LinkedList<>();
        try{
            result.addAll(getByGuest(uuid));
            result.addAll(getByAdmin(uuid));
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No such userName found");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for attender with uuid: " + uuid, e);
        }
        return result;
    }
    
    /**
     * Get the bookings for a user with given uuid who is admin
     *
     * @param uuid of the admin
     * @return all bookings a user is admin, if none null
     */
    @Override
    public List<Booking> getByAdmin(String uuid) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Booking> query = cb.createQuery(Booking.class);
        Root<Booking> root = query.from(Booking.class);
        From adminsJoin = root.join(Booking_.admins);
        Path<Booking> adminPath = adminsJoin.get(User_.Uuid);
        Predicate isAdmin = cb.equal(adminPath, uuid);
        query.where(isAdmin);
        query.select(root);
        try{
            return em.createQuery(query).getResultList();
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No such userName found");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for admin with uuid: " + uuid, e);
        }
        return null;
    }
    
    /**
     * Get the bookings for a user with given uuid who is guest
     *
     * @param uuid of the user
     * @return all bookings a user is attending or has attended, if none null
     */
    @Override
    public List<Booking> getByGuest(String uuid) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Booking> query = cb.createQuery(Booking.class);
        Root<Booking> root = query.from(Booking.class);
        From guestsJoin = root.join(Booking_.guests);
        Path<Booking> guestPath = guestsJoin.get(User_.Uuid);
        Predicate isGuest = cb.equal(guestPath, uuid);
        query.where(isGuest);
        query.select(root);
        try{
            return em.createQuery(query).getResultList();
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No such userName found");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for guest with uuid: " + uuid, e);
        }
        return null;
    }
}
