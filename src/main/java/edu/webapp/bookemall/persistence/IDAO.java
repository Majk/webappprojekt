package edu.webapp.bookemall.persistence;

import java.util.List;
import javax.ejb.Local;

/**
 * A Data Access Object interface (IDAO) from Von Hachts examples.
 * 
 * @author Gustaf Ringius <gustaf@linux.com>
 * @author hajo
 */
@Local  // Bean run in same JVM
public interface IDAO<T, K> {

    public void add(T t);

    public void remove(K id);

    public T update(T t);

    public T find(K id);

    public List<T> getAll();

    public List<T> getRange(int maxResults, int firstResult);

    public int getCount();
}
