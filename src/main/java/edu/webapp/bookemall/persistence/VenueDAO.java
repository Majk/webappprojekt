package edu.webapp.bookemall.persistence;

import edu.webapp.bookemall.booking.Address;
import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.User_;
import edu.webapp.bookemall.booking.Venue;
import edu.webapp.bookemall.booking.Venue_;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * DAO for Venue
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
@Stateless
public class VenueDAO extends AbstractDAO<Venue, String> implements IVenueDAO {
    
    private static final String JTAPU = "bookemall_jta_pu";
    
    @PersistenceContext(unitName=JTAPU)
    private EntityManager em;
    
    public VenueDAO() {
        super(Venue.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
        /**
     * Gets all the venues with an exact name
     * @param venueName the name to query by
     * @return a specific venue or null
     */
        @Override
    public Venue getByExactVenueName(String venueName) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Venue> query = cb.createQuery(Venue.class);
        Root<Venue> root = query.from(Venue.class);
        query.select(root).where(cb.equal(root.get(Venue_.venueName), venueName));
        try{
            return em.createQuery(query).getSingleResult();
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No such venueName found");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for venue with exact venueName: " + venueName, e);
        }
        return null;
    }
    
    
    /**
     * Gets all the venues that contain a certain name-part
     * @param venueName the name to query by
     * @return a list with all the matching venues
     */
    @Override
    public List<Venue> getByVenueName(String venueName) {
       List<Venue> venueByName = new LinkedList<>();
       venueName = venueName.trim().toLowerCase();
        for(Venue venue : getAll()) {
            String check = venue.getVenueName().trim().toLowerCase();
            if(check.contains(venueName)) {
                venueByName.add(venue);
            }
        }
        return venueByName;
    }
    
    /**
     * Gets all the venues with a certain creator
     * @param creator the creator
     * @return a list with all the matching venues
     */
    @Override
    public List<Venue> getByCreator(User creator) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Venue> query = cb.createQuery(Venue.class);
        Root<Venue> root = query.from(Venue.class);
        List<Venue> list = new LinkedList<>();
        try{
            query.select(root).where(cb.equal(root.get("creator"), creator));
            list.addAll(em.createQuery(query).getResultList());
        } catch (Exception e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for venues with creator: " + creator.getUserName(), e);
        }
        return list;
    }
    
    /**
     * Gets all the venues that user is admin for, gets this info by adminId
     * @param admin the users ID
     * @return list of all the venues that the user is admin for
     */
    @Override
    public List<Venue> getByAdmin(User admin) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Venue> query = cb.createQuery(Venue.class);
        Root<Venue> root = query.from(Venue.class);
        List<Venue> list = new LinkedList<>();
        try{
            From adminsJoin = root.join(Venue_.admins);
            Path<Venue> adminPath = adminsJoin.get(User_.Uuid);
            Predicate isAdmin = cb.equal(adminPath, admin.getUuid());
            query.where(isAdmin);
            query.select(root);
            list.addAll(em.createQuery(query).getResultList());
        } catch (Exception e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for admins of venues", e);
        }
        return list;
    }
    
    /**
     * Searches for all the Venues that has from a number of seats to a number of seats
     * @param from how many seats at least
     * @param to how many seats at most
     * @return a list of all the seats
     */
    @Override
    public List<Venue> getByNumberOfSeats(int from, int to) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Venue> query = cb.createQuery(Venue.class);
        Root<Venue> root = query.from(Venue.class);
        List<Venue> list = new LinkedList<>();
        try{
            query.select(root).where(cb.and(cb.greaterThanOrEqualTo(root.<Integer> get("seats"), from),
                    cb.lessThanOrEqualTo(root.<Integer> get("seats"), to)));
            list.addAll(em.createQuery(query).getResultList());
        } catch (Exception e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for venues with seats from: " + from + " to: " + to, e);
        }
        return list;
    }
    
    /**
     * Gets all the venues that match a specific address and returns them
     * @param address where the venue is at
     * @return all venues at that Address
     */
    @Override
    public List<Venue> getByAddress(Address address) {
        List<Venue> venuesByAddress = new LinkedList<>();
        for(Venue venue : getAll()) {
            if(addressMatch(venue.getAddress(), address)) {
                venuesByAddress.add(venue);
            }
        }
        return venuesByAddress;
    }
     /**
     * Checks if two addresses are considered equal on the parameters of their street, city and country
     * @param first one address
     * @param second another
     * @return true if a match
     */
    private boolean addressMatch(Address first, Address second) {
        if(compareStrings(first.getCountry(), second.getCountry())) {
            return false;
        }
        if(compareStrings(first.getCity(), second.getCity())) {
            return false;
        }
        if(compareStrings(first.getStreet(), second.getStreet())) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Compare two strings to see if they are equal
     * @param first String to check
     * @param second String to check
     * @return true if not equal
     */
    private boolean compareStrings(String first, String second) {
        return first != null && second != null && 
                !first.isEmpty() && !second.isEmpty() && 
                !first.equalsIgnoreCase(second);
    }
    
}
