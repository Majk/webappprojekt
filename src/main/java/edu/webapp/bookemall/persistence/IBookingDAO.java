package edu.webapp.bookemall.persistence;

import edu.webapp.bookemall.booking.Booking;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface for a BookingDAO
 * @author Johan W-Schützer <Johan.DSS@Gmail.com>
 */
@Local
public interface IBookingDAO extends IDAO<Booking, String>{
    
    /**
     * Get the bookings created by a User by its uuid
     *
     * @param uuid of the creator of the booking
     * @return the events created by the user if any, otherwise null.
     */
    List<Booking> getByEventName(String bookingName);
    
    /**
     * Gets all the bookings that contains a certain part of a name
     * @param eventName the name to query by
     * @return a list with all the matching booking
     */
    List<Booking> getByCreator(String uuid);
    
    /**
     * Gets all bookings with the venue with matching uuid
     * @param uuid of the venue
     * @return List of bookings
     */
    List<Booking> getByVenue(String uuid);
    
    /**
     * Gets all bookings with the date stamp with matching uuid
     * @param uuid of the date stamp
     * @return List of bookings
     */
    List<Booking> getByDateStamp(String uuid);
    
    /**
     * Get the bookings for a user with given uuid who is admin or guest
     *
     * @param uuid of the given attender
     * @return all bookings a user is attending or has attended, if none null
     */
    List<Booking> getByAttender(String uuid);
    
    /**
     * Get the bookings for a user with given uuid who is admin
     *
     * @param uuid of the admin
     * @return all bookings a user is admin, if none null
     */
    List<Booking> getByAdmin(String uuid);
    
    /**
     * Get the bookings for a user with given uuid who is guest
     *
     * @param uuid of the user
     * @return all bookings a user is attending or has attended, if none null
     */
    List<Booking> getByGuest(String uuid);
}
