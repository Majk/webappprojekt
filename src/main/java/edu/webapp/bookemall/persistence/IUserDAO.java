package edu.webapp.bookemall.persistence;

import edu.webapp.bookemall.booking.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
@Local
public interface IUserDAO extends IDAO<User, String>{

    public User getByUserName(String userName);
    public List<User> getByName(String name);
    public List<User> getAdmins();
    public User findLoginCredentials(String userName, String password);
    public void promoteUser(User user);
    public User getRememberMe(String cookieNr);
    public void deleteRememberMe(String userName);
    public User getUserFromMailToken(String mailToken);
}
