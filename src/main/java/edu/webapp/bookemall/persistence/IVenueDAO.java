package edu.webapp.bookemall.persistence;

import edu.webapp.bookemall.booking.Address;
import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.Venue;
import java.util.List;
import javax.ejb.Local;

/**
 *
 *  @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
@Local
public interface IVenueDAO extends IDAO<Venue, String>{
     /**
      * Gets the exact match of a venues name
      * @param venueName Exact name to check
      * @return The venue if found
      */
     public Venue getByExactVenueName(String venueName);
     /**
      * Gets a list of all venues which name contains a certain string
      * @param venueName String to match
      * @return All matches found
      */
     public List<Venue> getByVenueName(String venueName);
     /**
      * Gets a list of all venues a user is admin for
      * @param admin user which is admin
      * @return All matches found
      */
     public List<Venue> getByAdmin(User admin);
     /**
      * Gets a list of all venues which match a certain range of seats
      * @param from nr of seats
      * @param to nr of seats
      * @return All matches found
      */
     public List<Venue> getByNumberOfSeats(int from, int to);
     /**
      * Gets a list of all venues at a certain address
      * @param address where the venue is at
      * @return All matches found
      */
     public List<Venue> getByAddress(Address address);
     /**
      * Gets a list of all the venues a user is creator of
      * @param creator User
      * @return All matches found
      */
     public List<Venue> getByCreator(User creator);
    
}
