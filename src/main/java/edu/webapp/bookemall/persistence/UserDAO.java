package edu.webapp.bookemall.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import edu.webapp.bookemall.booking.User;
import edu.webapp.bookemall.booking.User.Group;
import edu.webapp.bookemall.booking.User_;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

/**
 * User Data Access Object handling users.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
@Stateless
public class UserDAO extends AbstractDAO<User, String> implements IUserDAO{
    
    private static final String JTAPU = "bookemall_jta_pu";
    
    @PersistenceContext(unitName=JTAPU)
    private EntityManager em;
    
    public UserDAO() {
        super(User.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    /**
     * Gets a user by his/her userName.
     *
     * @param userName linked to the user to look for.
     * @return the user if found, otherwise null.
     */
    @Override
    public User getByUserName(String userName) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(cb.equal(root.get(User_.userName), userName));
        try{
            return em.createQuery(query).getSingleResult();
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No such userName found");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for user with userName: " + userName, e);
        }
        return null;
    }
    
    /**
     * Gets all users that have the String parameter name
     * in either their first, last name or both names.
     *
     * @param name to search for.
     * @return a list of users found, if none empty list.
     */
    @Override
    public List<User> getByName(String name) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        List<User> list = new ArrayList<>();
        try{
            query.select(root).where(cb.equal(root.get(User_.firstName), name));
            list.addAll(em.createQuery(query).getResultList());
            query.select(root).where(cb.equal(root.get(User_.lastName), name));
            list.addAll(em.createQuery(query).getResultList());
        } catch (Exception e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for user with name: " + name, e);
        }
        return list;
    }
    
    /**
     * Gets all the site administrators.
     *
     * @CollectionTable is some magic I do not understand.
     *
     * Usual queries is not possible. Join is not possible.
     * Future TODO
     *
     * @return a list of all the site administrators, if none empty list.
     */
    @Override
    public List<User> getAdmins() {
        List<User> admins = new ArrayList<>();
        for (User u : getAll()){
            if (u.isAdmin()){
                admins.add(u);
            }
        }
        return admins;
    }
    
    /** Checks for a user's login Credentials.
     *
     * @param userName of the user
     * @param password of the user
     * @return the user object if check passed, otherwise null
     */
    @Override
    public  User findLoginCredentials(String userName, String password) {
        User user = getByUserName(userName);
        if (user != null && password.equals(user.getPassword())){
            return user;
        }
        return null;
    }
    
    /**
     * Promotes a user to Administrator of the site.
     *
     * @param user to promote
     */
    @Override
    public void promoteUser(User user) {
        if (!user.getGroups().contains(Group.ADMIN)){
            user.setAdmin();
            update(user);
        }
    }
        
    /**
     * Gets a user from a rememberCookieNr.
     *
     * @param cookieNr of the user
     * @return the found user, or if none null
     */
    @Override
    public User getRememberMe(String cookieNr) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(cb.equal(root.get(User_.rememberCookie), cookieNr));
        try{
            return em.createQuery(query).getSingleResult();
        } catch (NoResultException e){
            Logger.getAnonymousLogger().log(Level.WARNING, "No RememberMe Cookie Number matching");
        } catch (Exception e){
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception was thrown when searching for cookieNr: " + cookieNr, e);
        }
        return null;
    }
    
    @Override
    public void deleteRememberMe(String userName) {
        User user = getByUserName(userName);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(cb.equal(root.get(User_.Uuid), user.getUuid()));
        User found = em.createQuery(query).getSingleResult();
        if (found != null){
            user.setRememberCookie(null);
            update(user);
        }
    }

    @Override
    public User getUserFromMailToken(String mailToken) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(cb.equal(root.get(User_.mailToken), mailToken));
        try{
            return em.createQuery(query).getSingleResult();
        } catch (Exception e) {
            
        }
        return null;
    }
}
