package edu.webapp.bookemall.persistence;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Based upon the workshop EJB with JTA.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
public abstract class AbstractDAO<T, K> implements IDAO<T, K> {
    
    private final Class<T> clazz;
    
    protected AbstractDAO(Class<T> clazz){
        this.clazz = clazz;
    }
    
    protected abstract EntityManager getEntityManager();
    
    /** Adds an object to the database.
     *
     * @param t : object to add.
     */
    @Override
    public void add(T t) {
        getEntityManager().persist(t);
        // Have to force the EntityManager to evict the cache on each add.
        // This has to be done to get the @OneToMany-lists to be filled with the new data
        // Doing this makes all cascades work perfectly
        getEntityManager().getEntityManagerFactory().getCache().evictAll();
    }
    
    /** Removes an object.
     *
     * @param id of the object to remove
     */
    @Override
    public void remove(K id) {
        getEntityManager().remove(getEntityManager().getReference(clazz, id));
    }
    
    /** Updates an object.
     *
     * @param t of object to update
     * @return the updated object
     */
    @Override
    public T update(T t) {
        return getEntityManager().merge(t);
    }
    
    /** Finds item in table.
     *
     * @param id of the object to find
     * @return the found object, if none null
     */
    @Override
    public T find(K id) {
        return getEntityManager().find(clazz, id);
    }
    
    /** Gets a list of objects from the database.
     *
     * @param first object in range
     * @param nItems number of objects in range
     * @return a list of objects in range.
     */
    @Override
    public List<T> getRange(int first, int nItems) {
        List<T> found = new ArrayList<>();
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(clazz));
        Query q = getEntityManager().createQuery(cq);
        
        q.setFirstResult(first);
        q.setMaxResults(nItems);
        
        found.addAll(q.getResultList());
        
        return found;
    }
    
    /** Gets the amount of objects in database.
     *
     * @return the total amount of objects.
     */
    @Override
    public int getCount() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<T> rt = cq.from(clazz);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        int count = ((Long) q.getSingleResult()).intValue();
        return count;
    }
    
    /** Returns all objects
     *
     * @return all objects
     */
    @Override
    public List<T> getAll() {
        return getRange(0, getCount());
    }
}
