package edu.webapp.bookemall.util;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import javax.naming.NamingException;

/**
 * A mailer class that handles JAVA MAIL.
 *
 * This is a mail service going over TLS that
 * handles sending of messages from the server.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
public final class Mailer {
    
    private Session mailSession;
    // Just linked to a google mail account for now
    private final String LOGIN = "bookemall.server";
    private final String PASSWORD = "JagGillarBanan";
    
    /** 
     * Method sending messages from the server to a client.
     * 
     * @param email is the server email address
     * @param recipient is the user's email address
     * @param subject is what the message is about
     * @param plainText is the main content in plan text
     * @param htmlText is content in html with tags
     * @throws MessagingException
     * @throws NamingException 
     */
    public void sendMessage(String email, String recipient, String subject,
            String plainText, String htmlText) throws MessagingException, NamingException{
        
        Properties props = new Properties();
        props.setProperty("mail.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        
        Authenticator auth = new SMTPAuthenticator(LOGIN, PASSWORD);
        
        mailSession = Session.getInstance(props, auth);
        
        MimeMessage message = new MimeMessage(mailSession);
        InternetAddress from = new InternetAddress(email);
        InternetAddress to = new InternetAddress(recipient);
        
        message.setSubject(subject);
        message.setFrom(from);
        message.addRecipient(Message.RecipientType.TO, to);
        Multipart multipart = new MimeMultipart();
        
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(plainText);
        multipart.addBodyPart(messageBodyPart);
        messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(htmlText, "text/html");
        
        multipart.addBodyPart(messageBodyPart);
        
        message.setContent(multipart);
        Transport transport = mailSession.getTransport("smtps");
        try {
            transport.connect(LOGIN, PASSWORD);
            transport.sendMessage(message, message.getAllRecipients());
            Logger.getAnonymousLogger().log(Level.INFO, "Message sent");
        }
        catch (Exception e) {
            System.out.println("Unable to send message." + e);
        }
        finally {
            transport.close();
        }
    }
    
    /**
     *  Helper class for Authentication
     */
    private class SMTPAuthenticator extends Authenticator {
        
        private PasswordAuthentication authentication;
        
        /**
         * Method for creating an authentication data holder.
         * 
         * Simply a repository for a user name and a PASSWORD.
         * 
         * @param LOGIN is the username
         * @param PASSWORD is the token linked to the username
         */
        public SMTPAuthenticator(String login, String password) {
            authentication = new PasswordAuthentication(login, password);
        }
        
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
}
