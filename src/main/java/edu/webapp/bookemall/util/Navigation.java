package edu.webapp.bookemall.util;

/**
 * Enum for controlling navigation constants
 * in one easy to find place.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
public enum Navigation {
    ADMIN("/webappprojekt/admin/controlPanel.xhtml"),
    USER("/webappprojekt/users/timeline.xhtml"),
    LOGOUT("/webappprojekt/index.jspx"),
    LOGIN("/webappprojekt/index.jspx"),
    CONFIRMED("/webappprojekt/index.jspx?confirmed=true"),
    NOTCONFIRMED("/webappprojekt/index.jspx?notConfirmed=true"),
    INVALIDLOGIN("/webappprojekt/index.jspx?authInValid=true");
    
    private final String page;
    
    private Navigation(final String page) {
        this.page = page;
    }
    
    @Override
    public String toString() {
        return page;
    }
    
}
