package edu.webapp.bookemall.util;

/**
 * Enum with keys for stored constants.
 *
 * @author Gustaf Ringius <gustaf@linux.com>
 */
public enum Keys {
    COOKIE_AGE,
    RECAPTCHA,
    COOKIE_NAME;
}
