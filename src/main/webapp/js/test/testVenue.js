test("venueHandler.getAll", function() {
    var data = vProxy.getAll();
    ok(data, "Test ok, received venues from server");
});

test("venueHandler.getById", function() {
    var allVenues = vProxy.getAll();
    var venue = elementFromArray(allVenues, 'venue');
    var uuid = venue.uuid;
    var data = vProxy.getById(uuid, ok);
    ok(data.uuid === uuid, "Test ok");
});

test("venueHandler.getByAddress", function() {
    var allVenues = vProxy.getAll();
    var venue = elementFromArray(allVenues, 'venue');

    var country = venue.address.country;
    var city = venue.address.city;
    var street = venue.address.street;
    var number = 0;

    var data = vProxy.getByAddress(street, number, city, country);

    data = elementFromArray(data, 'venue');

    ok(data.uuid === venue.uuid, "UUID's match");
});

test("venueHandler.getByVenueName", function() {
    var allVenues = vProxy.getAll();

    var venue = elementFromArray(allVenues, 'venue');

    var data = vProxy.getByVenueName(venue.venueName);
    data = elementFromArray(data, 'venue');

    ok(data.uuid === venue.uuid, "Uuids match");
});

test("venueHandler.getByCreator", function() {
    var loggedUser = uProxy.getUser();
    var data = vProxy.getByCreator();
    data = elementFromArray(data, 'venue');
    ok(data.creator.uuid === loggedUser.uuid, "Uuids match");
});

test("venueHandler.getByAdmins", function() {
    var allVenues = vProxy.getAll();

    var venue = elementFromArray(allVenues, 'venue');

    var admins = venue.admins;
    var admin = elementFromArray(admins, 'user');

    var data = vProxy.getByAdmin(admin.uuid);

    data = elementFromArray(data, 'venue');
    data = elementFromArray(data.admins, 'user');

    ok(data.uuid === admin.uuid, "UUIDs match");
});


asyncTest("venueHandler.updateVenue", function() {
    var vP = vProxy;
    var createdVenues = vP.getByCreator();

    var venue = elementFromArray(createdVenues, 'venue');

    var savedNbrSeats = venue.seats;
    venue.seats = savedNbrSeats + 1;

    var response = vP.updateVenue(venue, ok);

    response.done(function() {
        var updatedVenue = vP.getById(venue.uuid, ok);
        ok(venue.uuid === updatedVenue.uuid, "Update success");

        venue.seats = savedNbrSeats;
        vP.updateVenue(venue, start);
    });

    response.fail(function() {
        ok(false, "Run failed");
        QUnit.start();
    });
});

test("venueHandler.getAdmins", function() {
    var allVenues = vProxy.getAll();
    allVenues = allVenues instanceof Array ? allVenues : allVenues.venue;
    var venue = elementFromArray(allVenues, 'venue');
    var admins;
    while (venue && allVenues.length > 0) {
        admins = vProxy.getAdmins(venue.uuid);
        console.log(admins);
        if (admins) {
            ok(admins, "There was admins");
        }
        if (allVenues.length === 1) {
            allVenues = null;
        } else {
            allVenues = allVenues.splice(0, 1);
        }
        venue = elementFromArray(allVenues, 'venue');
    }
});

test("venueHandler.getInvokeList", function() {
    var allVenues = vProxy.getAll();

    var venue = elementFromArray(allVenues, 'venue');

    var invokes = vProxy.getInvokeList(venue.uuid);
    var user = elementFromArray(invokes, "user");
    ok(user, "There was user to be invoked");
});

asyncTest("venueHandler.invokeDemoteAdmin", function() {
    var vP = vProxy;
    var createdVenues = vP.getByCreator();

    var meUser = uProxy.getUser();
    var venue = elementFromArray(createdVenues, 'venue');
    var invokeList = vP.getInvokeList(venue.uuid);
    var toInvoke = elementFromArray(invokeList, 'user');
    console.log(toInvoke);
    var response = vP.invokeAdminStatus(venue.uuid, toInvoke.uuid, ok);

    response.done(function() {
        var updatedVenue = vP.getById(venue.uuid, $.noop());
        console.log(updatedVenue);

        var admins = updatedVenue.admins;
        admins = admins instanceof Array ? admins : admins.user;
        ok(contains(admins, toInvoke), "Invoke success");

        vP.revokeAdminStatus(venue.uuid, toInvoke.uuid, function() {
        });
        ok(!contains(updatedVenue.admins, toInvoke, "Demote success"));

        start();
    });

    response.fail(function() {
        ok(false, "Run failed");
        QUnit.start();
    });
});


test("venueHandler.getRegular", function() {
    var regulars = vProxy.getRegular();
    var venue = elementFromArray(regulars, "venue");
    var loggedUser = uProxy.getUser();
    ok(!contains(venue, loggedUser), "Test passed");
});

asyncTest("venueHandler.addVenue and venueHandler.deleteVenue", function() {
    var vP = vProxy;
    var me = this;
    var venue = {
        venueName: 'weirdUniqueTestName',
        seats: '9999',
        address: {
            street: '123456',
            nbr: '1',
            city: 'TestTown',
            country: 'TestCountry'
        }
    };

    var response = vP.addVenue(venue, ok);

    response.done(function() {
        var addedVenue = vP.getByVenueName("weirdUniqueTestName", ok);
        addedVenue = elementFromArray(addedVenue, 'venue');
        ok(addedVenue.venueName === "weirdUniqueTestName", "Add success");
        stop();
        var clean = vP.getByVenueName("weirdUniqueTestName", ok);
        clean = elementFromArray(clean, 'venue');
        var resp2 = vP.deleteVenue(clean.uuid, function() {
        });
        resp2.done(function() {
            clean = vP.getByVenueName("weirdUniqueTestName", $.noop());
            console.log(clean);
            ok(!clean, "Delete Success");
            start();
        }, 150);

        resp2.fail(function() {
            ok(false, "Run failed");
            QUnit.start();
        });
    });

    response.fail(function() {
        ok(false, "Run failed");
        QUnit.start();
    });
});