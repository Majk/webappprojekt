test("bookingHandler.getAll", function() {
    var data = bProxy.getAll();
    ok(data, "Test ok, received bookings from server");
});

test("bookingHandler.getByEventName", function() {
    var allBookings = bProxy.getAll();
    var booking = elementFromArray(allBookings, "booking");
    var data = bProxy.getByEventName(booking.eventName);
    data = elementFromArray(data, "booking");
    ok(data.uuid === booking.uuid, "Uuids match");
});

test("bookingHandler.getBookingById", function() {
    var allBookings = bProxy.getAll();
    var booking = elementFromArray(allBookings, "booking");
    var data = bProxy.getBookingById(booking.uuid);
    ok(data.uuid === booking.uuid, "Test ok");
});

test("bookingHandler.getByAttender", function() {
    var allBookings = bProxy.getAll();
    var booking;

    var user;
    while (!user && allBookings.length > 0) {
        booking = elementFromArray(allBookings, "booking");
        user = elementFromArray(booking.admins, "user");
        if (!user) {
            user = elementFromArray(booking.guests, "user");
        }
        if (allBookings instanceof Array && allBookings.length > 0 && !user) {
            allBookings = allBookings.splice(0, 1);
        }
    }

    var attenderBookings = bProxy.getBookingByAttender(user.uuid);
    attenderBookings = attenderBookings instanceof Array ? attenderBookings : attenderBookings.booking;
    ok(contains(attenderBookings, booking), "Test ok");
});

test("bookingHandler.getByCreator", function() {
    var createdBookings = bProxy.getBookingByCreator();
    var loggedInUser = uProxy.getUser();
    console.log(createdBookings)
    createdBookings = createdBookings instanceof Array ? createdBookings : createdBookings.booking;
    if (createdBookings instanceof Array) {
        $.each(createdBookings, function(i, booking) {
            console.log(booking);
            ok(booking.creator.uuid === loggedInUser.uuid, "Test ok, uuid " + loggedInUser.uuid + " matched");
        });
    } else {
        ok(createdBookings.creator.uuid === loggedInUser.uuid, "Test ok, uuid " + loggedInUser.uuid + " matched");
    }
});

asyncTest("bookingHandler.updateBooking", function() {
    var bP = bProxy;
    var allBookings = bP.getAll(ok);
    var booking;
    if (allBookings.booking instanceof Array) {
        allBookings = allBookings.booking;
        booking = allBookings[0];
    } else {
        booking = allBookings.booking;
    }
    ;

    var savedEventName = booking.eventName;
    booking.eventName = "testName";

    var response = bP.updateBooking(booking, ok);

    response.done(function() {
        var data = bP.getBookingById(booking.uuid, ok);
        ok(data.eventName === "testName", "Test ok");

        booking.eventName = savedEventName;
        bP.updateBooking(booking, start);
    });

    response.fail(function() {
        ok(false, "Run failed");
        start();
    });
});