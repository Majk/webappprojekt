test("userHandler.getAll", function() {
    var data = uProxy.getAll(ok);
    ok(data, "Test ok, received users from server");
});

test("userHandler.getUserById", function() {
    var allUsers = uProxy.getAll(ok);
    var user = elementFromArray(allUsers, 'user');
    var uuid = user.uuid;
    var data = uProxy.getUserById(uuid, ok);
    ok(data.uuid === uuid , "Test ok"); 
});

test("userHandler.getUser", function() {
    var data = uProxy.getUser();
    ok(data, "Test ok, received users from server");
});

asyncTest("userHandler.updateUser", function() {
    var uP = uProxy;
    var allUsers = uP.getAll(ok);
    
    var user = elementFromArray(allUsers, 'user');
    
    var savedUserName = user.firstName;
    user.firstName = "testName";

    var response = uP.updateUser(user, ok);
    
    response.done(function(){
       var data = uP.getUserById(user.uuid, ok);
       ok(data.firstName === "testName", "Test ok");

       user.firstName = savedUserName;
       uP.updateUser(user, start);
    });
    
    response.fail(function(){
       ok(false, "Run failed");
       QUnit.start();
    });
});