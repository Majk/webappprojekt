var elementFromArray = (function(array, propertyName) {

    if (array) {
        while (array[propertyName] instanceof Array) {
            array = array[propertyName];
        }
        if (array instanceof Array) {
            array = array[0];
        }
        if (array[propertyName]) {
            array = array[propertyName];
        }
    }

    return array;
});

var contains = (function(array, entity) {
    if (array instanceof Array && entity) {
        var i = array.length;
        while (i--) {
            if (entity.uuid && array[i].uuid) {
                if (entity.uuid === array[i].uuid) {
                    return true;
                }
            }
        }
    }
    return false;
});