/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

asyncTest("Ajax.get", function() {
    stop();
    var baseUri = "https://localhost:8181/webappprojekt/users/timeline.xhtml";
    var response = $.ajax(
            {
                type: "GET",
                url: this.baseUri,
                success: function(data) {
                    ok(data, "Test passed!");
                    start();
                },
                error: function(data) {
                    ok(false, "Is server up ?");
                    start();
                }
            });
    response.done(function(data) {
        ok(data, "Test passed!");
        start();
    });
    response.fail(function(data) {
        ok(false, "Test failed!");
        start();
    });
});
