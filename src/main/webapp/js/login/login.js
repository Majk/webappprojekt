/* 
 * This script handles/displays all the dynamic updating,
 * that needs to be done for the login page.
 */



$(function() {
    
    /*
     * Button/link controls for dialogs/alerts
     */
    $("#newUser").on("click", function() {
        createAddUserDialog().dialog("open"); 
    });
    $("#dialoglink").on("click", function() {
        openPolicyDialog().dialog("open");  
    });
    $("#forgot").on("click", function() {
        openCodeDialog().dialog("open");
    });   
    
    /* 
     * Creates a dialog for adding new users.
     * 
     * @returns {unresolved}
     */
    function createAddUserDialog() {
        reloadRecaptcha(); //load recaptcha
        $("#dialog-form").dialog({
            autoOpen: false,
            modal: true,
            stack: true,
            maxHeight : 800,
            width: 500,
            buttons: {
                Submit: function() {
                    var me = this;
                    if ($("#register").valid()){
                        $.ajax({
                            type: "POST",
                            url: "login?action=verify",
                            async: false,
                            data: {
                                challenge: Recaptcha.get_challenge(),
                                response: Recaptcha.get_response()
                            },
                            success: function (ret){
                                if(ret === "true"){
                                    addUser();
                                    clearAddForm();
                                    $(me).dialog("close");
                                    Recaptcha.destroy();
                                }else{
                                    createDialog("You entered an invalid captcha, are you human?", "PEBKAC").dialog("open");
                                    reloadRecaptcha();
                                }
                            }
                        });
                    }
                },
                Cancel: function() {
                    clearAddForm();
                    $(this).dialog("close");
                }
            }
        });
        $("#dialog-form").dialog('option', 'title', 'Welcome');
        $("#dialog-form fieldset").text();
        return $('#dialog-form');
    }
    
    /*
     * Opens the new password reset code
     * form for user password reset.
     * 
     * @returns {unresolved}
     */
    function openCodeDialog() {
        $("#code-form").dialog({
            autoOpen: false,
            modal: true,
            stack: true,
            maxHeight : 800,
            width: 400,
            buttons: {
                GetCode: function() {
                    getCode();
                    clearCodeForm();
                    $(this).dialog("close");
                },
                Cancel: function() {
                    clearCodeForm();
                    $(this).dialog("close");
                }
            }
        });
        $("#code-form").dialog('option', 'title', 'Get reset code');
        $("#code-form fieldset").text();
        return $('#code-form');
    }
    
    /*
     * Opens the new password dialog
     * when code was sucessfully sent
     * from the server.
     * 
     * @returns {unresolved}
     */
    function openPasswordDialog() {
        $("#pass-form").dialog({
            autoOpen: false,
            modal: true,
            stack: true,
            maxHeight : 800,
            width: 400,
            dialogClass: 'no-close',
            buttons: {
                Submit: function() {
                    changePass();
                    clearPassForm();
                    $(this).dialog("close");
                },
                Cancel: function() {
                    clearPassForm();
                    $(this).dialog("close");
                }
            }
        });
        $("#pass-form").dialog('option', 'title', 'Now change your password');
        $("#pass-form fieldset").text();
        return $('#pass-form');
    }
    
    /*
     * Sends data from new password form
     * to the server for validation and
     * password update attempt.
     * 
     * @returns {undefined}
     */
    function changePass() {
        $.ajax({
            type: "POST",
            url: "login?action=newPass",
            data: {
                password: function(){
                    return $("#pass-form #password1").val();
                },
                code: function(){
                    return $("#pass-form #code").val();
                }
            },       
            success: function(ret){
                if(ret === "success"){
                    createDialog("Password sucessfully updated!", "Success!").dialog("open");
                }else{ 
                    createDialog(ret, "PEBKAC").dialog("open");
                }
            }
        });
    }
    
    /*
     * Sends the username and email entered
     * in the restform to the server for
     * validation and code request.
     * 
     * @returns {undefined}
     */
    function getCode() {
        $.ajax({
            type: "POST",
            url: "login?action=reset",
            data: {
                username: function(){
                    return $("#code-form #username1").val();
                },
                email: function(){
                    return $("#code-form #email1").val();
                }
            },       
            success: function(ret){
                if(ret === "new code sent"){
                    openPasswordDialog().dialog("open");
                }else{ 
                    createDialog(ret, "PEBKAC").dialog("open");
                }
            }
        });
    }
    
    /** 
     * Gets the relevant data form the add form 
     * that must be added to the new user.
     * 
     * @returns a user
     */
    function getAddFormData() {
        var user = {};
        user.username = $("#dialog-form #username").val();
        user.email = $("#dialog-form #email").val();
        user.password = $("#dialog-form #password").val();
        return user;
    }
    
    /* Creates a message dialog
     * 
     * @param {type} message
     * @returns {undefined}
     */
    function createDialog(message, title) {
        $("#dialog-message").dialog({
            autoOpen: false,
            modal: true,
            stack: true,
            buttons: {
                Close: function() {
                    $(this).dialog("close");
                }
            }
        });
        $('#dialog-message').dialog('option', 'title', title);
        $("#dialog-message #msg").text(message);
        return $('#dialog-message');
    }
    
    /*
     * Clears the reset code form.
     *  
     * @returns {undefined}
     */
    function clearCodeForm() {
        $("#code-form #username1").val("");
        $("#code-form #email1").val("");
    }
    
    /*
     * Clears the reset password form fields.
     * 
     * @returns {undefined}
     */
    function clearPassForm() {
        $("#pass-form #password1").val("");
        $("#pass-form #rpasswd1").val("");
        $("#pass-form #code").val("");
        validatePass.resetForm();
        clearPasswordMeter();
    }
    
    /* Clears the add form fields.
     * 
     * TODO: Have yet to figure out how to reset the password
     * strength bar when pressing close in dialog window.
     *  
     * @returns {undefined}
     */
    function clearAddForm() {
        $("#dialog-form #username").val("");
        $("#dialog-form #email").val("");
        $("#dialog-form #remail").val("");
        $("#dialog-form #password").val("");
        $("#dialog-form #rpasswd").val("");
        $('input:checkbox').removeAttr('checked');
        validateResistration.resetForm(); // Reset form validation
        clearPasswordMeter();
    }
    
    /*
     * Reset pasword meter Gustaf style:
     * 
     * @returns {undefined}
     */
    function clearPasswordMeter() {
        $(".password-meter").find(".password-meter-bar")
                .removeClass().addClass("password-meter-bar")
                .addClass("password-meter-" + "");
        $(".password-meter").find(".password-meter-message")
		.removeClass().addClass("password-meter-message").text("");
    }
    
    /*
     * ReCaptcha client loading
     * 
     * @returns {Boolean}
     */ 
    function reloadRecaptcha() {
        var publicKey = "6Le_necSAAAAAP3lQUw3NDbw8CMBLF40z5xyKBVI";
        var div = "recap";
        Recaptcha.create(publicKey,div,{
            theme: "clean",
            lang: "en",
            callback: Recaptcha.focus_response_field
        });
        return false;
    }
    
    /*
     * Add a new User
     * 
     * @returns {undefined}
     */
    function addUser(){
        var user = getAddFormData();
        $.ajax({
            type: "POST",
            url: "login?action=newUser",
            data : {username: user.username, email: user.email, password: user.password},
            success: function (ret){
                if(ret === "invalid") {
                    createDialog("Something went bad", "Failure!").dialog("open");
                } else {
                    createDialog("Confirmation link has been sent to the email address you supplied", "Success!").dialog("open");
                }
            }
        });
    }
    
    /*
     * This gives the password bar life while writing
     * the new password. I.e it prints the strength dynamically.
     */
    $("#password").keyup(function() {
        $(this).valid();
    });
    
    
    /*
     * This handles validation of the inputs wneh adding a user through
     * the add user form. Everything is checked dynamically but also
     * before submission to make sure that the data entered is as valid
     * as possible.
     */
    var validateResistration = $("#register").validate({
        rules: {
            username: { 
                required: true,
                minlength: 4,
                maxlength: 20,
                remote: {
                    type: "POST",
                    url: "login?action=checkUserName",
                    data: {
                        username: function(){
                            return $("#dialog-form #username").val();
                        }
                    }
                }
            },
            password: {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            rpasswd: {
                required: true,
                minlength: 5,
                maxlength: 100,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            remail: {
                required: true, 
                email: true,
                equalTo: email
            },
            recaptcha_response_field: {
                required: true   
            }
        },
        messages: {
            username: {
                required: "Please enter a username",
                minlength: "Your username must contain more than three letters",
                maxlength: "Your username cannot contain more than 20 characters",
                remote: "That Username is already taken"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            rpasswd: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            remail: { 
                requierd: "Please enter a valid email address",
                equalTo: "The email addresses you entered are not the same"    
            },
            recaptcha_response_field: {
                required: "Please enter the text above"
            },        
            email: "Please enter a valid email address",
            agree: "Please accept our policy"
        }
    });
    
    /*
     * Valdation of reset password form.
     * Shouldn't handle the code more than
     * setting it to required.
     */
    var validatePass = $("#pass").validate({
        rules: {
            code: {
                required: true
            },
            password1: {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            rpasswd1: {
                required: true,
                minlength: 5,
                maxlength: 100,
                equalTo: "#password1"
            }
        },
        messages: {
            code: {
                required: "You must enter the code we sent to your email"
            },
            password1: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            rpasswd1: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            }
        }
    });
    
});

// This is neede to debug dynamically downloaded JS in Chrome
//@ sourceURL=products.js