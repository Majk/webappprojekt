/**
 * Author: Johan W-Schützer <Johan.DSS@Gmail.com>
 */

/**
 * Constructor for booking handler
 * @param {string} address
 * @param {function} fail
 * @returns {BookingHandler}
 */
var BookingHandler = function(address, fail) {
    this.address = address;
   
    // Define the function to run at failure.
    if (!fail) {
        
        // if the fail argument is left out, set fail to empty function.
        fail = $.noop();
    }
    
    this.fail = fail;
};

BookingHandler.prototype = (function() {
    return {
        /**
         * Gets all bookings from the database
         * @returns {JSON} bookings
         */
        getAll: function() {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address,
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Gets all bookings with matching eventName from the database
         * @param {String} eventName
         * @returns {JSON} bookings
         */
        getByEventName: function(eventName) {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address + '/byName/' + eventName,
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Gets the booking with matching uuid from the database
         * @param {string} uuid
         * @returns {JSON} booking
         */
        getBookingById: function(uuid) {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address + '/byId/' + uuid,
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Gets a list of bookings with the uuid of an attender
         * @param {string} uuid
         * @returns {JSON} bookings
         */
        getBookingByAttender: function(uuid) {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address + '/byAttenderId/' + uuid,
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Gets all bookings with matching venue from the database
         * @param {string} uuid
         * @returns {JSON} bookings
         */
        getBookingByVenue: function(uuid) {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address + '/byVenueId/' + uuid,
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Gets all bookings created by the logged in user from the database
         * @returns {JSON} bookings
         */
        getBookingByCreator: function() {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address + '/byCreator/',
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Adds an admin to the matching booking
         * @param {string} bookingUuid
         * @param {JSON} admin
         */
        addAdmin: function(bookingUuid, admin) {
            return $.ajax(
                    {
                        type: "POST",
                        url: this.address + '/addAdmin/' + bookingUuid,
                        data: admin,
                        error: this.fail
                    });
        },
        /**
         * Adds a guest to the matching booking
         * @param {string} bookingUuid
         * @param {JSON} guest
         */
        addGuest: function(bookingUuid, guest) {
            return $.ajax(
                    {
                        type: "POST",
                        url: this.address + '/addGuest/' + bookingUuid,
                        data: guest,
                        error: this.fail
                    });
        },
        /**
         * Adds the logged in user to the bookings attender list
         * @param {JSON} booking
         * @param {function} atSuccess to be run if the operation was successful
         */
        attendBooking: function(booking, atSuccess) {
            return $.ajax(
                    {
                        type: "POST",
                        url: this.address + '/attendBooking/' + booking.uuid,
                        success: function() {
                            booking = bProxy.getBookingById(booking.uuid);
                            atSuccess(booking);
                        },
                        error: this.fail
                    });
        },
        /**
         * Removes the logged in user from the bookings attender list
         * @param {JSON} booking
         * @param {function} atSuccess to be run if the operation was successful
         */
        unAttendBooking: function(booking, atSuccess) {
            return $.ajax(
                    {
                        type: "POST",
                        url: this.address + '/unAttendBooking/' + booking.uuid,
                        success: function() {
                            booking = bProxy.getBookingById(booking.uuid);
                            atSuccess(booking);
                        },
                        error: this.fail
                    });
        },
        /**
         * Demotes an admin of the booking to guest
         * @param {string} bookingUuid
         * @param {JSON} admin
         * @param {function} editBooking to be run if the operation is successful
         */
        demoteAdmin: function(bookingUuid, admin, editBooking) {
            return $.ajax(
                    {
                        type: "PUT",
                        url: this.address + '/demoteAdmin/' + bookingUuid + '/' + admin.uuid,
                        success: function() {
                            var booking = bProxy.getBookingById(bookingUuid);
                            editBooking(booking);
                        },
                        error: this.fail
                    });
        },
        /**
         * Promotes a guest of the booking to admin
         * @param {string} bookingUuid
         * @param {JSON} guest
         * @param {function} editBooking to be run if the operation is successful
         */
        promoteGuest: function(bookingUuid, guest, editBooking) {
            return $.ajax(
                    {
                        type: "PUT",
                        url: this.address + '/promoteGuest/' + bookingUuid + '/' + guest.uuid,
                        success: function() {
                            var booking = bProxy.getBookingById(bookingUuid);
                            editBooking(booking);
                        },
                        error: this.fail
                    });
        },
        /**
         * Removes an attender from the matching booking
         * @param {string} bookingUuid
         * @param {JSON} attender
         * @param {function} editBooking to be run if the operation is successful
         */
        removeAttender: function(bookingUuid, attender, editBooking) {
            return $.ajax(
                    {
                        type: "PUT",
                        url: this.address + '/removeAttender/' + bookingUuid + '/' + attender.uuid,
                        success: function() {
                            var booking = bProxy.getBookingById(bookingUuid);
                            editBooking(booking);
                        },
                        error: this.fail
                    });
        },
        /**
         * Adds a new booking to the database
         * @param {JSON} booking
         * @param {function} renderBooking to be run if the operation was successful
         */
        addBooking: function(booking, renderBooking) {
            return $.ajax(
                    {
                        type: "POST",
                        url: this.address,
                        contentType: "application/json",
                        data: JSON.stringify(booking),
                        success: function() {
                            var byCreator = bProxy.getBookingByCreator();
                            byCreator = byCreator instanceof Array ? byCreator : byCreator.booking;
                            booking = byCreator instanceof Array ? byCreator[byCreator.length - 1] : byCreator;
                            console.log(booking);
                            renderBooking(booking);
                        },
                        error: this.fail
                    });
        },
        /**
         * Updates the given booking in the database
         * @param {JSON} booking
         * @param {function} atSuccess to be run if the operation was successful
         */
        updateBooking: function(booking, atSuccess) {
            return $.ajax( 
                    {
                        type: "PUT",
                        url: this.address,
                        data: JSON.stringify(booking),
                        async: false,
                        contentType: "application/json",
                        success: function() {
                            atSuccess(booking);
                        },
                        error: this.fail
                    });
        },
        /**
         * Deletes the matching booking from the database
         * @param {string} uuid
         */
        deleteBooking: function(uuid) {
            return $.ajax(
                    {
                        type: "DELETE",
                        url: this.address + '/' + uuid,
                        success: function() {
                            previousMenuFunction();
                        },
                        error: this.fail
                    });
        }
    };
}());


