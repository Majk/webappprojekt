/**
 * A factory that creates handlers for the backend.
 */
var factory = (function(){
    
    var baseUri = "https://localhost:8181/webappprojekt/rs/";  
    var venues = new VenueHandler(baseUri + "venue");
    var users = new UserHandler(baseUri + "user");
    var bookings = new BookingHandler(baseUri + "booking");
    
    return {
        /**
         * 
         * @returns {VenueHandler} Returns a handler for the venue service.
         */
        getVenueHandler : function(){
            return venues;
        },
                /**
                 * 
                 * @returns {BookingHandler} Returns a handler for the booking service.
                 */
        getBookingHandler : function(){
            return bookings;
        },
                 /**
                  * 
                  * @returns {UserHandler} Returns a handler for the user service.
                  */
        getUserHandler : function(){
            return users;
        },
                /**
                 * 
                 * @returns {String} Returns the uri to the backend.
                 */
        getBaseUri : function(){
            return baseUri;
        }
    };    
})();

