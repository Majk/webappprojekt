var VenueHandler = function(address, fail) {
    this.address = address;
    
    // Define the function to run at failure.
    if (!fail) {
        
        // if the fail argument is left out, set fail to empty function.
        fail = $.noop();
    }
    
    this.fail = fail;
};

VenueHandler.prototype = (function() {
    return {
        /**
         * Gets a venue by its uuid
         * @param {String} uuid
         * @returns {JSON} The matching venue
         */
        getById: function(uuid) {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/byId/' + uuid,
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
    	},
        /**
         * Gets a list of all the users that arent admins or creators of a certain venue
         * @param {String} venueUuid
         * @returns {JSON} All matches
         */
        getInvokeList: function(venueUuid) {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/getInvokeList/' + venueUuid,
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
    	},
        /**
         * Gets a list of all venues a user is admin for
         * @param {String} User uuid to check admin status for
         * @returns {JSON} All matches
         */
        getByAdmin: function(uuid) {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/byAdmin/' + uuid,
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
    	},
        /**
         * Gets a list of all venues which name contains a certain string
         * @param {String} venueName name to match to
         * @returns {JSON} All matches
         */
        
        getByVenueName: function(venueName) {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/byName/' + venueName,
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
        },
        /**
         * Gets all venues with a certain range of seats
         * @param {int} from nr of seats
         * @param {int} to nr of seats
         * @returns {JSON} All matches
         */
        getByNumberOfSeats: function(from, to) {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/bySeats',
                data: 'from=' + from + '&to=' + to,
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
        },
        /**
         * Gets all venues at a certain address
         * @param {String} street 
         * @param {int} number
         * @param {String} city
         * @param {String} country
         * @returns {JSON} All matches
         */     
        getByAddress: function(street, number, city, country) {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/byAddress',
                data: 'street=' + street + '&number=' + number + '&city=' + city + '&country=' + country,
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
        },
        /**
         * Gets all venues
         * @returns {JSON} All venues
         */
        getAll: function() {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address,
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
        },
        /**
         * Gets all venues the logged in user is creator of
         * @returns {JSON} All matches
         */
        getByCreator: function() {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/byCreator/',
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
        },
        /**
         * Gets all venues the logged in user isn't creator or admin for
         * @returns {JSON} All venuues
         */
        getRegular: function() {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/regular/',
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
        },
        /**
         * Gets all admins for a certain venue
         * @param {String} uuid of venue
         * @returns {JSON} All matches
         */
        getAdmins: function(uuid) {
            var result;
            $.ajax(
                    {
                type: "GET",
                url: this.address + '/getAdmins/' + uuid,
                async: false,
                dataType: "json",
                success: function(data) {
                    result = data;
                },
                error: this.fail
            });
            return result;
        },
        /**
         * Adds a venue
         * @param {Venue} venue to add
         * @param {function} atSuccess method to call when this is successful
         * @returns {@exp;$@call;ajax} calls atSuccess when this is finished ok
         */     
        addVenue: function(venue, atSuccess, atFail) {
            var me = this;
            return $.ajax(
                    {
                type: "POST",                         
                url: this.address + '/add/',
                contentType: "application/json",
                data: JSON.stringify(venue),
                success: function() {
                    createdVenue = me.getByVenueName(venue.venueName);
                    atSuccess(createdVenue.venue);
                },
                fail: function() {
                    atFail();
                },
                error: this.fail
            });
    	},
        /**
         * Delete sa venue
         * @param {String} uuid of venue
         * @param {function} atSuccess method to call when this is successful
         * @returns {@exp;$@call;ajax} calls atSuccess when this is finished ok
         */  
        deleteVenue: function(uuid, atSuccess) {
            return $.ajax(
                    {
                type: "DELETE",
                url: this.address + '/delete/' + uuid,
                success: function() {
                    atSuccess();
                },
                error: this.fail
            });
        }  ,      
        /**
         * Updates a venue
         * @param {Venue} venue Updates for venue
         * @param {function} atSuccess method to call when this is successful
         * @returns {@exp;$@call;ajax} calls atSuccess when this is finished ok
         */
        updateVenue: function(venue, atSuccess) {
            return $.ajax(
                    {
                type: "PUT",
                url: this.address + '/update/',
                contentType: "application/json",
                data: JSON.stringify(venue),
                success: function() {
                    atSuccess(venue);
                },
                error: this.fail
            });
        },
        /**
         * Removes admin status a user has for a venue
         * @param {String} venueUuid of venue 
         * @param {String} adminUuid of user
         * @param {function} atSuccess method to call when this is successful
         * @returns {@exp;$@call;ajax} calls atSuccess when this is finished ok
         */      
        revokeAdminStatus: function(venueUuid, adminUuid, atSuccess) {
            var me = this;
            return $.ajax(
                    {
                type: "PUT",
                url: this.address + '/revokeAdmin/' + venueUuid + '/' + adminUuid,
                success: function() {
                    var venue = me.getById(venueUuid);
                    atSuccess(venue);
                },
                error: this.fail
            });
        },
        /**
         * Grants admin status to a user for a venue
         * @param {String} venueUuid of venue
         * @param {String} adminUuid of user
         * @param {function} atSuccess method to call when this is successful
         * @returns {@exp;$@call;ajax} calls atSuccess when this is finished ok
         */
        invokeAdminStatus: function(venueUuid, adminUuid, atSuccess) {
            var me = this;
            return $.ajax(
                    {
                type: "PUT",
                url: this.address + '/invokeAdmin/' + venueUuid + '/' + adminUuid,
                success: function() {
                    var venue = me.getById(venueUuid);
                    atSuccess(venue);
                },
                error: this.fail
            });
        }
    };
}());


