// renders the menu when the page is loaded.
$(function() {
        commonProducer.menuProducer();
});

// definition of producers.
var commonProducer = new CommonProducer();
var userProducer = new UserProducer();
var venueProducer = new VenueProducer();
var bookingProducer = new BookingProducer();

// lastCalledMenuFunction initialised as an empty function. 
var previousMenuFunction = $.noop();
var currentMenuFunction = $.noop();

