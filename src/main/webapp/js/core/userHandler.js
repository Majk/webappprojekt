/**
 * Constructs a userHandler that handles all communication to the UserService on the backend.
 * @param {URI} address The uri to the backend.
 * @param {function} fail The function to be called on errorus response from backend, will be declared as an empty function if null.
 * @returns {UserHandler} The constructed Userhandler. 
 */
var UserHandler = function(address, fail) {
    // These ar not private just unique for each object
    this.address = address;
    
    // Define the function to run at failure.
    if (!fail) {
        
        // if the fail argument is left out, set fail to empty function.
        fail = $.noop();
    }
    
    this.fail = fail;
};

UserHandler.prototype = (function() {
    return {
        /**
         * Requests the logged in user as a Json from the backend.
         * @returns {Json} The logged in user, if logged in. 
         */
        getUser: function() {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address,
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Requests a list of all registered users from the backend.
         * @returns {Json} A list of all registered users as a Json.
         */
        getAll: function() {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address + '/getAll/',
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Requests a user with a specific uuid from the backend.
         * @param {String} uuid The uuid to search for.
         * @returns {Json} The user as a Json if found.
         */
        getUserById: function(uuid) {
            var result;
            $.ajax(
                    {
                        type: "GET",
                        url: this.address + '/' + uuid,
                        async: false,
                        dataType: "json",
                        success: function(data) {
                            result = data;
                        },
                        error: this.fail
                    });
            return result;
        },
        /**
         * Updates a user in the database.
         * @param {Json} user A User represented as a Json with the new values. 
         * @param {function} atSuccess The function to be called if the update is a success.
         * @returns {Json} The response from the server as a Json.
         */
        updateUser: function(user, atSuccess) {
            return $.ajax(
                    {
                        type: "PUT",
                        contentType: "application/json",
                        url: this.address,
                        data: JSON.stringify(user),
                        success: function() {
                            user = uProxy.getUserById(user.uuid);
                            atSuccess(user);
                        },
                        error: this.fail
                    });
        },
        /**
         * Requests to delete a user from the backend.
         * @param {String} uuid The uuid of the user to delete.
         * @param {function} atSuccess The function to be called on success.
         * @returns {Json} The Response as a Json
         */
        deleteUser: function(uuid, atSuccess) {
            return $.ajax(
                    {
                        type: "DELETE",
                        url: this.address + '/' + uuid,
                        success: atSuccess(),
                        error: this.fail
                    });
        },
    };
}());


