/***********************************************
 *  
 * A page producer for all User-related pages 
 * 
 *  
 */
var UserProducer = function() {
};

UserProducer.prototype = (function() {
    return {
        /**
         * Renders a userpage in the given context.
         * @param {Json} user The user to render as a Json.
         * @param {jQuery} context The context to render the user page in.
         */
        renderUser: function(user, context) {
            console.log("entering renderUser()");
            var me = userProducer;
            context.append('<h1>' + user.userName + '</h1>');

            context.append('<img src="' + user.picture + '" alt="" class="largeUserPicture"/>');

            if (user.firstName) {
                context.append('<p>FirstName: ' + user.firstName + '</p>');
            }

            if (user.lastName) {
                context.append('<p>LastName: ' + user.lastName + '</p>');
            }

            context.append('<p>Email: ' + user.email + '</p>');
            if (user.birthDate) {
                context.append('<p>Born: ' + user.birthDate + '</p>');
            }
            if (user.registeredOn) {
                context.append('<p>Registered: ' + user.registeredOn + '</p>')
            }

            var meUser = uProxy.getUser();

            if (meUser.uuid === user.uuid) {
                context.append('<input type="button" value="Edit" class="link" id="editUser" />');
                context.append('<input type="button" value="Delete" class="link" id="deleteUser" />');

                context.find('#editUser').last().click(function() {
                    me.editUser(meUser);
                });

                context.find('#deleteUser').last().click(function() {
                    uProxy.deleteUser(meUser.uuid, function() {
//                        Run this script at success
                        window.location.replace("https://localhost:8181/webappprojekt/");
                    });
                });
            }

            console.log("exiting renderUser()");
        },
        /**
         * Renders the Edit user page
         * @param {Json} user The user for whom to render the edit page.
         */
        editUser: function(user) {
            console.log("entering editUser()");
            var me = userProducer;
            // define this variabel to be able to access it in the inner function below.
            var cP = commonProducer;

            // Render the form and bind listeners
            var fieldset = me.renderUserForm(user, 'Edit');
            fieldset.find("#submit").click(function() {
                user = me.loadUserFromForm(fieldset, user);
                uProxy.updateUser(user, function(user) {
                    me.renderUser(user);
                    cP.updateMenuUser();
                });
            });
            fieldset.find("#cancel").click(function() {
                // reload last page visited before entering edit
                currentMenuFunction();
            });

            console.log("exiting editUser()");
        },
        /**
         * Takes a user and updates its values with values from a form.
         * @param {jQuery} form A jQuery holding the form. 
         * @param {Json} user A user Json to update the values in.
         * @returns {Json} The user Json updated with values from the form.
         */
        loadUserFromForm: function(form, user) {
            console.log("entering loadUserFromForm()");

            user.firstName = form.find('#firstName').val();
            user.lastName = form.find('#lastName').val();
            user.picture = form.find('#picture').val();

            console.log('exiting loadUserFromForm()');
            return user;
        },
        /**
         * Lists users in under a given header.
         * @param {Array} users the Array of users to render.
         * @param {String} header the header to render.
         */
        listUsers: function(users, header) {
            console.log('entering listUsers()');
            var me = userProducer;
            var context = $('#context');
            context.append('<table id="contextTable"/>');

            var table = context.find('table').last();
            table.append('<th>' + header + '</th>');
            if (users) {
                users = (users.user instanceof Array ? users.user : users);
                $.each(users, function(i, user) {
                    if (user) {
                        table.append('<tr/>')
                        var row = table.find('tr').last();

                        row.append('<td id="' + i + '" class="link"><a/></td>');

                        var a = row.find('a').last();

                        me.renderUserWithImage(a, user);

                        row.find('#' + i).click(function() {
                            context.html(me.renderUser(user));
                        });
                    }
                });
            }
            console.log('exiting listUsers()');
        },
        /**
         * Renders a user with username and profilepicture.
         * @param {jQuery} context The jQuery context to render in.
         * @param {Json} user The user to render.
         */
        renderUserWithImage: function(context, user) {
            context.append('<img src="' + user.picture + '" alt="user_picture" class="smallUserPicture"/> ');
            context.append(user.userName);

        },
        /**
         * Renders a form for a Editing/adding a user.
         * @param {Json} user The user Json to load data from.
         * @param {String} btnText The text on the submit button.
         * @returns {jQuery} The jQuery context holding the form.
         */
        renderUserForm: function(user, btnText) {
            console.log("entering renderUserForm()");
            var context = $('#context');

//            Empty Context
            context.html("");
            context.append('<form><fieldset/></form>');

            var fieldset = $('fieldset').last();

            if (user.uuid) {
                fieldset.append('<input type="hidden" name="uuid" id="uuid" value="' + user.uuid + '"/>');
            }
            fieldset.append('<label for="firstName">Firstname: </label>');
            fieldset.append('<input type="text" name="FirstName" id="firstName" value="' + user.firstName + '" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<br/>');

            fieldset.append('<label for="lastName">Lastname: </label>');
            fieldset.append('<input type="text" name="lastName" id="lastName" value="' + user.lastName + '" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<br/>');

            fieldset.append('<label for="picture">Picture url: </label>');
            fieldset.append('<input type="url" name="picture" id="picture" value="' + user.picture + '" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<div id="pictureHolder"/>')
            var pictureHolder = fieldset.find('#pictureHolder');

            // when changing the picture input preview the image
            fieldset.find('#picture').change(function() {
                pictureHolder.html('');
                pictureHolder.append('<img src="' + fieldset.find('#picture').val() + '" alt="" class="largeUserPicture"/>');
            });

            pictureHolder.append('<img src="' + user.picture + '" alt="" class="largeUserPicture"/>');

            fieldset.append('<br/>');
            fieldset.append('<input type="button" value="Cancel" id="cancel" />');
            fieldset.append('<input type="button" value="' + btnText + '" id="submit" />');

            console.log("exiting renderUserForm()");
            return fieldset;
        },
        /**
         * Renders the logged in users userpage.
         */
        myUser: function() {
            console.log("entering myUser()");
            commonProducer.clearContext();
            var me = userProducer;

            commonProducer.setCurrentMenuFunction(me.myUser);
            var user = uProxy.getUser();

            var context = commonProducer.clearContext();
            me.renderUser(user, context);
            console.log("exiting myUser()");
        },
        /**
         * Renders a list of all registerd users.
         */
        allUsers: function() {
            console.log("entering allUsers()");
            commonProducer.clearContext();
            var me = userProducer;

            commonProducer.setCurrentMenuFunction(me.allUsers);
            var users = uProxy.getAll();


            var context = commonProducer.clearContext();
            me.renderUser(users, context);
            console.log("exiting allUsers()");
        }
    };
}());
