/***********************************************
 *  
 * A page producer for all venue-related pages 
 * 
 *  
 */
var VenueProducer = function() {
};


VenueProducer.prototype = (function() {
    return {
        /**
         * Lists all the venues in a list with certain differences depending on the users status for that venue
         * @param {Venue} venues all venues to list
         * @param {String} status status of logged in user
         */
        listVenues: function(venues, status, header) {
            console.log("entering listVenues()");
            var me = venueProducer;
            var context = $('#context');
            
            var list = venues;
            if (venues.venue instanceof Array) {
                list = venues.venue;
            }
            $.each(list, function(i, venue) {
                if (status === "creator") {
                    var table = context.find("#creatorTable").last();
                    table.append('<tr/>');
                    var row = table.find('tr').last();
                    row.append('<td><name id="name' + i + '" class="link"><a>' + venue.venueName + '</a></name></td>');
                    row.append('<td><edit id="edit' + i + '" class="link"><a>Edit</a></edit></td>');
                    row.append('<td><delete id="delete' + i + '" class="link"><a>Delete</a></delete></td>');
                    context.find('#name' + i).last().click(function() {
                        console.log("View venue");
                        me.renderVenue(venue);
                    });
                    
                    context.find("#edit" + i).last().click(function() {
                        console.log("Edit");
                        me.editVenue(venue);
                        console.log("Pressed Edit Venue");
                    });

                    context.find("#delete" + i).last().click(function() {
                        console.log("Delete");
                        vProxy.deleteVenue(venue.uuid, me.myVenues);
                        console.log("Pressed Delete Venue");

                    });
                    
                    
                } else if (status === "admin") {
                    var table = context.find("#adminTable").last();
                    table.append('<tr/>');
                    var row = table.find('tr').last();
                    row.append('<td><name id="name' + i + '" class="link"><a>' + venue.venueName + '</a></name></td>');
                    row.append('<td><edit id="edit' + i + '" class="link"><a>Edit</a></edit></td>');
                    context.find("#edit" + i).last().click(function() {
                        console.log("Edit");
                        me.editVenue(venue);
                        console.log("Pressed Edit Venue");
                    });
                } else if (status === "regular") {
                    var table = context.find("#regularTable").last();
                    table.append('<tr/>');
                    var row = table.find('tr').last();
                    row.append('<td><name id="name' + i + '" class="link"><a>' + venue.venueName + '</a></name></td>');
                }
                context.find("#name" + i).last().click(function() {
                    me.renderVenue(venue);
                });
            });
            console.log("exiting listVenues()");
        },
        /**
         * Renders a venue page
         * @param {Venue} venue to render for
         */
        renderVenue: function(venue) {
            console.log("entering renderVenue()");
            var me = venueProducer;
            var context = $('#context');
            
            //            Empty Context
            commonProducer.clearContext();
            
            context.append('<h1>' + venue.venueName + '</h1>');
            
            context.append('<p>Creator:</p>');
            userProducer.renderUserWithImage(context, venue.creator);
            
            me.renderAddress(venue.address);
            
            userProducer.listUsers(venue.admins, 'Admins');
            
            context.append('<p>Seats: ' + venue.seats + '</p>');
            var meUser = uProxy.getUser();
            if (venue.creator.uuid === meUser.uuid) {
                context.append('<input type="button" value="Edit" id="edit" />');
                context.append('<input type="button" value="Delete" id="delete" />');

                context.find('#edit').last().click(function() {
                    me.editVenue(venue);
                });

                context.find('#delete').last().click(function() {
                    vProxy.deleteVenue(venue.uuid, currentMenuFunction);
                });
            }
            console.log("exiting renderVenue()");
        },
        /**
         * Renders an address
         * @param {Address} address to render
         */
        renderAddress: function(address) {
            console.log("entering renderAddress()");
            var context = $('#context');
            context.append('<p>Address: ' + address.country + ', ' + address.city + ', ' + address.street + ', ' + address.nbr + '</p>');
            console.log("exiting renderAddress()");

        },
        /**
         * Builds the add form for a venue
         */
        addVenue: function() {
            console.log("entering addVenue()");
            commonProducer.setCurrentMenuFunction(venueProducer.addVenue);
            var common = commonProducer
            var me = venueProducer;
            var venue = {
                venueName: '',
                seats: '',
                address: {
                    street: '',
                    nbr: '',
                    city: '',
                    country: ''
                }
            };

            var fieldset = me.renderVenueForm(venue, 'Add');
            fieldset.find("#submit").click(function() {
                venue = me.loadVenueFromForm(fieldset, venue);
                if (me.checkValidVenue(venue)) {
                    vProxy.addVenue(venue, me.renderVenue, commonProducer.nameAlreadyExist());
                }
            });
            console.log("exiting addVenue()");
        },
        /**
         * Builds the edit form for a venue
         * @param {Venue} venue to edit
         */
        editVenue: function(venue) {
            console.log("entering editVenue()");
            var me = venueProducer;
            var fieldset = me.renderVenueForm(venue, 'Edit');
            fieldset.find("#submit").click(function() {
                venue = me.loadVenueFromForm(fieldset, venue);
                if (me.checkValidVenue(venue)) {
                    vProxy.updateVenue(venue, me.renderVenue);
                }
            });
            fieldset.find("#cancel").click(function() {
                previousMenuFunction();
            });

            console.log("exiting editVenue()");
        },
        /**
         * Checks if the param is a valid venue
         * @param {Venue} venue to check
         */
        checkValidVenue: function(venue) {
            console.log("entering checkValidVenue()");
            var valid = true;
            valid = commonProducer.isValidNumber(venue.seats, "Seats");
            valid &= commonProducer.isValidNumber(venue.address.nbr, "House number");
            console.log("exiting checkValidVenue()");
            return valid;
        },
        /**
         * Loads a venue from a form
         * @param {form} form Form to load from
         * @param {Venue} venue to save form info to
         * @return {Venue} venue created from form
         */
        loadVenueFromForm: function(form, venue) {
            console.log("entering loadVenueFromForm()");
            venue.seats = form.find('#seats').val();
            venue.venueName = form.find('#venueName').val();

            venue.address.street = form.find('#street').val();
            venue.address.nbr = form.find('#nbr').val();
            venue.address.city = form.find('#city').val();
            venue.address.country = form.find('#country').val();
            console.log("exiting loadVenueFromForm()");
            return venue;
        },
        /**
         * Builds a form for edit and adding a venue
         * @param {Venue} venue
         * @param {String} formType What type of form this is, and text for submit button
         */
        renderVenueForm: function(venue, formType) {
            console.log("entering renderVenueForm()");
            var context = $('#context');
            var me = venueProducer;
            //            Empty Context
            commonProducer.clearContext();
            context.append('<form><fieldset/></form>');

            var fieldset = $('fieldset').last();

            if (venue.uuid) {
                fieldset.append('<input type="hidden" name="uuid" id="uuid" value="' + venue.uuid + '"/>');
            }
            fieldset.append('<label for="venueName">Venue Name: </label>');
            fieldset.append('<input type="text" name="venueName" id="venueName" value="' + venue.venueName + '" placeholder="Name of Venue" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<div/>');
            fieldset.append('<label for="seats"># of seats: </label>');
            fieldset.append('<input type="text" name="seats" id="seats" value="' + venue.seats + '" placeholder="Number of Seats" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<div/>');
            fieldset.append('<label for="street">Street Address: </label>');
            fieldset.append('<input type="text" name="street" id="street" value="' + venue.address.street + '" placeholder="Street" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<div/>');
            fieldset.append('<label for="nbr">Street Number: </label>');
            fieldset.append('<input type="text" name="nbr" id="nbr" value="' + venue.address.nbr + '" placeholder="House Number" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<div/>');
            fieldset.append('<label for="city">City: </label>');
            fieldset.append('<input type="text" name="city" id="city" value="' + venue.address.city + '" placeholder="City" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<div/>');
            fieldset.append('<label for="country">Country: </label>');
            fieldset.append('<input type="text" name="country" id="country" value="' + venue.address.country + '" placeholder="Country" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<div/>');
            if(formType === 'Edit') {
                fieldset.append('<input type="button" value="Cancel" id="cancel" />');
            }
            fieldset.append('<input type="button" value="' + formType + '" id="submit" />');
            if(formType === 'Edit') {
                me.buildTables(venue);
            }
            console.log("exiting renderVenueForm()");
            return fieldset;
        },
        /**
         * Creates a page which lists all the venues a user is creator or admin for
         */
        myVenues: function(input) {
            console.log("entering myVenues()");
            commonProducer.setCurrentMenuFunction(venueProducer.myVenues);
            var header = "My Venues";
            if(input) {
                header =  "All Venues";
            }
            venueProducer.createAdminTables(header);
            var venues = vProxy.getByCreator();
            if(venues !== null) {
                venueProducer.listVenues(venues, "creator");
            }
            var user = uProxy.getUser();
            venues = vProxy.getByAdmin(user.uuid);
            if(venues !== null) {
                venueProducer.listVenues(venues, "admin");
            }
            console.log("exiting myVenues()");
        },
        /**
         * Creates a page which lists all venues
         */
        allVenues: function() {
            console.log("entering allVenues()");
            commonProducer.setCurrentMenuFunction(venueProducer.allVenues);
            venueProducer.myVenues("All Venues");
            venueProducer.createRegularTable();
            var venues = vProxy.getRegular();
            if (venues) {
                venueProducer.listVenues(venues, "regular");
            }                
            console.log("exiting allVenues()");
        },
        /**
         * Creates the search page
         */
        searchForm: function() {
            console.log("entering searchForm()");
            commonProducer.setCurrentMenuFunction(venueProducer.searchForm);
            var me = venueProducer;
            var context = $('#context');
            commonProducer.clearContext();
            context.append('<form><fieldset/></form>');
            var fieldset = $('fieldset').last();
            me.createSearchByName();
            me.createSearchBySeats();
            me.createSearchByAddress();
            fieldset.append('<errorBox/>')
            console.log("exiting searchForm()");
        },
                
        createSearchByName: function() {
            var me = venueProducer;
            var common = commonProducer;
            var context = $('#context');
            var fieldset = $('fieldset').last();
            fieldset.append('<label for="byName">Search by name: </label>');
            fieldset.append(common.textInput('byName', 'Name to Search for'));
            fieldset.append('<input type="button" value="Search By Name" id="searchByName" />');
            fieldset.find("#searchByName").click(function() {
                var venueToSearchFor = fieldset.find('#byName').val();
                var venues = vProxy.getByVenueName(venueToSearchFor);
                if(venues) {
                    commonProducer.clearContext();
                    me.createRegularTable();
                    me.listVenues(venues, "regular");
                } else {
                    common.renderMessageInErrorBox("No matches found please refine your search");
                }
      
            });
            fieldset.append('<div/>');
        },
        createSearchBySeats: function() {
            var me = venueProducer;
            var common = commonProducer;
            var context = $('#context');
            var fieldset = $('fieldset').last();
            fieldset.append('<label for="bySeats">Search by # of seats: </label>');
            fieldset.append(common.textInput('from', 'From # of Seats'));
            fieldset.append(common.textInput('to', 'To # of Seats'));
            fieldset.append('<input type="button" value="Search By Seats" id="searchBySeats" />');
            fieldset.find("#searchBySeats").click(function() {
                var from = fieldset.find('#from').val();
                var to = fieldset.find('#to').val();
                if(common.isValidNumber(from, "From number of seats") && common.isValidNumber(to, "To number of seats")){
                    var venues = vProxy.getByNumberOfSeats(from, to);
                    if(venues) {
                        commonProducer.clearContext();
                        me.createRegularTable();
                        me.listVenues(venues, "regular");
                    } else {
                        common.renderMessageInErrorBox("No matches found please refine your search");
                    }
                }
            });
            fieldset.append('<div/>');
        },
        createSearchByAddress: function() {
            var me = venueProducer;
            var common = commonProducer;
            var fieldset = $('fieldset').last();
            fieldset.append('<label for="byAddress">Search By Address: </label>');
            fieldset.append(common.textInput('street', 'Street'));
            fieldset.append(common.textInput('city', 'City'));
            fieldset.append(common.textInput('country', 'Country'));
            fieldset.append('<input type="button" value="Search By Address" id="searchByAddress" />');
            fieldset.find("#searchByAddress").click(function() {
                
                var street = fieldset.find('#street').val();
                var city = fieldset.find('#city').val();
                var country = fieldset.find('#country').val();
                console.log(country);
                if(!street && !city && !country) {
                    common.renderMessageInErrorBox("No matches found please refine your search");
                } else {
                    var venues = vProxy.getByAddress(street, 0, city, country);
                    if(venues) {
                        commonProducer.clearContext();
                        me.createRegularTable();
                        me.listVenues(venues, "regular");
                    } else {
                        common.renderMessageInErrorBox("No matches found please refine your search");
                    }
                }
                
                
            });
        },
                                      
        /**
         * Creates the base of admin and creator tables
         */      
        createAdminTables: function(header) {
            console.log("entering createAdminTables()");
            var context = $('#context');
            commonProducer.clearContext();
            if (header) {
                context.append('<h1>'+header+'</h1>');
            }
            context.append('<table id="creatorTable" class="contextTable"/>');
            var table = context.find("#creatorTable").last();
            table.append('<th>Creator of</th><th/><th/>');
            context.append('<table id="adminTable" class="contextTable"/>');
            var table = context.find("#adminTable").last();
            table.append('<th>Admin of</th><th/>');
            console.log("exiting createAdminTables()");
        },
        /**
         * Creates the base of a regular table
         */
        createRegularTable: function(header) {
            console.log("entering createRegularTables()");
            var context = $('#context');
            if (header) {
                context.append('<h1>'+header+'</h1>');
            }
            context.append('<table id="regularTable" class="contextTable"/>');
            var table = context.find("#regularTable").last();
            table.append('<th>Venues</th>');
            console.log("exiting createRegularTables()");
        },
        /**
         * Builds the invoke admin status table for a venue
         * @param {Venue} venue
         */      
        invokeTable: function(venue) {
            console.log("entering invokeForm()");
            var me = venueProducer;
            var context = $('#context');
            context.append('<table id="userTable" class="contextTable"/>');
            var table = context.find("#userTable").last();
            table.append('<th>Invoke Admin Rights</th>');
            var users = vProxy.getInvokeList(venue.uuid);
            if(users) {
                console.log(users);
                var list = users;
                if (users.user instanceof Array) {
                    list = users.user;
                }

                if(list) {
                    $.each(list, function(i, user) {
                        table.append('<tr/>');
                        var row = table.find('tr').last();
                        row.append('<td><name id="name' + i + '" class="link"><a>' + user.userName + '</a></name></td>');
                        console.log(user.userName);
                        context.find("#name" + i).click(function() {
                            vProxy.invokeAdminStatus(venue.uuid, user.uuid, me.editVenue);
                        });
                    });
                }
            } else {
                table.append('<tr/>');
                var row = table.find('tr').last(); 
                row.append('<td>No users to grant admin status</td>')
            }
            console.log("exiting invokeForm()");
        },
        /**
         * Build demote admin status table
         * @param {Venue} venue
         */
        demoteTable: function(venue) {
            console.log("entering demoteForm()");
            var me = venueProducer;
            var context = $('#context');
            context.append('<table id="adminTable" class="contextTable"/>');
            var table = context.find("#adminTable").last();
            table.append('<th>Demote Admin Rights</th>');
            var users = vProxy.getAdmins(venue.uuid);
            if(users) {
                console.log(users);
                var list = users;
                if (users.user instanceof Array) {
                    list = users.user;
                }

                if(list) {
                    $.each(list, function(i, user) {
                        table.append('<tr/>');
                        var row = table.find('tr').last();
                        row.append('<td><name id="name' + i + '" class="link"><a>' + user.userName + '</a></name></td>');
                        console.log(user.userName);
                        context.find("#name" + i).click(function() {
                            vProxy.revokeAdminStatus(venue.uuid, user.uuid, me.editVenue);
                        });
                    });
                }
            } else {
                table.append('<tr/>');
                var row = table.find('tr').last();
                row.append('<td>No users to demote admin status</td>');
            }
            console.log("exiting demoteForm()");
        },
        /**
         * Builds invoke and revoke admin status for a venues
         * @param {Venue} venue to build tables for
         */
        buildTables: function (venue) {
            var me = venueProducer;
            var user = uProxy.getUser();
            var creatorRights = user.uuid === venue.creator.uuid;
            var adminRights = false;
            if(venue.admins) {
                
                var list = venue.admins;
                if(venue.admins.user instanceof Array) {
                    list = venue.admins.user;
                }
                var adminRights = commonProducer.contains(list, user);
            }
            console.log(adminRights);
            if(adminRights || creatorRights) {
                console.log("admins");
                me.invokeTable(venue);
                   
            }
            if(creatorRights) {
                console.log("creator");
                me.demoteTable(venue);
            }
             
        },
    };
}());
