/* 
 * Handles the pop ups from the footer.
 */

$(function() {
    
    $("#aboutlink").on("click", function() {
        openDialog("#about", 'About us', 450).dialog("open");
    });
    $("#termslink").on("click", function() {
        openDialog("#terms", 'Terms Of Use', 450).dialog("open");  
    });
    $("#privacylink").on("click", function() {
        openDialog("#policy", 'Privacy Policy', 400).dialog("open");  
    });
    
    function openDialog(id, title, width) {
        $(id).dialog({
            autoOpen: false,
            modal: true,
            stack: true,
            maxHeight : 800,
            maxWidth : 600,
            width: width,
            buttons: {
                Close: function() {
                    $(id).dialog("close");
                }
            }
        });
        $(id).dialog('option', 'title', title);
        $(id).text();
        return $(id);
    }
});

// This is neede to debug dynamically downloaded JS in Chrome
//@ sourceURL=products.js