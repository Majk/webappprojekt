/***********************************************
 *  
 * A page producer for all common pages
 * 
 *  
 */
var CommonProducer = function() {
    // These ar not private just unique for eacusername: this.userName,h object
};

// defines proxy variables for the handlers.
var bProxy = factory.getBookingHandler();
var vProxy = factory.getVenueHandler();
var uProxy = factory.getUserHandler();

CommonProducer.prototype = (function() {
    return {
        /**
         * Checks if a number is valid, renders error message in #errorbox if not.
         * @param {number} number The number to check.
         * @param {String} name The name of the number to be printed if check fails.
         * @returns {Boolean} The result of the check.
         */
        isValidNumber: function(number, name) {
            var errorBox = $('#errorBox');
            var valid = true;
            if (!$.isNumeric(number)) {
                errorBox.append(name + " must be an integer!<br/>");
                valid = false;
            }
            else if (number < 0) {
                errorBox.append(name + " must be greater than 0!<br/>");
                valid = false;
            }

            return valid;

        },
        /**
         * Renders a error message in errorBox.
         * @param {String} text The text to render.
         */
        renderMessageInErrorBox: function(text) {
            var errorBox = $('#errorBox');
            errorBox.append(text);
        },
        noMatches: function() {
            commonProducer.renderMessageInErrorBox("No matches found, please refine your search");
        },
        nameAlreadyExist: function() {
            commonProducer.renderMessageInErrorBox("Name already exists, please choose another");
        },
        /**
         * Updates the usersection in the menu.
         */
        updateMenuUser: function() {
            var user = uProxy.getUser();
            var menuUser = $('#menuUser');

            menuUser.html('');

            menuUser.append('<img src="' + user.picture + '" alt="" class="smallUserPicture"/>');

            menuUser.find('img').last().click(function() {
                userProducer.myUser();
            });
        },
        /**
         * Renders the menu.
         */
        menuProducer: function() {
            var me = this;
            console.log("Entered menuProducer");
            var menu = $('#menu');

            // Emtpy menu
            menu.html("");

            menu.append('<span id="menuUser" />');
            commonProducer.updateMenuUser();

            menu.append('<ul/>');
            var ulMain = $('ul').last();

            // BACK BUTTON
            ulMain.append('<li class="link"><a><span>Back</span></a></li>');
            ulMain.find('li').last().click(function() {
                if (previousMenuFunction) {
                    previousMenuFunction();
                }
            });

            // USERS BUTTON
            ulMain.append('<li class="link"><a><span>Users</span></a><ul/></li>');
            var ul = $('ul').last();
            ul.append('<li class="link"><a><span>My User</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed My User");
                userProducer.myUser();
            });
            ul.append('<li class="link"><a><span>All Users</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed All Users");
                userProducer.allUsers();
            });

            // VENUE BUTTON
            ulMain.append('<li class="link"><a><span>Venues</span></a><ul/></li>');
            var ul = $('ul').last();
            ul.append('<li class="link"><a><span>My Venues</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed My Venues");
                venueProducer.myVenues();
            });
            ul.append('<li class="link"><a><span>All Venues</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed All Venues");
                venueProducer.allVenues();
            });
            ul.append('<li><a><span>Search</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed Search venue");
                venueProducer.searchForm();
            });
            ul.append('<li class="link"><a><span>New Venue</span></a></li>');
            ul.find('li').last().click(function() {
                venueProducer.addVenue();
            });

            // BOOKING BUTTON
            ulMain.append('<li class="link"><a><span>Bookings</span></a><ul/></li>');
            var ul = $('ul').last();
            ul.append('<li class="link"><a><span>My Bookings</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed My Bookings");
                bookingProducer.myBookings();
            });
            ul.append('<li class="link"><a><span>All Bookings</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed All Bookings");
                bookingProducer.allBookings();
            });
            ul.append('<li><a><span>Search</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed Search booking");
                bookingProducer.searchForm();
            });
            ul.append('<li class="link"><a><span>New Booking</span></a></li>');
            ul.find('li').last().click(function() {
                console.log("Pressed New Booking");
                bookingProducer.addBooking();
            });

            // ABOUT BUTTON
            ulMain.append('<li class="link"><a><span>About</span></a></li>');
            ulMain.find('li').last().click(function() {
                commonProducer.renderAbout();
            });

            // LOG OUT BUTTON
            ulMain.append('<li class="link"><a><span>Log Out</span></a></li>');
            ulMain.find('li').last().click(function() {
                console.log("Pressed log out");
                window.location = 'https://localhost:8181/webappprojekt/login?action=logout';
            });
        },
        /**
         * Sets the current menu function used by the back functionallity
         * @param {function} newFunction The new function.
         */
        setCurrentMenuFunction: function(newFunction) {
            previousMenuFunction = currentMenuFunction;
            currentMenuFunction = newFunction;
        },
        bumpCurrentMenuFunction: function() {
            previousMenuFunction = currentMenuFunction;
        },
        /**
         * Renders the about page.
         */
        renderAbout: function() {
            commonProducer.setCurrentMenuFunction(commonProducer.renderAbout);

            var context = $('#context');
            commonProducer.clearContext();

            context.append('<p>About</p>');
            context.append('<p>Programming: Viktor Sjölind</p>');
            context.append('<p>Flavortext: Johan Schützer</p>');
            context.append('<p>Graphics: Mikael Stolpe</p>');
            context.append('<p>Sound effects: Gustav Ringius</p>');
        },
        /**
         * Return true if the given entity is represented in array.
         * Does not check for equality of objects, just equality of
         * the uuid's. The objects in the array and the given
         * entity must have the field uuid.
         * @param {type} array Array of objects that have the field uuid
         * @param {type} entity Object that has the field uuid
         * @returns {Boolean} true if the given entity is represented in the given array
         */
        contains: function(array, entity) {
            if (array instanceof Array && entity) {
                var i = array.length;
                while (i--) {
                    if (entity.uuid && array[i].uuid) {
                        if (entity.uuid === array[i].uuid) {
                            return true;
                        }
                    }
                }
            }
            return false;
        },
        /**
         * Clears the context and the errorbox
         */
        clearContext: function() {
            console.log("Entering clearContext()");
            var context = $('#context');
            context.html('');
            $('#errorBox').html('');
            console.log("Exiting clearContext()");
            return context;
        },
        /**
         * 
         * @param {String} text name of var
         * @param {String} placeHolder text in placeholder
         * @returns {String} finished text input field
         */
        textInput: function(text, placeHolder) {
            console.log(text);
            console.log(placeHolder);
            return '<input type="text" name="' + text + '" id="' + text + '" value="" placeholder="' + placeHolder + '" class="text ui-widget-content ui-corner-all" />'
        }
    };
}());
