/***********************************************
 *  
 * A page producer for all booking-related pages 
 * 
 * Author: Johan W-Schützer <Johan.DSS@Gmail.com>
 */

/**
 * Constructor function
 * @returns {undefined}
 */
var BookingProducer = function() {
};

BookingProducer.prototype = (function() {
    return {
        /**
         * Generates html rendering the given booking
         * @param {JSON} booking
         */
        renderBooking: function(booking) {
            console.log("entering renderBooking()");
            var context = $('#context');
            var me = bookingProducer;
            //            Empty Context
            context.html("");
            console.log(booking);
            //            Fill context with new stuff
            context.append('<p>Name: ' + booking.eventName + '</p>');
            context.append('<p>Description: <br/>' + booking.description + '</p>');
            var creator = booking.creator;
            context.append('<p>Creator: </p>');
            context.append('<p id="creator" class="link"><a/></p>');

            userProducer.renderUserWithImage(context.find('a').last(), creator);

            context.find('#creator').click(function() {
                userProducer.renderUser(creator);
            });

            var nAttenders = 0;
            var admins = booking.admins;
            var guests = booking.guests;

            if (admins && admins.user) {
                admins = (admins.user instanceof Array ? admins.user : [admins.user]);
                nAttenders = nAttenders + admins.length;
            } else {
                admins = [];
            }
            if (guests && guests.user) {
                guests = (guests.user instanceof Array ? guests.user : [guests.user]);
                nAttenders = nAttenders + guests.length;
            } else {
                guests = [];
            }
            context.append('<p>Attenders: ' + nAttenders + '</p>');
            var attenders = $.merge(admins, guests);

            userProducer.listUsers(attenders, "Attenders");

            context.append('<p>Venue: ' + booking.venue.venueName + '</p>');
            context.append('Time: ');
            me.addDateStamp(context, booking.dateStamp);

            context.append('<br/><br/>');
            var user = uProxy.getUser();

            if (!commonProducer.contains(attenders, user)) {
                context.append('<input type="button" value="Attend" id="attend" />');

                context.find('#attend').last().click(function() {
                    bProxy.attendBooking(booking, me.renderBooking);
                });
            } else {
                context.append('<input type="button" value="Unattend" id="unattend" />');

                context.find('#unattend').last().click(function() {
                    bProxy.unAttendBooking(booking, me.renderBooking);
                });
            }
            if (booking.creator.uuid === user.uuid || commonProducer.contains(admins, user)) {
                context.append('<input type="button" value="Edit booking" id="edit" />');
                if (booking.creator.uuid === user.uuid) {
                    context.append('<input type="button" value="Delete booking" id="delete" />');
                }
                
                context.find('#edit').last().click(function() {
                    me.editBooking(booking);
                });

                context.find('#delete').last().click(function() {
                    bProxy.deleteBooking(booking.uuid);
                });
            }

        },
        /**
         * Lists the given bookings in the given context with the given header
         * @param {JSON} bookings
         * @param {JQuery} context
         * @param {string} header
         */
        listBookings: function(bookings, context, header) {
            console.log("Entering listBooking");
            var me = bookingProducer;
            if (header) {
                context.append('<h1>' + header + '</h1>');
            }
            context.append('<table id="contextTable"/>');
            var table = context.find('table').last();
            table.append('<th>Event Name</th><th>Time</th><th>Address</th>');

            if (bookings) {
                bookings = (bookings.booking instanceof Array ? bookings.booking : bookings);
                $.each(bookings, function(i, booking) {
                    if (booking) {
                        table.append('<tr/>');
                        var row = table.find('tr').last();
                        row.append('<td><name id="name' + i + '" class="link"><a>' + booking.eventName + '</a></name></td>');
                        row.append('<td/>');
                        var td = row.find('td').last();
                        me.addDateStamp(td, booking.dateStamp);
                        row.append('<td><venue id="venue' + i + '" class="link"><a>' + booking.venue.venueName + '</a></venue></td>');

                        context.find("#name" + i).click(function() {
                            me.renderBooking(booking);
                        });

                        context.find("#venue" + i).click(function() {
                            venueProducer.renderVenue(booking.venue);
                        });
                    }
                });
            }
            commonProducer.bumpCurrentMenuFunction();
            console.log("Exiting listBookings");
        },
        /**
         * Pre function to render the form used to create bookings
         */
        addBooking: function() {
            console.log("entering addBooking()");
            commonProducer.setCurrentMenuFunction(bookingProducer.addBooking);
            var me = bookingProducer;
            var booking = {
                eventName: 'Name of the booking',
                description: 'Description of the booking',
                dateStamp: {
                    year: '',
                    month: '',
                    day: '',
                    hour: '',
                    minute: ''
                }
            };

            var fieldset = me.renderBookingForm(booking);
            fieldset.append('<input type="button" value="Add" id="submit" />');
            fieldset.find("#submit").click(function() {
                booking = me.loadBookingFromForm(fieldset, booking);
                if (me.checkValidBooking(booking)) {
                    bProxy.addBooking(booking, me.renderBooking);
                }
            });
            console.log("exiting addBooking()");
        },
        /**
         * Prefunction to render the form used to edit the given booking
         * @param {JSON} booking
         */
        editBooking: function(booking) {
            console.log("entering editBooking()");
            console.log(booking);
            var me = bookingProducer;
            var fieldset = me.renderBookingForm(booking);
            me.renderEditAdmins(booking.uuid);
            me.renderEditGuests(booking.uuid);
            commonProducer.bumpCurrentMenuFunction();
            fieldset.append('<input type="button" value="Cancel" id="cancel" />');
            fieldset.append('<input type="button" value="Edit" id="submit" />');
            fieldset.find("#submit").click(function() {
                booking = me.loadBookingFromForm(fieldset, booking);
                if (me.checkValidBooking(booking)) {
                    bProxy.updateBooking(booking, me.renderBooking);
                }
            });
            fieldset.find("#cancel").click(function() {
                booking = me.loadBookingFromForm(fieldset, booking);
                if (me.checkValidBooking(booking)) {
                    previousMenuFunction();
                }
            });
            console.log("exiting editBooking()");
        },
        /**
         * Checks if the given booking is a valid booking
         * @param {JSON} booking
         * @returns {Boolean} if the given booking is a valid booking
         */
        checkValidBooking: function(booking) {
            console.log("entering checkValidBooking()");
            var valid = true;

            valid &= commonProducer.isValidNumber(booking.dateStamp.year, 'Year');
            valid &= commonProducer.isValidNumber(booking.dateStamp.month, 'Month');
            valid &= commonProducer.isValidNumber(booking.dateStamp.day, 'Day');
            valid &= commonProducer.isValidNumber(booking.dateStamp.hour, 'Hour');
            valid &= commonProducer.isValidNumber(booking.dateStamp.minute, 'Minute');
            valid &= booking.venue !== null;
            console.log("exiting checkValidBooking()");
            return valid;
        },
        /**
         * Loads the form used for creating/editing bookings
         * @param {JQuery} form
         * @param {JSON} booking
         * @returns {JSON} booking
         */
        loadBookingFromForm: function(form, booking) {
            console.log("entering loadBookingFromForm()");
            booking.description = form.find('#description').val();
            booking.eventName = form.find('#eventName').val();

            if (form.find('#venue').val()) {
                var venueUUID = form.find('#venue').val();
                booking.venue = vProxy.getById(venueUUID);
            }
            booking.dateStamp.year = form.find('#year').val();
            booking.dateStamp.month = form.find('#month').val();
            booking.dateStamp.day = form.find('#day').val();
            booking.dateStamp.hour = form.find('#hour').val();
            booking.dateStamp.minute = form.find('#minute').val();

            console.log("exiting loadBookingFromForm()");
            return booking;
        },
        /**
         * Renders the given booking in a form
         * @param {JSON} booking
         */
        renderBookingForm: function(booking) {
            console.log("entering renderBookingForm()");
            var context = $('#context');
            var me = bookingProducer;
            //            Empty Context
            context.html("");
            context.append('<form><fieldset/></form>');

            var fieldset = $('fieldset').last();

            if (booking) {
                fieldset.append('<input type="hidden" name="uuid" id="uuid" value="' + booking.uuid + '"/>');
            }
            fieldset.append('<label for="eventName">Booking Name: </label>');
            fieldset.append('<input type="text" name="eventName" id="eventName" value="' + booking.eventName + '" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<br/>');
            fieldset.append('<label for="description">Description: </label>');
            fieldset.append('<textarea name="description" id="description" class="text ui-widget-content ui-corner-all">' + booking.description + '</textarea>');
            fieldset.append('<br/>');

            var user = uProxy.getUser();
            var byAdmin = vProxy.getByAdmin(user.uuid);
            var byCreator = vProxy.getByCreator();

            fieldset.append("Venue: ");
            fieldset.append('<select id="venue"/>');
            if (byAdmin) {
                me.addOptions(byAdmin, 'By admin');
            }
            if (byCreator) {
                me.addOptions(byCreator, 'By creator');
            }
            if (!byAdmin && !byCreator) {
                fieldset.append("You do not have any venues!<br/>");
            }

            fieldset.append('<br/><label for="year">Year: </label>');
            fieldset.append('<input type="number" name="year" id="year" value="' + booking.dateStamp.year + '" class="text ui-widget-content ui-corner-all" /><br/>');

            fieldset.append('<label for="month">Month: </label>');
            fieldset.append('<input type="number" name="month" id="month" value="' + booking.dateStamp.month + '" class="text ui-widget-content ui-corner-all" /><br/>');

            fieldset.append('<label for="day">Day: </label>');
            fieldset.append('<input type="number" name="day" id="day" value="' + booking.dateStamp.day + '" class="text ui-widget-content ui-corner-all" /><br/>');

            fieldset.append('<label for="hour">Hour: </label>');
            fieldset.append('<input type="number" name="hour" id="hour" value="' + booking.dateStamp.hour + '" class="text ui-widget-content ui-corner-all" /><br/>');

            fieldset.append('<label for="minute">Minutes: </label>');
            fieldset.append('<input type="number" name="minute" id="minute" value="' + booking.dateStamp.minute + '" class="text ui-widget-content ui-corner-all" /><br/>');

            fieldset.append('<br/>');

            console.log("exiting renderBookingForm()");
            return fieldset;
        },
        /**
         * Adds a list of venues under a label to a html selectlist
         * @param {JSON} venues
         * @param {string} label
         */
        addOptions: function(venues, label) {
            console.log("entering addOptions()");
            var select = $('select').last();
            select.append('<optgroup label="' + label + '"/>');
            var optgroup = select.find('optgroup').last();
            venues = (venues.venue instanceof Array ? venues.venue : venues);
            $.each(venues, function(i, venue) {
                if (venue) {
                    optgroup.append('<option value="' + venue.uuid + '">' + venue.venueName + '</option>');
                }
            });
            console.log("exiting addOptions()");
        },
        /**
         * Adds a date stamp to the context to be rendered
         * @param {JQuery} context
         * @param {JSON} dateStamp
         */
        addDateStamp: function(context, dateStamp) {
            console.log("entering addDateStamp()");
            //            Not clearing html context here since it is used from other functions
            context.append(dateStamp.year + '-' + dateStamp.month + '-' + dateStamp.day);
            context.append(' | ');
            context.append(dateStamp.hour + ':' + dateStamp.minute);
            console.log("exiting addDateStamp()");
        },
        /**
         * Render an edit list of admins for the matching booking
         * @param {String} bookingId
         */
        renderEditAdmins: function(bookingId) {
            console.log("Entering renderEditAdmins");
            var admins = bProxy.getBookingById(bookingId).admins;
            var context = $('#context');
            var me = bookingProducer;

            context.append('<table id="contextTable"/>');
            var table = context.find('table').last();
            table.append('<th>Admins</th><th/><th/>');

            if (admins) {
                admins = (admins.user instanceof Array ? admins.user : admins);
                $.each(admins, function(i, admin) {
                    if (admin) {
                        table.append('<tr/>');
                        var row = table.find('tr').last();
                        row.append('<td><username>' + admin.userName + '</username></td>');
                        row.append('<td><demote id="demote' + i + '" class="link"><a>Demote</a></demote></td>');
                        row.append('<td><remove id="remove' + i + '" class="link"><a>Remove</a></remove></td>');

                        context.find("#demote" + i).click(function() {
                            console.log("Demoting");
                            bProxy.demoteAdmin(bookingId, admin, me.editBooking);
                        });

                        context.find("#remove" + i).click(function() {
                            bProxy.removeAttender(bookingId, admin, me.editBooking);
                        });
                    }
                });
            }
            console.log("Exiting renderEditAdmins");
        },
        /**
         * Render an edit list of guests for the matching booking
         * @param {String} bookingId
         */
        renderEditGuests: function(bookingId) {
            console.log("Entering renderEditGuests");
            var guests = bProxy.getBookingById(bookingId).guests;
            var context = $('#context');
            var me = bookingProducer;

            context.append('<table id="contextTable"/>');
            var table = context.find('table').last();
            table.append('<th>Guests</th><th/><th/>');

            if (guests) {
                guests = (guests.user instanceof Array ? guests.user : guests);
                $.each(guests, function(i, guest) {
                    if (guest) {
                        table.append('<tr/>')
                        var row = table.find('tr').last();
                        row.append('<td><username>' + guest.userName + '</username></td>');
                        row.append('<td><promote id="promote' + i + '" class="link"><a>Promote</a></promote></td>');
                        row.append('<td><remove id="remove' + i + '" class="link"><a>Remove</a></remove></td>');

                        context.find("#promote" + i).click(function() {
                            console.log("Promoting");
                            bProxy.promoteGuest(bookingId, guest, me.editBooking);
                        });

                        context.find("#remove" + i).click(function() {
                            bProxy.removeAttender(bookingId, guest, me.editBooking);
                        });
                    }
                });
            }
            console.log("Exiting renderEditGuests");
        },
        /**
         * Renders a list of all bookings related to the logged in user
         */
        myBookings: function() {
            var me = bookingProducer;
            console.log("entering myBookings()");
            commonProducer.setCurrentMenuFunction(me.myBookings);
            var context = $('#context');
            context.html('');
            var bookings = bProxy.getBookingByCreator();
            me.listBookings(bookings, context, "Bookings I've created");
            var loggedIn = uProxy.getUser();
            bookings = bProxy.getBookingByAttender(loggedIn.uuid);
            console.log(bookings);
            me.listBookings(bookings, context, "Bookings I'm attending");
            console.log("exiting myBookings()");
        },
        /**
         * Renders a list of all existing bookings
         */
        allBookings: function() {
            var me = bookingProducer;
            console.log("entering allBookings()");
            commonProducer.setCurrentMenuFunction(me.allBookings);
            var context = $('#context');
            context.html('');
            var bookings = bProxy.getAll();
            me.listBookings(bookings, context, "All bookings");
        },
        /**
         * Renders a form used for searching for bookings
         */
        searchForm: function() {
            console.log("entering searchForm()");
            commonProducer.setCurrentMenuFunction(bookingProducer.searchForm);
            var me = bookingProducer;
            var context = $('#context');
            context.html('');
            context.append('<form><fieldset/></form>');
            var fieldset = context.find('fieldset').last();
            me.createSearchByName();
            me.createSearchByVenue();
            fieldset.append('<errorBox/>')
            console.log("exiting searchForm()");
        },
        /**
         * Renders a row with search by name functionality
         */
        createSearchByName: function() {
            var me = bookingProducer;
            var common = commonProducer;
            var context = $('#context');
            var fieldset = context.find('fieldset').last();
            fieldset.append('<label for="byName">Search by name: </label>');
            fieldset.append('<input type="text" name="byName" id="byName" placeholder="Name to Search for" class="text ui-widget-content ui-corner-all" />');
            fieldset.append('<input type="button" value="Search By Name" id="searchByName" />');
            fieldset.find("#searchByName").click(function() {
                var bookingToSearchFor = fieldset.find('#byName').val();
                var bookings = bProxy.getByEventName(bookingToSearchFor);
                if (bookings) {
                    context.html('');
                    me.listBookings(bookings, context, "Results for search: " + bookingToSearchFor);
                } else {
                    common.noMatches();
                }

            });
            fieldset.append('<div/>');
        },
        /**
         * Renders a row with search by venue functionality
         */
        createSearchByVenue: function() {
            var me = bookingProducer;
            var common = commonProducer;
            var context = $('#context');
            var fieldset = context.find('fieldset').last();
            var venues = vProxy.getAll();
            fieldset.append('<label for="byName">Search by venue: </label>');
            fieldset.append('<select id="venue"/>');
            if (venues) {
                me.addOptions(venues, 'All');
            }

            fieldset.append('<input type="button" value="Search By Venue" id="searchByVenue" />');
            fieldset.find("#searchByVenue").click(function() {
                if (context.find('#venue').val()) {
                    var venueUUID = context.find('#venue').val();
                }
                var bookings = bProxy.getBookingByVenue(venueUUID);
                var venue = vProxy.getById(venueUUID);
                if (bookings) {
                    context.html('');
                    me.listBookings(bookings, context, "Results for search by venue: " + venue.venueName);
                } else {
                    common.noMatches();
                }
            });
            fieldset.append('<div/>');
        }
    };
}());
